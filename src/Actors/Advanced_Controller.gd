extends Base_Controller

class_name Advanced_Controller

### constants ###
const CLASS_NAME                                     = "Advanced_Controller"

### signals ###
signal wants_path                                                                                                                             # triggered when we feel like needing a path to follow

# exported variables, not used in code
export (Vector2) var path_margin    : Vector2        = Vector2(4, 4)                                                                          # margin used to build the path
export (Vector2) var exit_margin    : Vector2        = Vector2(2, 2)                                                                          # margin used to exit areas in vanilla collisions mode
export (int)     var exit_path      : int            = 5                                                                                      # how many times we need to see the player with the direct laser to exit path tracking
export (float)   var exit_path_time : float          = 2.0                                                                                    # time to wait between seeing the player and actually exiting the path
export (float)   var berz_path_time : float          = 1.0                                                                                    # exit path time during berzerk
export (float)   var min_stuck_time : float          = 1.5                                                                                    # minimum time of the path stuck timer
export (float)   var max_stuck_time : float          = 3.0                                                                                    # maximum time of the path stuck timer

# variables used in code
var following_path                  : bool           = false                                                                                  # used to disable path finding when not needed
var path                            : Array          = []                                                                                     # points of the chosen path
var path_idx                        : int            = 0                                                                                      # current index of the chosen path
var current_area                    : Vector2        = Vector2(-1, -1)                                                                        # current area we're in
var target_area                     : Vector2        = Vector2(-1, -1)                                                                        # area the target is in
var corners                         : Dictionary     = {}                                                                                     # corners dictionary, used to allow collisions with some walls with vanilla collisions enabled
var path_score                      : float          = -1.0                                                                                   # pathfight score, assigned at random on draw to get a winner
var path_id                         : float                                                                                                   # path uid
var pathfights_won                  : int                                                                                                     # how many robots are following this path
var seen_player                     : int                                                                                                     # how many times we've seen the player
var path_group                      : String                                                                                                  # path group we belong to
var desired_dir                     : int                                                                                                     # stored global desired direction, to be accessed from outside of the physics loop

onready var path_navigation         : bool           = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "PATH_NAVIGATION")[0]         # if true we'll search for a path to get the player
onready var line_of_sight           : bool           = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "LINE_OF_SIGHT")[0]           # if true, we will only fire at targets on sight
onready var path_max_length         : int            = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "PATH_MAX_LENGTH")[0]         # path max length, stop computing path if it's higher than this number, no matter what
onready var path_min_length         : int            = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "PATH_MIN_LENGTH")[0]         # path min length, don't use multiplier for distances this low
onready var path_max_multi          : float          = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "PATH_MAX_MULTIPLIER")[0]     # path max length multiplier
onready var laser_collision         : int            = 17                                                                                     # this should mean, collide with walls and hitboxes

onready var nav_points : Dictionary                  = {global_vars.UP    : [],                                                               # navigation points, used to avoid walls in vanilla mode
														global_vars.DN    : [],
														global_vars.LX    : [],
														global_vars.RX    : [],
														global_vars.LX_UP : [],
														global_vars.LX_DN : [],
														global_vars.RX_UP : [],
														global_vars.RX_DN : []}


### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	v_coll                    = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "VANILLA_COL_DETECT")[0]      # if true we'll try to behave as vanilla as possible in the collision detection department
	barrel_align              = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "BARREL_AIM_ALIGN")[0]        # if true we'll align the aim at our barrels instead of the center of our body
	ambidex                   = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "AMBIDEXTROUS")[0]            # if true we'll fire with both arms
	avoid_robots              = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "AVOID_ROBOT_COLLS")[0]       # if true we'll try to prevent collisions with other robots
	berzerk_lasers            = global_vars.get_game_config("ADVANCED_ROBOT_CONTROLLER", "BERZERK_ADD_LASERS")[0]      # extra maximum lasers when in berzerk 

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta) -> void:
	var t_pos          : Vector2
	var b_pos          : Vector2
	var l_pos          : Vector2
	var space_state    : Physics2DDirectSpaceState
	var result         : Dictionary
	var left           : bool                        = false
	var f_dir          : int                         = global_vars.STOP
	var tile           : Vector2
	var used_tolerance : Vector2
	
	current_dir   = global_vars.STOP
	desired_dir   = global_vars.STOP
	
	used_tolerance = move_tolerance
	
	if is_instance_valid(target) && current_area.x >= 0 && is_instance_valid(body) && body.operative:
		tile  = mapgen.map.world_to_map(body.global_position)
		t_pos = target.global_position
		
		if barrel_align:
			b_pos = body.barrel.global_position
			
			# align at the left barrel
			if ambidex && t_pos.x < b_pos.x:
				left = true
				b_pos.x -= body.barrel.position.x
		
		else:
			b_pos = body.global_position
		
		# fire_rate
		if fire_rate > 0:
			fire_rate -= delta
		
		else:
			# try to get a fire direction
			f_dir = can_fire(b_pos, t_pos)
			
			l_pos = b_pos
			
			if ambidex && barrel_align && f_dir == global_vars.STOP:
				
				# we failed aiming with the left barrel, try right
				if left:
					left = false
					l_pos = Vector2(b_pos.x + body.barrel.position.x, b_pos.y)
				
				# we failed aiming with the right barrel, try left
				else:
					left = true
					l_pos = Vector2(b_pos.x - body.barrel.position.x, b_pos.y)
				
				f_dir = can_fire(l_pos, t_pos)
		
			# we have a target, fire
			if f_dir != global_vars.STOP:
				fire_rate = max_fr
				
				if !line_of_sight:
					body.fire(f_dir, left)
				
				else:
					space_state = get_world_2d().direct_space_state
					result = space_state.intersect_ray(l_pos, target.global_position, [body, body.hitbox], laser_collision)
					
					if result && result.collider.is_in_group(global_vars.PLAYER_HIT_GROUP):
						body.fire(f_dir, left)
						
						following_path = false
						path_idx       = 1
		
		if body.last_tile != tile:
			body.last_tile = tile
			
			avoid_collisions(tile)
			
			if following_path:
				space_state = get_world_2d().direct_space_state
				result = space_state.intersect_ray(l_pos, target.global_position, [body, body.hitbox], laser_collision)
				
				if result && result.collider.is_in_group(global_vars.PLAYER_HIT_GROUP):
					seen_player += 1
					
					# start the timer in case we've seen the player
					if $ExitPathTimer.is_stopped():
						
						if body.in_berzerk:
							$ExitPathTimer.wait_time = berz_path_time
						
						else:
							$ExitPathTimer.wait_time = exit_path_time
						
						$ExitPathTimer.start()
					
					if seen_player >= exit_path:
						seen_player    = 0
						following_path = false
						path_idx       = 1
						
						# stop the timer since we already exited the path
						if !$ExitPathTimer.is_stopped():
							$ExitPathTimer.stop()
				
				else:
					if !$ExitPathTimer.is_stopped():
						$ExitPathTimer.stop()
					
					seen_player = 0
		
		if avoid_robots:
			if rc_cur_tick > 0:
				rc_cur_tick -= delta
			
			else:
				rc_cur_tick = rc_tick
				avoid_robot_collisions()
		
		if following_path && path_idx < len(path):
			t_pos = mapgen.tile_center(path[path_idx])
			b_pos = body.global_position
		
		elif path_navigation && path_idx > 0:
			path_idx       = 0
			current_area   = Vector2(-1, -1)
			avoid_collisions(tile)
		
		if b_pos.x + used_tolerance.x > t_pos.x && b_pos.x - used_tolerance.x > t_pos.x:
			if !avoid_l && !robot_l:
				current_dir = global_vars.LX
			
			else:
				desired_dir       = global_vars.LX
		
		elif b_pos.x + used_tolerance.x < t_pos.x && b_pos.x - used_tolerance.x < t_pos.x:
			if !avoid_r && !robot_r:
				current_dir = global_vars.RX
			
			else:
				desired_dir       = global_vars.RX
		
		if b_pos.y + used_tolerance.y < t_pos.y && b_pos.y - used_tolerance.y < t_pos.y:
			if !avoid_d && !avoid_l && !robot_d && !robot_l && current_dir == global_vars.LX:
				current_dir = global_vars.LX_DN
			
			elif !avoid_d && !avoid_r && !robot_d && !robot_r && current_dir == global_vars.RX:
				current_dir = global_vars.RX_DN
		
			elif !avoid_d && !robot_d:
				current_dir = global_vars.DN
			
			elif (avoid_d || robot_d) && desired_dir == global_vars.LX:
				desired_dir       = global_vars.LX_DN
			
			elif (avoid_d || robot_d) && desired_dir == global_vars.RX:
				desired_dir       = global_vars.RX_DN
			
			elif (avoid_d || robot_d) && desired_dir == global_vars.STOP:
				desired_dir       = global_vars.DN
		
		if b_pos.y + used_tolerance.y > t_pos.y && b_pos.y - used_tolerance.y > t_pos.y:
			if !avoid_u && !avoid_l && !robot_u && !robot_l && current_dir == global_vars.LX:
				current_dir = global_vars.LX_UP
			
			elif !avoid_u && !avoid_r && !robot_u && !robot_r && current_dir == global_vars.RX:
				current_dir = global_vars.RX_UP
			
			elif !avoid_u && !robot_u:
				current_dir = global_vars.UP
			
			elif (avoid_u || robot_u) && desired_dir == global_vars.LX:
				desired_dir       = global_vars.LX_UP
			
			elif  (avoid_u || robot_u) && desired_dir == global_vars.STOP:
				desired_dir       = global_vars.UP
		
		
		if following_path && current_dir == global_vars.STOP:
			if $PathStuckTimer.is_stopped():
				start_path_stuck_timer()
			
			body.last_tile = tile
			on_path_keypoint(tile)
		
		elif !$PathStuckTimer.is_stopped():
			$PathStuckTimer.stop()
		
		
		if path_navigation:
			if !following_path && (desired_dir != global_vars.STOP || current_dir == global_vars.STOP):
				if $PathTimer.is_stopped():
					$PathTimer.start()
			
			elif !$PathTimer.is_stopped():
				$PathTimer.stop()
		
		body.move(current_dir, delta)
		
		last_dir = current_dir


### common methods ###


# eventually restart the exit path timer when entering berzerk
func berzerk() -> void:
	.berzerk()
	
	if !$ExitPathTimer.is_stopped() && $ExitPathTimer.time_left > berz_path_time:
		$ExitPathTimer.stop()
		$ExitPathTimer.wait_time = berz_path_time
		$ExitPathTimer.start()


# overridden to support path fighting TODO: implement pathfighting
func avoid_robot_collisions() -> void:
	var b_pos : Vector2  = body.last_tile
	var dist  : Vector2  = Vector2()
	var t_pos : Vector2 
	
	robot_l = false
	robot_r = false
	robot_u = false
	robot_d = false
	
	for c in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		if c != body && is_instance_valid(c) && !c.is_queued_for_deletion():
			t_pos = c.last_tile
			
			dist.x = max(b_pos.x, t_pos.x) - min(b_pos.x, t_pos.x)
			dist.y = max(b_pos.y, t_pos.y) - min(b_pos.y, t_pos.y)
			
			if dist.x > dist.y:
				# we're roughly aligned on the y with the other robot
				if b_pos.y > t_pos.y - rc_tiles.y && b_pos.y < t_pos.y + rc_tiles.y:
					# we're on the right and close
					if b_pos.x > t_pos.x && b_pos.x - t_pos.x < rc_tiles.x:
						robot_l = true
						#print(body.robot_id, " - LEFT")
						
						if following_path && c.controller.get_class() == CLASS_NAME && c.controller.following_path:
						
							if (last_dir == global_vars.LX || desired_dir == global_vars.LX) && (c.controller.last_dir == global_vars.RX || c.controller.desired_dir == global_vars.RX):
								#print(body.robot_id, " - PATHFIGHT LEFT")
								
								start_pathfight(c.controller)
					
					# we're on the left and close
					elif b_pos.x < t_pos.x && t_pos.x - b_pos.x < rc_tiles.x:
						robot_r = true
						#print(body.robot_id, " - RIGHT")
						
						if following_path && c.controller.get_class() == CLASS_NAME && c.controller.following_path:
							
							if (last_dir == global_vars.RX || desired_dir == global_vars.RX) && (c.controller.last_dir == global_vars.LX || c.controller.desired_dir == global_vars.LX):
								#print(body.robot_id, " - PATHFIGHT RIGHT")
								
								start_pathfight(c.controller)
			
			
			else:
				if b_pos.x > t_pos.x - rc_tiles.x && b_pos.x < t_pos.x + rc_tiles.x:
					# we're under the other robot and close
					if b_pos.y > t_pos.y && b_pos.y - t_pos.y < rc_tiles.y:
						robot_u = true
						#print(body.robot_id, " - UP")
						
						if following_path && c.controller.get_class() == CLASS_NAME && c.controller.following_path:
							
							if (last_dir == global_vars.UP || desired_dir == global_vars.UP) && (c.controller.last_dir == global_vars.DN || c.controller.desired_dir == global_vars.DN):
								#print(body.robot_id, " - PATHFIGHT UP")
								
								start_pathfight(c.controller)
					
					# we're over the other robot and close
					elif b_pos.y < t_pos.y && t_pos.y - b_pos.y < rc_tiles.y:
						robot_d = true
						#print(body.robot_id, " - DOWN")
						
						if following_path && c.controller.get_class() == CLASS_NAME && c.controller.following_path:
							
							if (last_dir == global_vars.DN || desired_dir == global_vars.DN) && (c.controller.last_dir == global_vars.UP || c.controller.desired_dir == global_vars.UP):
								#print(body.robot_id, " - PATHFIGHT DOWN")
								
								start_pathfight(c.controller)


# avoid collisions with walls
# warning-ignore:unused_argument
func avoid_collisions(tile : Vector2, delta : float = 0) -> void:
	var used_margin : Vector2 = Vector2()
	var new_area    : bool    = false
	var corner      : Vector2
	
	if v_coll:
		
		if current_area.x > 0:
			
			
			if body.last_tile.x < current_area.x + mapgen.area_size.x / 2:
				if nav_points[global_vars.LX][1]:
					used_margin.y += exit_margin.y
			
			elif nav_points[global_vars.RX][1]:
				used_margin.y += exit_margin.y
			
			
			if body.last_tile.y < current_area.y + mapgen.area_size.y / 2:
				if nav_points[global_vars.UP][1]:
					used_margin.x += exit_margin.x
			
			elif nav_points[global_vars.DN][1]:
				used_margin.x += exit_margin.x
	
	
	if current_area.x < 0:
		new_area = true
	
	elif tile.x < current_area.x - used_margin.x:
		new_area = true
	
	elif tile.x > get_area_corner(global_vars.RX_UP).x + used_margin.x:
		new_area = true
	
	elif tile.y < current_area.y - used_margin.y:
		new_area = true
	
	elif tile.y > get_area_corner(global_vars.LX_DN).y + used_margin.y:
		new_area = true
	
	if new_area:
		current_area   = mapgen.get_area(tile)
		nav_points = detect_walls(current_area)
		
		#print("NEW_AREA ", current_area)
		
		for dir in global_vars.diagonal_directions():
			corners[dir] = is_wall_apex(get_area_corner(dir))
	
	
	avoid_l = nav_points[global_vars.LX][1] && tile.x <= nav_points[global_vars.LX][0].x
	avoid_r = nav_points[global_vars.RX][1] && tile.x >= nav_points[global_vars.RX][0].x
	avoid_u = nav_points[global_vars.UP][1] && tile.y <= nav_points[global_vars.UP][0].y
	avoid_d = nav_points[global_vars.DN][1] && tile.y >= nav_points[global_vars.DN][0].y
	
	
	if nav_points[global_vars.LX_UP][1] && tile.y <= nav_points[global_vars.LX_UP][0].y && tile.x <= nav_points[global_vars.LX_UP][0].x:
		
		if !v_coll || !corners[global_vars.LX_UP]:
			if tile.x < current_area.x + margin.x || nav_points[global_vars.UP][1]:
				avoid_u = true
			
			else:
				avoid_l = true

	
	if nav_points[global_vars.LX_DN][1] && tile.y >= nav_points[global_vars.LX_DN][0].y && tile.x <= nav_points[global_vars.LX_DN][0].x:
		
		if !v_coll || !corners[global_vars.LX_DN]:
			corner = get_area_corner(global_vars.LX_DN)
			
			if tile.x < corner.x + margin.x || nav_points[global_vars.DN][1]:
				avoid_d = true
			
			else:
				avoid_l = true
	
	if nav_points[global_vars.RX_UP][1] && tile.y <= nav_points[global_vars.RX_UP][0].y && tile.x >= nav_points[global_vars.RX_UP][0].x:
		
		if !v_coll || !corners[global_vars.RX_UP]:
			corner = get_area_corner(global_vars.RX_UP)
			
			if tile.x > corner.x - margin.x || nav_points[global_vars.UP][1]:
				avoid_u = true
			
			else:
				avoid_r = true
	
	
	if nav_points[global_vars.RX_DN][1] && tile.y >= nav_points[global_vars.RX_DN][0].y && tile.x >= nav_points[global_vars.RX_DN][0].x:
		
		if !v_coll || !corners[global_vars.RX_DN]:
			corner = get_area_corner(global_vars.RX_DN)
			
			if tile.x > corner.x - margin.x || nav_points[global_vars.DN][1]:
				avoid_d = true
			
			else:
				avoid_r = true

# start fighting for path and act according to the result
func start_pathfight(other : Node2D) -> void:
	
	# we just have more robotpower, we win
	if pathfights_won > other.pathfights_won:
		pathfight_win(other)
		#print(body.robot_id, "WON")
	
	# draw since the groups are identical in numbers, check for a winner and act accordingly
	elif pathfights_won == other.pathfights_won:
		path_score = randf()
		
		if other.path_score >= 0:
			if other.path_score <= path_score:
				pathfight_win(other)
				#print(body.robot_id, "WON 2")
		
			else:
				pathfight_lose(other)
				#print(body.robot_id, "LOST 2")
		
	
	# we lost
	else:
		pathfight_lose(other)
		#print(body.robot_ID, "LOST")


# called on the initial winner to eventually inform the others we won
func pathfight_win(other : Node2D) -> void:
	#print(body.robot_id, " - WON - ", other.body.robot_id)
	
	# we're part of a group, tell the others we won
	if path_group != "":
		get_tree().call_group(path_group, "won_pathfight")
	
	# we're alone, just update us
	else:
		won_pathfight()
	
	# other isn't alone, tell its whole group they lost
	if other.path_group != "":
		get_tree().call_group(other.path_group, "set_new_path", path, path_id, path_idx, pathfights_won, path_group)
	
	# other is alone, just inform it
	else:
		other.set_new_path(path, path_id, path_idx, pathfights_won, path_group)


# called on the initial winner to eventually inform the others we lost
func pathfight_lose(other : Node2D) -> void:
	#print(body.robot_id, " - LOST - ", other.body.robot_id)
	
	# other isn't alone, tell them they won
	if other.path_group != "":
		get_tree().call_group(other.path_group, "won_pathfight")
	
	# other is alone, just inform it
	else:
		other.won_pathfight()
	
	# if we already have a path group, tell the others we lost
	if path_group != "":
		get_tree().call_group(path_group, "set_new_path", other.path, other.path_id, other.path_idx + 1, other.pathfights_won, other.path_group)
	
	# we're alone, just update us
	else:
		set_new_path(other.path, other.path_id, other.path_idx + 1, other.pathfights_won, other.path_group)


# group function, increase pathfights won and reset the random score
func won_pathfight() -> void:
	#print(body.robot_id, " - GROUP WIN")
	
	pathfights_won += 1
	path_score     = -1.0
	
	if path_group == "":
		path_group = global_vars.get_new_path_group()
		add_to_group(path_group)


# group function, set new path for the whole group
func set_new_path(new_path : Array, new_path_id : float, new_path_idx : int,  new_pathfights_won : int, new_path_group : String) -> void:
	#print(body.robot_id, " - GROUP LOSE")
	
	if new_path_id != path_id:
		path           = new_path
		path_id        = new_path_id
		path_idx       = new_path_idx
		pathfights_won = new_pathfights_won
		
		# replace path group
		if new_path_group != path_group:
			if path_group != "":
				remove_from_group(path_group)
			
			path_group = new_path_group
			add_to_group(new_path_group)
	
	path_score     = -1.0


# called when the robot is operative and ready to do its dirty job
func body_operative() -> void:
	var tile : Vector2 = mapgen.map.world_to_map(body.global_position)
	body.last_tile = tile
	# warning-ignore:return_value_discarded
	find_new_target()
	
	avoid_collisions(tile)


# start this controller properly take over its body
func start(new_body : Node2D, new_map : Node) -> void:
	# setting up main vars
	body         = new_body
	mapgen       = new_map
	
	# setting up main body vars
	body.controller = self



# are we ready to fire? in what direction?
func can_fire(b_pos : Vector2, t_pos : Vector2) -> int:
	var deg : float
	
	if global_vars.lasers < global_vars.levels[difficulty][0] + additional_lasers:
		
		if b_pos.x + move_tolerance.x > t_pos.x && b_pos.x - move_tolerance.x < t_pos.x:
			if b_pos.y < t_pos.y:
				return global_vars.DN
			
			return global_vars.UP
			
		if b_pos.y + move_tolerance.y > t_pos.y && b_pos.y - move_tolerance.y < t_pos.y:
			if b_pos.x < t_pos.x:
				return global_vars.LX
			
			return global_vars.RX
		
		deg = rad2deg(b_pos.angle_to_point(t_pos))
		
		if deg > 0:
			if deg > aim_angle - angle_tolerance && deg < aim_angle + angle_tolerance:
				return global_vars.LX_UP
			
			elif deg > aim_angle * 3 - angle_tolerance && deg < aim_angle * 3 + angle_tolerance:
				return global_vars.RX_UP
		
		else:
			if deg < -aim_angle + angle_tolerance && deg > -aim_angle - angle_tolerance:
				return global_vars.LX_DN
			
			elif deg < -aim_angle * 3 + angle_tolerance && deg > -aim_angle * 3 - angle_tolerance:
				return global_vars.RX_DN
			
	return global_vars.STOP


### custom methods ###

# find a path that leads to your target
func find_path() -> bool:
	var target_pos   : Vector2
	var checked_area : Vector2    = mapgen.get_area(body.last_tile)
	var next_area    : Vector2    = checked_area
	var directions   : Array      = []
	var exclude      : Array      = []
	var checked      : int        = 0
	var area_result  : Dictionary = {}
	var tested_path  : Array      = []
	var first        : bool       = true
	var last_chance  : bool       = false
	var tries        : int        = 0
	var max_checked  : int        = path_max_length
	var dist         : Vector2
	
	# return if there's no target
	if !is_instance_valid(target):
		return false
	
	pathfights_won = 0
	path_id        = 0
	
	target_pos     = mapgen.map.world_to_map(target.global_position)
	target_area    = mapgen.get_area(target_pos)
	current_area   = checked_area
	
	dist           = mapgen.get_area_distance(checked_area, target_area)
	
	if dist.x * dist.y > path_min_length:
		# warning-ignore:narrowing_conversion
		max_checked  = min(max_checked, int(dist.x * dist.y * path_max_multi))
	
	directions   = generate_path_directions(checked_area, target_area, dist)
	
	if target_area == current_area:
		$PathTimer.start()
		return false
	
	#print("INIT: ", checked_area, " TARGET: ", target_area)
	
	while checked < max_checked && checked_area != target_area:
		checked    += 1
		
		#print("CHECKED: ", checked)
		
		if tries >= 4:
			if len(tested_path) == 1:
				
				if last_chance:
					#print("FAILED")
					return false
				
				last_chance = true
				tries       = 0
			
			else:
				last_chance = false
				exclude.append(tested_path.pop_back())
				checked_area = tested_path[len(tested_path) - 1]
				tries        = 0
			
			#print("EXCLUDING: ", "NEW: ", checked_area)
		
		tries       = 0
		
		if checked_area != target_area:
			area_result = detect_walls(checked_area)
			directions  = generate_path_directions(checked_area, target_area)
		
		else:
			area_result = {}
		
		# appending current area to the area_path
		if first:
			first = false
			tested_path.append(checked_area)
		
		#else:
		#	print("NEXT_AREA: ", checked_area, "DIRECTIONS: ", directions)
		
		for dir in directions:
			#print("TESTING DIRECTION: ", dir, " TRIES: ", tries)
			
			if area_result[dir][1]:
				tries += 1
				
				#print("WALL: ", tries)
			
			else:
				next_area = get_next_area(checked_area, dir)
				
				if next_area in exclude:
					tries += 1
					#print("AVOID: ", next_area, " current ", checked_area, " dir ",  dir, " ", tries)
				
				
				elif next_area in tested_path:
					tries += 1
				
				else:
					checked_area = next_area
					tested_path.append(checked_area)
					break
				
	
	if len(tested_path) > 0:
		path = build_path(tested_path, target_pos)
		#mapgen.draw_debug_path(path, true)
		return true
	
	$PathTimer.start()
	return false

# returns next path areas
func get_next_area(area : Vector2, dir : int) -> Vector2:
	if dir == global_vars.RX:
		#print("RX")
		area.x += mapgen.area_size.x
	
	elif dir == global_vars.LX:
		#print("LX")
		area.x -= mapgen.area_size.x
	
	elif dir == global_vars.UP:
		#print("UP")
		area.y -= mapgen.area_size.y
	
	elif dir == global_vars.DN:
		#print("DN")
		area.y += mapgen.area_size.y
	
	return area

# returns a path from an area_path
func build_path(area_path : PoolVector2Array, target_pos : Vector2) -> PoolVector2Array:
	var path_result : PoolVector2Array = []
	var idx         : int              = 0
	var last_pos    : Vector2          = body.last_tile
	var next_pos    : Vector2          = Vector2()
	var last_area   : Vector2          = area_path[0]
	var next_area   : Vector2
	
	while idx < len(area_path):
		if idx + 1 < len(area_path):
			next_area = area_path[idx + 1]
		
		# first area of the path
		if area_path[idx] == current_area:
			next_pos.x = max(body.last_tile.x, current_area.x + path_margin.x)
			next_pos.x = min(next_pos.x, current_area.x + mapgen.area_size.x - path_margin.x)
			
			next_pos.y = min(body.last_tile.y, current_area.y + mapgen.area_size.y - path_margin.y)
			next_pos.y = max(next_pos.y, current_area.y + path_margin.y)
		
		# last area of the path
		elif area_path[idx] == target_area:
			next_pos = target_pos
		
		elif next_area.y != area_path[idx].y:
			if last_area.x < area_path[idx].x:
				#print("LX")
				next_pos = Vector2(area_path[idx].x + path_margin.x, last_pos.y)
			
			elif last_area.x > area_path[idx].x:
				#print("RX")
				next_pos = Vector2(area_path[idx].x + mapgen.area_size.x - path_margin.x, last_pos.y)
			
			else:
				last_area = area_path[idx]
				idx += 1
				continue
		
		else:
			if last_area.y < area_path[idx].y:
				#print("UP")
				next_pos = Vector2(last_pos.x, area_path[idx].y + path_margin.y)
			
			elif last_area.y > area_path[idx].y:
				#print("DN")
				next_pos = Vector2(last_pos.x, area_path[idx].y + mapgen.area_size.y - path_margin.y)
			
			else:
				last_area = area_path[idx]
				idx += 1
				continue
		
		if area_path[idx] != current_area:
			if area_path[idx].x == target_area.x:
				next_pos.x = max(target_pos.x, target_area.x + path_margin.x)
				next_pos.x = min(next_pos.x, target_area.x + mapgen.area_size.x - path_margin.x)
			
			if area_path[idx].y == target_area.y:
				next_pos.y = max(target_pos.y, target_area.y + path_margin.y)
				next_pos.y = min(next_pos.y, target_area.y + mapgen.area_size.y - path_margin.y)
		
		if idx > 1:
			path_result.append(last_pos)
		
		last_area = area_path[idx]
		last_pos  = next_pos
		
		idx += 1
	
	# we created a new path, exit from the group
	if path_group != "":
		remove_from_group(path_group)
		path_group = ""
	
	path_result.append(next_pos)
	
	path_id = OS.get_ticks_msec()
	
	return path_result


# we reached a keypoint
func on_path_keypoint(tile : Vector2, timer : bool = false, force : bool = false) -> void:
	var dist      : Vector2
	
	if !force:
		dist       = mapgen.get_area_distance(mapgen.get_area(target.global_position, true), target_area)
	
	if force || tile == path[path_idx]:
		path_idx += 1
		
		if !$PathStuckTimer.is_stopped():
			$PathStuckTimer.stop()
		
		if dist.x > 1 || dist.y > 1 || path_idx >= len(path):
			following_path = false
			path_idx       = 1
			
			if timer:
				$PathTimer.start()
			
			else:
				emit_signal("wants_path")
			
			body.last_tile = tile
			current_area   = Vector2(-1, -1)
			avoid_collisions(tile)


# returns an array of directions based on target distance or random if needed
func generate_path_directions(start : Vector2, end : Vector2, dist : Vector2 = Vector2(-1, -1)) -> Array:
	var directs : Array = []
	
	if dist.x == -1:
		dist    = mapgen.get_area_distance(start, end)
	
	#print("DIST: ", dist)
	
	if dist.x >= dist.y:
		# we should mainly move left
		if start.x > end.x:
			directs.append(global_vars.LX)
			directs.append(global_vars.RX)
		
		# we should mainly move right
		else:
			directs.append(global_vars.RX)
			directs.append(global_vars.LX)
		
		if start.y > end.y:
			directs.insert(1, global_vars.DN)
			directs.insert(1, global_vars.UP)
		
		elif start.y < end.y:
			directs.insert(1, global_vars.UP)
			directs.insert(1, global_vars.DN)
		
		else:
			directs = global_vars.cardinal_directions(directs, 1, true)
	
	else:
		# we should mainly move up
		if start.y > end.y:
			directs.append(global_vars.UP)
			directs.append(global_vars.DN)
		
		# we should mainly move down
		else:
			directs.append(global_vars.DN)
			directs.append(global_vars.UP)
		
		if start.x > end.x:
			directs.insert(1, global_vars.RX)
			directs.insert(1, global_vars.LX)
		
		elif start.x < end.x:
			directs.insert(1, global_vars.LX)
			directs.insert(1, global_vars.RX)

		
		else:
			directs = global_vars.cardinal_directions(directs, 1, true)
	
	return directs

# restart the check timer and stop following the path if a new target was found 
func find_new_target() -> bool:
	if global_vars.players_left > 1:
		if !$CheckTargetTimer.is_stopped():
			$CheckTargetTimer.stop()
		
		$CheckTargetTimer.start()
	
	if .find_new_target():
		if following_path:
			following_path = false
			path_idx       = 1
		
		return true
	
	return false

# start path stuck timer with a rand range time
func start_path_stuck_timer():
	$PathStuckTimer.wait_time = rand_range(min_stuck_time, max_stuck_time)
	$PathStuckTimer.start()

# detect current area walls
func detect_walls(area : Vector2) -> Dictionary:
	var check    : Vector2     = Vector2()
	var wall     : bool        = false
	var mrg      : Vector2     = Vector2()
	var point    : Vector2     = Vector2()
	var count    : bool        = false
	var w_amount : int         = 0
	var walls    : Dictionary  = {}
	var cell     : int
	
	
	for dir in nav_points:
		count = false
		mrg   = Vector2()
		
		# check if there's a wall up
		if dir == global_vars.UP:
			check.x = area.x + 1
			check.y = area.y
			point.y = check.y
			mrg.y   = margin.y
			count   = true
		
		# check if there's a wall down
		elif dir == global_vars.DN:
			check.x = area.x + 1
			check.y = area.y + mapgen.area_size.y
			point.y = check.y
			mrg.y   = -margin.y
			count   = true
		
		# check if there's a wall on the left
		elif dir == global_vars.LX:
			check.x = area.x
			check.y = area.y + 1
			point.x = check.x
			mrg.x   = margin.x
			count   = true
		
		# check if there's a wall on the right
		elif dir == global_vars.RX:
			check.x = area.x + mapgen.area_size.x
			check.y = area.y + 1
			point.x = check.x
			mrg.x   = -margin.x
			count   = true
		
		# check top left corner
		elif dir == global_vars.LX_UP:
			check   = area
			point   = check
			mrg     = margin
		
		# check bottom left corner
		elif dir == global_vars.LX_DN:
			check.x = area.x
			check.y = area.y + mapgen.area_size.y
			point   = check
			mrg     = margin
			mrg.y   = -mrg.y
		
		# check top right corner
		elif dir == global_vars.RX_UP:
			check.x = area.x + mapgen.area_size.x
			check.y = area.y
			point   = check
			mrg     = margin
			mrg.x   = -mrg.x
		
		# check bottom left corner
		elif dir == global_vars.RX_DN:
			check.x = current_area.x + mapgen.area_size.x
			check.y = current_area.y + mapgen.area_size.y
			point   = check
			mrg     = -margin
		
		if check.x <= 0 || check.y <= 0 || check.x >= mapgen.map_size.x || check.y >= mapgen.map_size.y:
			wall = true
		
		else:
			cell = mapgen.map.get_cell(check.x, check.y)
			wall = cell > -1 && cell < mapgen.DEBUG
		
		if wall:
			point += mrg
			
			if count:
				w_amount += 1
		
		walls[dir] = [point, wall]
	
	# we're trapped, ask for a teleport
	if area == current_area && w_amount == 4:
		body.wants_teleport = true
	
	return walls


# return the location of a corner tile of current_area, defaults to top left (area pillar)
func get_area_corner(direction : int) -> Vector2:
	var corner = current_area
	
	if direction == global_vars.RX_UP:
		corner.x += mapgen.area_size.x
	
	elif direction == global_vars.LX_DN:
		corner.y += mapgen.area_size.y
	
	elif direction == global_vars.RX_DN:
		corner.x += mapgen.area_size.x
		corner.y += mapgen.area_size.y
	
	return corner

# return a custom class
func get_class() -> String:
	return CLASS_NAME

# returns true if an area corner isn't connected to any other wall
func is_wall_apex(wall_position : Vector2) -> bool:
	var walls = 0
	
	for n in range(-1, 2):
		if n != 0:
			if mapgen.map.get_cell(wall_position.x + n, wall_position.y) == mapgen.WALL:
				walls += 1
			
			if mapgen.map.get_cell(wall_position.x, wall_position.y + n) == mapgen.WALL:
				walls += 1
	
	return walls == 1

# stop following the path, here for compatibilty reasons
func clear_path() -> void:
	seen_player    = 0
	following_path = false
	path_idx       = 1

# used to check for a path when needed and after a while when it failed
func _on_PathTimer_timeout() -> void:
	emit_signal("wants_path")

# used to stop path tracking if we're stuck for some time
func _on_PathStuckTimer_timeout() -> void:
	on_path_keypoint(body.last_tile, true, true)

# used to periodically check if there are closer players
func _on_CheckTargetTimer_timeout():
# warning-ignore:return_value_discarded
	find_new_target()

# used to exit path over time if the robot is stuck and not changing tile
func _on_ExitPathTimer_timeout():
	clear_path()
