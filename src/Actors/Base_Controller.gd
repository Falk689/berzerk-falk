extends Node2D

class_name Base_Controller

### global vars ###
onready var global_vars               : Node     =  get_node("/root/Global")

# exported variables, not used in code
export (Vector2) var normal_rct_dist  : Vector2  = Vector2(5, 7)                                          # range in tiles to check collisions with other robots
export (Vector2) var berzerk_rct_dist : Vector2  = Vector2(6, 8)                                          # range int tiles to check collisions with other robots in berzerk mode
export (float)   var normal_rc_tick   : float    = 0.2                                                    # time to wait between robot collisions check
export (float)   var berzerk_rc_tick  : float    = 0.1                                                    # time fc wait between robot collisions check while in berzerk
export (float)   var aim_angle        : float    = 45.0                                                   # at what angle we should aim
export (float)   var angle_tolerance  : float    = 5.0                                                    # tolerance for aiming at angles
export (Vector2) var move_tolerance   : Vector2  = Vector2(1, 1)                                          # tolerance for the movement against the player
export (Vector2) var margin           : Vector2  = Vector2(2, 2)                                          # margin to keep from walls 

# variables used in code
var body                      : Node2D                                                                    # our body, used to issue controls
var target                    : Node2D                                                                    # our target
var mapgen                    : Node                                                                      # mapgen class used to check current position and walls
var avoid_l                   : bool              = false                                                 # should we avoid moving left?
var avoid_r                   : bool              = false                                                 # should we avoid moving right?
var avoid_u                   : bool              = false                                                 # should we avoid moving up?
var avoid_d                   : bool              = false                                                 # should we avoid moving down?
var robot_l                   : bool              = false                                                 # there's a robot on the left
var robot_r                   : bool              = false                                                 # there's a robot on the right
var robot_u                   : bool              = false                                                 # there's a robot on us
var robot_d                   : bool              = false                                                 # there's a robot under us
var fire_rate                 : float             = 0   
var rc_tick                   : float             = normal_rc_tick                                        # time between robot collisions checks
var rc_tiles                  : Vector2           = normal_rct_dist                                       # range in tiles of robot collisions check
var rc_cur_tick               : float             = rc_tick                                               # robot fire rate
var additional_lasers         : int               = 0                                                     # how many lasers we add to our difficulty cap

onready var difficulty        : int               = global_vars.difficulty                                # difficulty we were spawned to

onready var max_fr            : float             = global_vars.levels[difficulty][2]                     # max firerate
onready var v_coll            : bool                                                                      # if true we'll try to behave as vanilla as possible in the collision detection department
onready var barrel_align      : bool                                                                      # if true we'll align the aim at our barrels instead of the center of our body
onready var ambidex           : bool                                                                      # if true we'll fire with both arms
onready var avoid_robots      : bool                                                                      # if true we'll try to prevent collisions with other robots
onready var berzerk_lasers    : int                                                                       # extra maximum lasers when in berzerk
onready var current_dir       : int               = global_vars.STOP                                      # current direction
onready var fire_direction    : int               = global_vars.STOP                                      # used to allow fire in a specific direction
onready var last_dir          : int               = global_vars.STOP                                      # last direction

### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass


### common methods ###

func avoid_collisions(_tile : Vector2, _delta : float = 0) -> void:
	pass

# called when the robot is operative and ready to do its dirty job
func body_operative() -> void:
	pass

# start this controller properly take over its body
func start(_new_body : Node2D, _new_map : Node) -> void:
	pass



# we're the last one, go berzerk
func berzerk() -> void:
	rc_cur_tick = 0
	rc_tick     = berzerk_rc_tick
	rc_tiles    = berzerk_rct_dist
	
	if global_vars.levels[difficulty][0] > 0:
		additional_lasers = berzerk_lasers

# selects a new target based on distance
func find_new_target() -> bool:
	var min_dist   : float
	var dist       : float
	var new_target : Node2D
	
	for enemy in get_tree().get_nodes_in_group(global_vars.PLAYER_GROUP):
		if is_instance_valid(enemy) && !enemy.is_queued_for_deletion() && !enemy.dying:
			dist = body.global_position.distance_squared_to(enemy.global_position)
			
			if min_dist == 0 || dist < min_dist:
				min_dist   = dist
				new_target = enemy
	
	if new_target != target:
		target = new_target
		return true
	
	return false

# check for collisions with other robots and avoid them
func avoid_robot_collisions() -> void:
	var b_pos : Vector2  = body.last_tile
	var dist  : Vector2  = Vector2()
	var t_pos : Vector2 
	
	robot_l = false
	robot_r = false
	robot_u = false
	robot_d = false
	
	for c in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		if c != body && is_instance_valid(c) && !c.is_queued_for_deletion():
			t_pos = c.last_tile
			
			dist.x = max(b_pos.x, t_pos.x) - min(b_pos.x, t_pos.x)
			dist.y = max(b_pos.y, t_pos.y) - min(b_pos.y, t_pos.y)
			
			if dist.x > dist.y:
				# we're roughly aligned on the y with the other robot
				if b_pos.y > t_pos.y - rc_tiles.y && b_pos.y < t_pos.y + rc_tiles.y:
					# we're on the right and close
					if b_pos.x > t_pos.x && b_pos.x - t_pos.x < rc_tiles.x:
						robot_l = true
						#print(body.robot_id, " - LEFT")
					
					# we're on the left and close
					elif b_pos.x < t_pos.x && t_pos.x - b_pos.x < rc_tiles.x:
						robot_r = true
						#print(body.robot_id, " - RIGHT")
			
			
			else:
				if b_pos.x > t_pos.x - rc_tiles.x && b_pos.x < t_pos.x + rc_tiles.x:
					# we're under the other robot and close
					if b_pos.y > t_pos.y && b_pos.y - t_pos.y < rc_tiles.y:
						robot_u = true
						#print(body.robot_id, " - UP")
					
					# we're over the other robot and close
					elif b_pos.y < t_pos.y && t_pos.y - b_pos.y < rc_tiles.y:
						robot_d = true
						#print(body.robot_id, " - DOWN")


# are we ready to fire? in what direction?
func can_fire(b_pos : Vector2, t_pos : Vector2) -> int:
	var deg : float
	
	if global_vars.lasers < global_vars.levels[difficulty][0] + additional_lasers:
		
		if b_pos.x + move_tolerance.x > t_pos.x && b_pos.x - move_tolerance.x < t_pos.x:
			if b_pos.y < t_pos.y:
				return global_vars.DN
			
			return global_vars.UP
			
		if b_pos.y + move_tolerance.y > t_pos.y && b_pos.y - move_tolerance.y < t_pos.y:
			if b_pos.x < t_pos.x:
				return global_vars.LX
			
			return global_vars.RX
		
		deg = rad2deg(b_pos.angle_to_point(t_pos))
		
		if deg > 0:
			if deg > aim_angle - angle_tolerance && deg < aim_angle + angle_tolerance:
				return global_vars.LX_UP
			
			elif deg > aim_angle * 3 - angle_tolerance && deg < aim_angle * 3 + angle_tolerance:
				return global_vars.RX_UP
		
		else:
			if deg < -aim_angle + angle_tolerance && deg > -aim_angle - angle_tolerance:
				return global_vars.LX_DN
			
			elif deg < -aim_angle * 3 + angle_tolerance && deg > -aim_angle * 3 - angle_tolerance:
				return global_vars.RX_DN
			
	return global_vars.STOP

# return a custom class
func get_class() -> String:
	return "Controller"

# stop following the path, here for compatibilty reasons
func clear_path() -> void:
	pass
