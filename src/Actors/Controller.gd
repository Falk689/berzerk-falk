extends Base_Controller

# exported variables, not used in code
export (int)     var min_walls    : int     = 2                                                                            # minimum amount of walls to detect to stop moving into a specific direction
export (int)     var max_free_i   : int     = 3                                                                            # maximum direction changes in free mode
export (float)   var max_stuck_t  : float   = 5.0                                                                          # max stuck time
export (float)   var max_stuck_tb : float   = 1.0                                                                          # max stuck time in berzerk
export (Vector2) var margin_x     : Vector2 = Vector2(4, 2)                                                                # area to check on the x axis to detect collisions 
export (Vector2) var margin_y     : Vector2 = Vector2(2, 4)                                                                # area to check on the y axis to detect collisions

# variables used in code
var stuck_t             : float             = 0.0                                                                          # how much time is passed since we were stuck
var free_mode           : bool              = false                                                                        # in free mode, we ignore the player and try to free ourselves
var free_i              : int               = 0                                                                            # how many times we had to change direction in free mode

onready var free_robots : bool              = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "FREE_WHEN_STUCK")[0] # if true we'll try to free ourselves when stuck

### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	move_tolerance   = Vector2(2, 2)                                                                         # tolerance for the movement against the player
	
	v_coll           = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "VANILLA_COL_DETECT")[0]       # if true we'll try to behave as vanilla as possible in the collision detection department
	barrel_align     = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "BARREL_AIM_ALIGN")[0]         # if true we'll align the aim at our barrels instead of the center of our body
	ambidex          = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "AMBIDEXTROUS")[0]             # if true we'll fire with both arms
	avoid_robots     = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "AVOID_ROBOT_COLLS")[0]        # if true we'll try to prevent collisions with other robots
	berzerk_lasers   = global_vars.get_game_config("LEGACY_ROBOT_CONTROLLER", "BERZERK_ADD_LASERS")[0]       # extra maximum lasers when in berzerk


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta : float) -> void:
	var t_pos : Vector2
	var b_pos : Vector2 
	var l_pos : Vector2
	var left  : bool     = false
	var w_dir : int      = global_vars.STOP
	var f_dir : int      = global_vars.STOP
	
	current_dir = global_vars.STOP
	
	if is_instance_valid(body) && body.operative && is_instance_valid(target):
		t_pos = target.global_position
		
		if barrel_align:
			b_pos = body.barrel.global_position
			
			# align at the left barrel
			if ambidex && t_pos.x < b_pos.x:
				left = true
				b_pos.x -= body.barrel.position.x
		
		else:
			b_pos = body.global_position
		
		# fire_rate
		if fire_rate > 0:
			fire_rate -= delta
		
		else:
			# try to get a fire direction
			f_dir = can_fire(b_pos, t_pos)
			
			if ambidex:
				if !barrel_align && t_pos.x < b_pos.x:
					left = true
				
				elif barrel_align && f_dir == global_vars.STOP:
					l_pos = b_pos
					
					# we failed aiming with the left barrel, try right
					if left:
						left = false
						l_pos = Vector2(b_pos.x + body.barrel.position.x, b_pos.y)
					
					# we failed aiming with the right barrel, try left
					else:
						left = true
						l_pos = Vector2(b_pos.x - body.barrel.position.x, b_pos.y)
					
					f_dir = can_fire(l_pos, t_pos)
			
			# we have a target, fire
			if f_dir != global_vars.STOP:
				fire_rate = max_fr
				
				body.fire(f_dir, left)
		
		avoid_collisions(mapgen.map.world_to_map(b_pos), delta)
		
		if !free_mode:
			if b_pos.x + move_tolerance.x > t_pos.x && b_pos.x - move_tolerance.x > t_pos.x:
				if !avoid_l && !robot_l:
					current_dir = global_vars.LX
				
				else:
					w_dir       = global_vars.LX
			
			elif b_pos.x + move_tolerance.x < t_pos.x && b_pos.x - move_tolerance.x < t_pos.x:
				if !avoid_r && !robot_r:
					current_dir = global_vars.RX
				
				else:
					w_dir       = global_vars.RX
			
			if b_pos.y + move_tolerance.y < t_pos.y && b_pos.y - move_tolerance.y < t_pos.y:
				if !avoid_d && !avoid_l && !robot_d && !robot_l && current_dir == global_vars.LX:
					current_dir = global_vars.LX_DN
				
				elif !avoid_d && !avoid_r && !robot_d && !robot_r && current_dir == global_vars.RX:
					current_dir = global_vars.RX_DN
			
				elif !avoid_d && !robot_d:
					current_dir = global_vars.DN
				
				elif (avoid_d || robot_d) && w_dir == global_vars.LX:
					w_dir       = global_vars.LX_DN
				
				elif (avoid_d || robot_d) && w_dir == global_vars.RX:
					w_dir       = global_vars.RX_DN
			
			
			if b_pos.y + move_tolerance.y > t_pos.y && b_pos.y - move_tolerance.y > t_pos.y:
				if !avoid_u && !avoid_l && !robot_u && !robot_l && current_dir == global_vars.LX:
					current_dir = global_vars.LX_UP
				
				elif !avoid_u && !avoid_r && !robot_u && !robot_r && current_dir == global_vars.RX:
					current_dir = global_vars.RX_UP
				
				elif !avoid_u && !robot_u:
					current_dir = global_vars.UP
				
				elif (avoid_u || robot_u) && w_dir == global_vars.LX:
					w_dir       = global_vars.LX_UP
				
				elif (avoid_u || robot_u) && w_dir == global_vars.RX:
					w_dir       = global_vars.RX_UP
			
			if current_dir == global_vars.STOP && free_robots && (avoid_robots || (w_dir == global_vars.RX_UP || w_dir == global_vars.LX_UP || w_dir == global_vars.RX_DN || w_dir == global_vars.LX_DN)):
				stuck_t += delta
				
				if stuck_t >= max_stuck_t:
					free_mode = true
					stuck_t   = 0
					free_i    = 0
			
			else:
				stuck_t       = 0
		
		
		# try to unstuck yourself
		if free_mode:
			if (avoid_d || robot_d) && !avoid_u && !robot_d:
				current_dir = global_vars.UP
			
			elif (avoid_u || robot_u) && !avoid_d && !robot_d:
				current_dir = global_vars.DN
			
			if (avoid_l || robot_l) && !avoid_r && !robot_r:
				if current_dir == global_vars.UP:
					current_dir = global_vars.RX_UP
				
				elif current_dir == global_vars.DN:
					current_dir = global_vars.RX_UP
				
				else:
					current_dir = global_vars.RX
			
			elif (avoid_r || robot_r) && !avoid_l && !robot_l:
				if current_dir == global_vars.UP:
					current_dir = global_vars.LX_UP
				
				elif current_dir == global_vars.DN:
					current_dir = global_vars.LX_UP
				
				else:
					 current_dir = global_vars.LX
			
			if !avoid_d && !avoid_u && !avoid_l && !avoid_r && !robot_d && !robot_u && !robot_l && !robot_r:
				free_mode = false
			
			if current_dir != last_dir:
				free_i += 1
				
				if free_i >= max_free_i:
					free_mode = false
			
		
		body.move(current_dir, delta)
		
		last_dir = current_dir


### common methods ###

# compatibility method with the advanced controller
func body_operative() -> void:
	find_new_target()

# start this controller properly take over its body
func start(new_body : Node2D, new_map : Node) -> void:
	# setting up main vars
	body         = new_body
	mapgen       = new_map
	
	# setting up main body vars
	body.controller = self


# we're the last one, go berzerk
func berzerk() -> void:
	max_stuck_t       = max_stuck_tb
	
	if global_vars.levels[difficulty][0] > 0:
		additional_lasers = berzerk_lasers


# Check if we can move in the next tile
func avoid_collisions(tile : Vector2, delta : float = 0) -> void:
	var t_pos    : Vector2    = Vector2()
	var t_pos2   : Vector2    = Vector2()
	var c_cells  : Dictionary = {}
	var aX_cells : int        = 0
	var aY_cells : int        = 0
	
	#pos    = mapgen.world_to_map(pos)
	
	# check only once per tile
	if tile == body.last_tile:
		
		if avoid_robots:
			if rc_cur_tick > 0:
				rc_cur_tick -= delta
			
			else:
				rc_cur_tick = rc_tick
				avoid_robot_collisions()
		
		return
	
	body.last_tile = tile
	
	avoid_l = false
	avoid_r = false
	avoid_d = false
	avoid_u = false
	
	if avoid_robots:
		rc_cur_tick = rc_tick
		avoid_robot_collisions()
	
	for x in range(margin_x.x):
		t_pos.x  = tile.x - x
		t_pos2.x = tile.x + x
		
		c_cells[x]  = []
		c_cells[-x] = []
		
		for y in range(-margin_x.y, margin_x.y + 1):
			
			t_pos.y  = tile.y + y
			t_pos2.y = tile.y + y
			
			#print("y: ", y, " X: ", t_pos, " ", t_pos2, " POS: ", pos)
			
			if mapgen.map.get_cell(t_pos.x, t_pos.y) > -1:
				if v_coll:
					c_cells[x].append(t_pos)
				
				else:
					avoid_l = true
			
			
			if mapgen.map.get_cell(t_pos2.x, t_pos2.y) > -1:
				if v_coll:
					c_cells[-x].append(t_pos2)
				
				else:
					avoid_r = true
	
	
	# vanilla collision, don't see 1 cell columns
	if v_coll:
		for x in c_cells:
			if len(c_cells[x]) > 1:
				
				if x > 0:
					avoid_l = true
				
				else:
					avoid_r = true
			
			# improve corner detection checking a cross around single blocks
			elif len(c_cells[x]) == 1:
				aX_cells = 0
				aY_cells = 0
				
				# if one block along the x and y axis is another wall, consider it a wall and avoid
				for aN in range(-1, 2):
					if aN != 0:
						
						if mapgen.map.get_cell(c_cells[x][0].x + aN, c_cells[x][0].y) > -1:
							aX_cells += 1
						
						if mapgen.map.get_cell(c_cells[x][0].x, c_cells[x][0].y + aN) > -1:
							aY_cells += 1
				
				# detected a corner, avoid
				if aX_cells > 0 && aY_cells > 0:
					if x > 0:
						avoid_l = true
					
					else:
						avoid_r = true
	
	c_cells  = {}
	
	for y in range(margin_y.y):
		t_pos.y   = tile.y - y
		t_pos2.y  = tile.y + y
		
		c_cells[y]  = []
		c_cells[-y] = []
		
		for x in range(-margin_y.x, margin_y.x + 1):
			
			t_pos.x  = tile.x + x
			t_pos2.x = tile.x + x
			
			if mapgen.map.get_cell(t_pos.x, t_pos.y) > -1:
				if v_coll:
					c_cells[y].append(t_pos)
				
				else:
					avoid_u = true
				
			if mapgen.map.get_cell(t_pos2.x, t_pos2.y) > -1:
				if v_coll:
					c_cells[-y].append(t_pos2)
				
				else:
					avoid_d = true
	
	# vanilla collision, don't see 1 cell columns
	if v_coll:
		for y in c_cells:
			if len(c_cells[y]) > 1:
				
				if y > 0:
					avoid_u = true
				
				else:
					avoid_d = true
			
			# improve corner detection checking a cross around single blocks
			elif len(c_cells[y]) == 1:
				aX_cells = 0
				aY_cells = 0
				
				# if one block along the x and y axis is another wall, consider it a wall and avoid
				for aN in range(-1, 2):
					if aN != 0:
						
						if mapgen.map.get_cell(c_cells[y][0].x + aN, c_cells[y][0].y) > -1:
							aX_cells += 1
						
						if mapgen.map.get_cell(c_cells[y][0].x, c_cells[y][0].y + aN) > -1:
							aY_cells += 1
				
				# detected a corner, avoid
				if aX_cells > 0 && aY_cells > 0:
					if y > 0:
						avoid_u = true
					
					else:
						avoid_d = true
