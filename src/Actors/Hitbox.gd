extends KinematicBody2D

### signals ###
signal hit(instigator, id)                   # fired when something hit us

# fire the hit signal
func hit(instigator : Node, id : int = -1):
	emit_signal("hit", instigator, id)
