extends Node2D

class_name Evil_Otto

### global vars ###
onready var global_vars               : Node  = get_node("/root/Global")

### vars used in code ###
var bounce_frame_start                : float = 1.6                # when to start showing the bounce frame during the tween
var bounce_frame_end                  : float = 0.3                # when to stop showing the bounce frame during the tween
var operative                         : bool  = false              # are we ready to kill?
var controller                        : Otto_Controller            # current controller class


onready var bounce_amount             : int      = global_vars.get_game_config("OTTO", "BOUNCE_AMOUNT")[0]         # how far should otto bounce high
onready var bounce_speed              : int      = global_vars.get_game_config("OTTO", "NORMAL_BOUNCE_SPEED")[0]   # multiplier to tweak bounce speed a bit so it looks more natural
onready var b_bounce_speed            : int      = global_vars.get_game_config("OTTO", "BERZERK_BOUNCE_SPEED")[0]  # berzerk bounce speed
onready var speed                     : int      = global_vars.get_game_config("OTTO", "NORMAL_SPEED")[0]          # current otto speed
onready var use_shaders               : bool     = global_vars.get_user_config("VISUAL", "USE_SHADERS")[0]         # should we use shaders?
onready var move_dir                  : int      = global_vars.STOP                                                # current move direction
onready var shader_material           : Material = $Visual/AnimatedSprite.material                                 # shader material
onready var visual                    : Node2D   = $Visual                                                         # visual node, in case we use shaders we need this to get its position

### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	var velocity : Vector2
	
	if move_dir == global_vars.RX:
		velocity.x += 1
	
	elif move_dir == global_vars.LX:
		velocity.x -= 1
	
	elif move_dir == global_vars.DN:
		velocity.y += 1
	
	elif move_dir == global_vars.UP:
		velocity.y -= 1
	
	elif move_dir == global_vars.RX_UP:
		velocity.x += 1
		velocity.y -= 1
	
	elif move_dir == global_vars.RX_DN:
		velocity.x += 1
		velocity.y += 1
	
	elif move_dir == global_vars.LX_UP:
		velocity.x -= 1
		velocity.y -= 1
	
	elif move_dir == global_vars.LX_DN:
		velocity.x -= 1
		velocity.y += 1
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	
	position += velocity * delta


### common methods ###


# just update the move variable
func move(dir : int) -> void:
	move_dir = dir


# setup otto and give it a proper position and color
func start(pos : Vector2, robots_left : int, col : Color = $AnimatedSprite.modulate) -> void:
	position                                   = pos
	
	if use_shaders:
		$Visual/AnimatedSprite.material.set_shader_param("u_main_color", col)
		$Visual/AnimatedSprite.material.set_shader_param("u_position", Vector2.ZERO)
		$Visual/AnimatedSprite.material.set_shader_param("u_distance", 50.0)
	
	else:
		$Visual/AnimatedSprite.modulate            = col
	
	$Visual/Detector/CollisionShape2D.set_deferred("disabled", false)
	$Visual/Hitbox/CollisionShape2D.set_deferred("disabled", false)
	
	if robots_left <= 0:
		berzerk()
	
	$Visual/AnimatedSprite.animation = "spawn"
	$Visual/AnimatedSprite.play()
	show()
	
	yield($Visual/AnimatedSprite, "animation_finished")
	
	operative = true
	
	$Visual/AnimatedSprite.animation = "bounce"
	$Visual/AnimatedSprite.frame     = 1
	$Visual/AnimatedSprite.stop()
	
	start_bouncing()


# stop, change color and go to sleep waiting to be peacefully terminated
func freeze(color : Color) -> void:
	$Tween.stop_all()
	$Visual/AnimatedSprite.stop()
	move_dir                                   = global_vars.STOP
	$Visual/AnimatedSprite.modulate            = color
	$Visual/Detector/CollisionShape2D.set_deferred("disabled", true)
	$Visual/Hitbox/CollisionShape2D.set_deferred("disabled", true)
	operative                                  = false

# there are no robots lefts, go berzerk
func berzerk() -> void:
	speed                              = global_vars.get_game_config("OTTO", "BERZERK_SPEED")[0]
	bounce_speed                       = b_bounce_speed
	$Tween.playback_speed              = b_bounce_speed

### custom methods ###


# start bouncing
func start_bouncing() -> void:
	$Tween.playback_speed = bounce_speed
	$Tween.interpolate_property($Visual, "position", Vector2(0, 0), Vector2(0, -bounce_amount), 1, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Visual, "position", Vector2(0, -bounce_amount), Vector2(0, 0), 1, Tween.TRANS_SINE, Tween.EASE_IN, 1)
	$Tween.start()


### signals ###


# otto basically should destroy everything except the map without colliding with anything
func hit(body : Node2D, _id : int = -1) -> void:
	if body.is_in_group(global_vars.ROBOT_GROUP):
		body.explode(self)
	
	elif body.is_in_group(global_vars.PLAYER_GROUP):
		body.die()

# toggle animation based on tween elpsed time
func _on_Tween_tween_step(_object : Object, _key : NodePath, elapsed : float, _value : Object) -> void:
	if elapsed > bounce_frame_start || elapsed < bounce_frame_end:
		$Visual/AnimatedSprite.frame = 1
	
	else:
		$Visual/AnimatedSprite.frame = 0
