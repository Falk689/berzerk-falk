extends Node2D

class_name Otto_Controller

### global vars ###
onready var global_vars              : Node    = get_node("/root/Global")

# configuration vars
onready var use_shaders               : bool   = global_vars.get_user_config("VISUAL", "USE_SHADERS")[0]  # should we use shaders?

# exported variables, not used in code
export (Vector2) var move_tolerance  : Vector2 = Vector2(2, 2)                                            # tolerance for the movement against the player

# variables used in code
var body                             : Node2D                                                             # our body, used to issue controls
var target                           : Node2D                                                             # our target


### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta : float) -> void:
	var t_pos : Vector2
	var b_pos : Vector2
	var dir   : int      = global_vars.STOP
	
	if is_instance_valid(body) && body.operative && is_instance_valid(target):
		t_pos = target.global_position
		b_pos = body.global_position
		
		if use_shaders:
			body.shader_material.set_shader_param("u_position", t_pos - body.visual.global_position)
			body.shader_material.set_shader_param("u_distance", body.visual.global_position.distance_to(t_pos))
		
		b_pos.y -= body.bounce_amount / 2.0
		
		# movement and collision detection
		
		if b_pos.x + move_tolerance.x > t_pos.x && b_pos.x - move_tolerance.x > t_pos.x:
			dir = global_vars.LX
		
		elif  b_pos.x + move_tolerance.x < t_pos.x && b_pos.x - move_tolerance.x < t_pos.x:
			dir = global_vars.RX
		
		if  b_pos.y + move_tolerance.y < t_pos.y && b_pos.y - move_tolerance.y < t_pos.y:
			if dir == global_vars.LX:
				dir = global_vars.LX_DN
				
			elif dir == global_vars.RX:
				dir = global_vars.RX_DN
			
			else:
				dir = global_vars.DN
		
		if b_pos.y + move_tolerance.y > t_pos.y && b_pos.y - move_tolerance.y > t_pos.y:
			if dir == global_vars.LX:
				dir = global_vars.LX_UP
				
			elif dir == global_vars.RX:
				dir = global_vars.RX_UP
			
			else:
				dir = global_vars.UP
		
		body.move(dir)

# selects a new target based on distance
func find_new_target() -> void:
	var min_dist   : float
	var dist       : float
	var new_target : Node2D
	
	for enemy in get_tree().get_nodes_in_group(global_vars.PLAYER_GROUP):
		if is_instance_valid(enemy) && !enemy.is_queued_for_deletion() && !enemy.dying:
			dist = body.global_position.distance_squared_to(enemy.global_position)
			
			if min_dist == 0 || dist < min_dist:
				min_dist   = dist
				new_target = enemy
	
	if use_shaders && is_instance_valid(new_target) && !new_target.is_queued_for_deletion():
		body.shader_material.set_shader_param("u_overlay_color", new_target.get_color())
	
	target = new_target
