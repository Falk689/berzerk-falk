extends KinematicBody2D

class_name Human_Player


### constants ###

const STOP : Vector2 = Vector2(0, 0)                                                                       # stop movement


### signals ###

signal hit                                                                                                 # triggered when hit by something, in this game everything kills you
signal dead                                                                                                # triggered at the end of deathtimer
signal ammo_update (player, wasted_ammo)                                                                   # triggered on fire and ammo pickup when limited ammo are enabled

### global vars ###

onready var global_vars              : Node = get_node("/root/Global")
onready var max_player_ammo          : int  = global_vars.get_game_config("PLAYER", "MAX_PLAYER_AMMO")[0]  # maximum amount of ammo the player can carry
### exported vars, not edited by code ###
export (PackedScene) var Laser_Class : PackedScene                                                         # our laser class

var speed                            : int   = 50                                                          # character speed
var fire_rate                        : float = 0.5                                                         # weapon fire rate, you can ignore fire_deadzone on this value
var laser_speed                      : int   = 220                                                         # speed or our laser
var spr_color                        : Color = Color(0, 1, 0)                                              # sprite color
var vm_multi                         : float = 0.9                                                         # vanilla movement multiplier
var next_fire_time                   : float = 0                                                           # if this is zero, it means we can fire, resets to fire_rate value after fire
var ready                            : bool  = false                                                       # we're not ready to die yet
var laser_color                      : Color = spr_color                                                   # current laser color
var player_id                        : int   = 0                                                           # identifier into the players array
var friendly_fire                    : bool  = false                                                       # is friendly fire active?
var player_colls                     : bool  = false                                                       # are collisions between players active?
var actual_fire_rate                 : float = fire_rate                                                   # automatically takes into account fire_deadzone
var electrified_walls                : bool  = false                                                       # are the walls deadly to us?
var dying                            : bool  = false                                                       # used to make sure enemies won't keep aiming at us when we're about to die
var fire_deadzone                    : float                                                               # additional wait time, used to ease diagonal fire, resets to default_fire_deadzone
var default_deadzone                 : float                                                               # deadzone default value, used to reset deadzone once we fired
var limited_ammo                     : bool                                                                # if true, our ammo are limited
var ammo                             : int                                                                 # current ammo amount
var last_pickup                      : Vector2                                                             # position of our last ammo pickup
var current_ammo_pack                : Ammo_Pack                                                           # ammo pack we're standing on while full


onready var death_timer              : Timer = $DeathTimer                                                 # our death timer to despawn us on death

### standard methods ###

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Hitbox/CollisionShape2D.set_deferred("disabled", false)
	$CollisionDetector/CollisionShape2D.set_deferred("disabled", false)
	hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta : float) -> void:
	var velocity  : Vector2 = Vector2.ZERO
	var fire_pos  : Vector2
	var fire      : int
	var fire_rot  : float
	var laser     : Single_Laser
	var t_speed   : float
	var collision : KinematicCollision2D
	
	if (!ready):
		return
	
	# reducing firerate
	if (next_fire_time > 0):
		next_fire_time -= delta
	
	# fire mechanics
	if Input.is_action_pressed(global_vars.controls[player_id][global_vars.FIRE]):
		velocity = Vector2.ZERO
		$AnimatedSprite.flip_h = false
		
		# selecting direction based on how many keys are pressed
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.RX]):
			fire = global_vars.RX
			#$AnimatedSprite.flip_h = false
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.LX]):
			if fire == global_vars.RX:
				fire = global_vars.STOP
			
			else:
				fire = global_vars.LX
				#AnimatedSprite.flip_h = true
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.UP]):
			if fire == global_vars.RX:
				fire = global_vars.RX_UP
			
			elif fire == global_vars.LX:
				fire = global_vars.LX_UP
			
			else:
				fire = global_vars.UP
				#$AnimatedSprite.flip_h = false
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.DN]):
			if fire == global_vars.RX:
				fire = global_vars.RX_DN
			
			elif fire == global_vars.LX:
				fire = global_vars.LX_DN
			
			else:
				fire = global_vars.DN
				#$AnimatedSprite.flip_h = false
		
		# doing stuff with the result, trigger an animation
		if fire > 0:
			if fire == global_vars.RX:
				fire_pos = $BarrelSideR.global_position
				fire_rot = $BarrelSideR.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.RX - 1
			
			elif fire == global_vars.LX:
				fire_pos = $BarrelSideL.global_position
				fire_rot = $BarrelSideL.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.LX - 1
				
			elif fire == global_vars.RX_UP:
				fire_pos = $BarrelDiagUpR.global_position
				fire_rot = $BarrelDiagUpR.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.RX_UP - 1
			
			elif fire == global_vars.LX_UP:
				fire_pos = $BarrelDiagUpL.global_position
				fire_rot = $BarrelDiagUpL.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.LX_UP - 1
			
			elif fire == global_vars.UP:
				fire_pos = $BarrelUp.global_position
				fire_rot = $BarrelUp.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.UP - 1
			
			elif fire == global_vars.RX_DN:
				fire_pos = $BarrelDiagDownR.global_position
				fire_rot = $BarrelDiagDownR.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.RX_DN - 1
			
			elif fire == global_vars.LX_DN:
				fire_pos = $BarrelDiagDownL.global_position
				fire_rot = $BarrelDiagDownL.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.LX_DN - 1
			
			else:
				fire_pos = $BarrelDown.global_position
				fire_rot = $BarrelDown.rotation
				
				$AnimatedSprite.animation = "fire"
				$AnimatedSprite.frame     = global_vars.DN - 1
				
			$AnimatedSprite.stop()
			
			
			# limit fire to the firerate
			if next_fire_time <= 0:
				# use deadzone so it's easier to fire diagonally
				if fire_deadzone > 0:
					fire_deadzone -= delta
				
				# fire! add a laser
				elif can_fire():
					next_fire_time   = actual_fire_rate
					fire_deadzone    = default_deadzone
					laser            = Laser_Class.instance()
					
					#laser.add_to_group(global_vars.LASER_GROUP)
					laser.instigator = self
					laser.ignore     = $Hitbox
					
					if global_vars.vibration:
						Input.stop_joy_vibration(player_id)
						Input.start_joy_vibration(player_id, 1.0, 0, 0.2)
					
					get_parent().add_child(laser)
					laser.fire(laser_speed, laser_color, self, fire_pos, fire_rot, player_id)
					$FireSound.play()
					
					if is_instance_valid(current_ammo_pack) && !current_ammo_pack.is_queued_for_deletion():
						current_ammo_pack.do_pickup(self)
				
				# just update deadzone
				else:
					fire_deadzone    = default_deadzone
					next_fire_time   = actual_fire_rate
					
					if global_vars.vibration:
						Input.stop_joy_vibration(player_id)
						Input.start_joy_vibration(player_id, 0.8, 0, 0.2)
					
					$ClickSound.play()
		
		# if the player presses weird combination of keys, just stand still
		else:
			$AnimatedSprite.animation = "walk"
			$AnimatedSprite.stop()
			$AnimatedSprite.frame = 0
	
	# movement mechanics
	else:
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.RX]):
			velocity.x += 1
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.LX]):
			velocity.x -= 1
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.DN]):
			velocity.y += 1
		
		if Input.is_action_pressed(global_vars.controls[player_id][global_vars.UP]):
			velocity.y -= 1
			
		if velocity.length() > 0:
			if velocity.x != 0:
				t_speed = speed
			
			else:
				t_speed = speed * vm_multi
			
			velocity = velocity.normalized() * t_speed
			$AnimatedSprite.play()
		
		else:
			$AnimatedSprite.animation = "walk"
			$AnimatedSprite.stop()
			$AnimatedSprite.frame = 0
		
		#linear_velocity = velocity
		
		if velocity.x != 0 || velocity.y != 0:
			$AnimatedSprite.animation = "walk"
			$AnimatedSprite.flip_v = false
			
			$AnimatedSprite.flip_h = velocity.x < 0
	
	if electrified_walls:
		collision = move_and_collide(velocity * delta)
		
		if collision and is_instance_valid(collision.collider):
			die()
	
	else:
		velocity = move_and_slide(velocity)


### common methods ###


# called on level start to set our position and show us
func start(pos : Vector2, spd : int, v_mov : float, f_rate : float, l_spd : int, f_fire : bool, p_colls : bool, e_walls : bool, f_dz : float, l_ammo : bool, i_ammo : int) -> void:
	position           = pos
	speed              = spd
	vm_multi           = v_mov
	fire_rate          = f_rate
	laser_speed        = l_spd
	electrified_walls  = e_walls
	default_deadzone   = f_dz
	fire_deadzone      = f_dz
	actual_fire_rate   = fire_rate - fire_deadzone
	friendly_fire      = f_fire
	player_colls       = p_colls
	limited_ammo       = l_ammo
	ammo               = i_ammo
	$CollisionShape2D.set_deferred("disabled", false)
	$AnimatedSprite.animation = "walk"
	$AnimatedSprite.stop()
	$AnimatedSprite.frame     = 0
	$AnimatedSprite.modulate  = spr_color
	show()
	ready                     = true

# mostly a compatibility method so we can just freeze everything but changes the color of the sprite too
func freeze(color : Color) -> void:
	ready = false
	$AnimatedSprite.modulate = color
	$AnimatedSprite.stop()

# change the sprite color
func change_color(color : Color) -> void:
	$AnimatedSprite.modulate = color
	spr_color = color

# return sprite color
func get_color() -> Color:
	return spr_color

# add ammo
func add_ammo(amount : int, point : Vector2 = Vector2(-689, -689)) -> void:
	var wasted_ammo : int 
	
	if ammo + amount > max_player_ammo:
		wasted_ammo = ammo + amount - max_player_ammo
		ammo        = max_player_ammo
	
	else:
		ammo       += amount
	
	last_pickup = point
	
	emit_signal("ammo_update", self, wasted_ammo)
	
	if point.x != -689 && !$AmmoSound.playing:
		$AmmoSound.play()


### custom methods ###

func can_fire() -> bool:
	if limited_ammo:
		if ammo > 0:
			ammo -= 1
			emit_signal("ammo_update", self, 0)
			return true
		
		return false
	
	return true

# set laser color
func change_laser_color(color : Color) -> void:
	laser_color = color


# usually happens at the very end of life
func die(_instigator : Node2D = null, _player_id : int = -1) -> void:
	if !ready:
		return
	
	if global_vars.vibration:
		Input.stop_joy_vibration(player_id)
		Input.start_joy_vibration(player_id, 0, 1, 1.0)
	
	ready    = false
	dying    = true
	$AnimatedSprite.modulate = Color(1, 1, 1)
	$AnimatedSprite.animation = "die"
	$AnimatedSprite.play()
	emit_signal("hit")
	$CollisionShape2D.set_deferred("disabled", true)
	$Hitbox/CollisionShape2D.set_deferred("disabled", true)
	$CollisionDetector/CollisionShape2D.set_deferred("disabled", true)

# stop animation
func stop_anim() -> void:
	$AnimatedSprite.stop()

### signals ###

# delete us after we died
func _on_DeathTimer_timeout():
	emit_signal("dead")
	queue_free()

# detect collisions with robots and other players
func _on_CollisionDetector_body_entered(body : Node) -> void:
	if is_instance_valid(body) && !body.is_queued_for_deletion() && body != self:
		if body.is_in_group(global_vars.ROBOT_GROUP):
			die()
		
		if player_colls and body.is_in_group(global_vars.PLAYER_GROUP):
			body.die()
			die()
