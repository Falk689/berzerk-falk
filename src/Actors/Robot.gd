class_name Standard_Robot

extends KinematicBody2D

### signals ###
signal enemy_hit                                               # triggered when hit by something

### global vars ###
onready var global_vars = get_node("/root/Global")

### exported global vars, not edited by code ###
export (PackedScene) var Laser_Class                           # our laser class

### global vars used by code ###
var controller             : Base_Controller                                     # current controller class

var last_tile              : Vector2                                             # last tile our controller checked for collisions
var laser_speed            : int                                                 # laser speed
var electrified_walls      : bool                                                # are walls deadly to us?
var operative              : bool                                                # status, all green
var in_berzerk             : bool                                                # are we the last one?
var points_awarded         : bool                                                # have we already awarded points to the player?
var started                : bool                                                # has the function start initialized stuff?
var wants_teleport         : bool                                                # this robot wants to teleport to another location
var exploding              : bool                                                # are we already exploding?
var robot_id               : int                                                 # robot id, mainly used for debug purposes

var killer                 : Node2D                                              # who killed us
var killer_id              : int                                                 # player_id of our killer

onready var difficulty     : int             = global_vars.difficulty            # difficulty we were spawned to
onready var barrel         : Node2D          = $Barrel                           # our barrel, exposed to the controller to aim better
onready var hitbox         : KinematicBody2D = $Hitbox                           # our hitbox accessed by the controller when casting a ray
onready var speed          : int             = global_vars.levels[difficulty][3] # robot walk speed
onready var berzerk_speed  : int             = global_vars.levels[difficulty][4] # robot berzerk speed
onready var ba_speed_s     : float           = global_vars.levels[difficulty][6] # berzerk animation speed scale

onready var vertical_multi : float           = global_vars.get_game_config("ROBOT", "V_MOVE_SPEED")[0]

### standard methods ###

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	
	# setting up laser speed
	if global_vars.levels[difficulty][1]:
		laser_speed = global_vars.get_game_config("ROBOT", "FAST_LASER_SPEED")[0]
	
	else:
		laser_speed = global_vars.get_game_config("ROBOT", "SLOW_LASER_SPEED")[0]
	
	$AnimatedSprite.modulate    = global_vars.levels[difficulty][7]
	
	if started:
		$AnimatedSprite.speed_scale = global_vars.levels[difficulty][5]
	
	hide()

### common enemy methods ###

func move(dir : int, delta : float) -> void:
	var velocity  : Vector2              = Vector2.ZERO
	var collision : KinematicCollision2D
	
	if !operative || dir == global_vars.STOP:
		$AnimatedSprite.animation = "stop"
	
	elif dir == global_vars.RX:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = false
		velocity.x = 1
	
	elif dir == global_vars.LX:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = true
		velocity.x = -1
	
	elif dir == global_vars.RX_UP:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = false
		velocity.y = -1
		velocity.x = 1
	
	elif dir == global_vars.LX_UP:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = true
		velocity.y = -1
		velocity.x = -1
	
	elif dir == global_vars.UP:
		$AnimatedSprite.animation = "walkUp"
		velocity.y = -1
	
	elif dir == global_vars.RX_DN:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = false
		velocity.y = 1
		velocity.x = 1
	
	elif dir == global_vars.LX_DN:
		$AnimatedSprite.animation = "walkSide"
		$AnimatedSprite.flip_h = true
		velocity.y = 1
		velocity.x = -1
	
	elif dir == global_vars.DN:
		$AnimatedSprite.animation = "walkDown"
		velocity.y = 1
		
	if velocity.length() > 0:
		if velocity.x == 0:
			speed *= vertical_multi
		
		velocity = velocity.normalized() * speed
	
	$AnimatedSprite.play()
	
	if electrified_walls:
		collision = move_and_collide(velocity * delta)
		
		if collision and is_instance_valid(collision.collider):
			explode(collision.collider)
	
	else:
		velocity = move_and_slide(velocity)

# setup this robot giving it position, speed, color and firerate
func start(pos : Vector2, brzk : bool, delay : float = 0) -> void:
	# setting up main vars
	position                                     = pos
	
	if delay == 0:
		started                                  = true
		
		# enabling collisions
		$CollisionShape2D.set_deferred("disabled", false)
		$Hitbox/CollisionShape2D.set_deferred("disabled", false)

		# playing the first animation
		$AnimatedSprite.animation = "stop"
		$AnimatedSprite.play()

		# now we just have to start the timer and go visible
		$OperativeTimer.wait_time = global_vars.levels[difficulty][2]
		$OperativeTimer.start()
		
		$AnimatedSprite.modulate    = global_vars.levels[difficulty][7]
		
		if brzk:
			berzerk()
		
	
	else:
		$Hitbox/CollisionShape2D.set_deferred("disabled", false)
		
		if delay > 0:
			$OperativeTimer.wait_time = delay
			$OperativeTimer.start()
			
			$AnimatedSprite.speed_scale = 1.0 / delay
			$AnimatedSprite.animation   = "teleport"
			$AnimatedSprite.play()
		
		else:
			_on_OperativeTimer_timeout()
		
		in_berzerk = brzk
	
	show()


# teleport to a specified location
func teleport(pos : Vector2, delay : float) -> void:
	started                     = false
	operative                   = false
	wants_teleport              = false
	
	if is_instance_valid(controller):
		controller.clear_path()
	
	position = pos
	
	if delay > 0:
		$CollisionShape2D.set_deferred("disabled", true)
		
		$OperativeTimer.wait_time   = delay
		$OperativeTimer.start()
	
		$AnimatedSprite.speed_scale = 1.0 / delay
		
		$AnimatedSprite.animation   = "teleport"
		$AnimatedSprite.play()


# stop, change color and go to sleep waiting to be peacefully terminated
func freeze(color : Color) -> void:
	$AnimatedSprite.modulate   = color
	$AnimatedSprite.stop()
	$CollisionShape2D.set_deferred("disabled", true)
	operative                  = false
	$OperativeTimer.stop()

# we're the last one, go berzerk
func berzerk() -> void:
	in_berzerk = true
	speed      = berzerk_speed
	$AnimatedSprite.speed_scale = ba_speed_s
	
	if is_instance_valid(controller):
		controller.berzerk()

# return sprite color
func get_color() -> Color:
	return $AnimatedSprite.modulate

### custom methods ###

# fire a laser, math resides in the controller
func fire(dir : int, left : bool = false) -> void:
	var fire_pos : Vector2       = $Barrel.global_position
	var laser    : Single_Laser  = Laser_Class.instance()
	var fire_cor : Vector2       = Vector2()
	var fire_rot : float
	
	fire_rot = $Barrel.rotation
	
	if dir == global_vars.RX:
		fire_rot += PI
	
	elif dir == global_vars.RX_UP:
		fire_rot -= PI / 4
	
	elif dir == global_vars.LX_UP:
		fire_rot -= PI
		fire_rot += PI / 4
	
	elif dir == global_vars.UP:
		fire_rot -= PI / 2
	
	elif dir == global_vars.DN:
		fire_rot += PI / 2
	
	elif dir == global_vars.RX_DN:
		fire_rot += PI / 4
		
	elif dir == global_vars.LX_DN:
		fire_rot -= PI
		fire_rot -= PI / 4
	
	# fire a shot from your left barrel
	if left:
		fire_cor = $Barrel.position
		fire_pos.x -= fire_cor.x * 2
	
	laser.instigator = self
	laser.ignore     = $Hitbox
	get_parent().add_child(laser)
	laser.add_to_group(global_vars.ENEMY_LASER_GROUP)
	laser.fire(laser_speed, $AnimatedSprite.modulate, self, fire_pos, fire_rot)
	
	$FireSound.play()


# go kaboom and disappear
func explode(cause : Node2D, player_id : int = -1) -> void:
	if !exploding:
		exploding = true
		
		$ExplodeSound.play()
		$AnimatedSprite.speed_scale = 1
		
		if global_vars.vibration:
			for idx in Input.get_connected_joypads():
				Input.stop_joy_vibration(idx)
				Input.start_joy_vibration(idx, 0, 0.8, 0.3)
		
		if !$OperativeTimer.is_stopped():
			$OperativeTimer.stop()
		
		killer         = cause
		killer_id      = player_id
		operative      = false
		
		if !points_awarded:
			points_awarded = true
			emit_signal("enemy_hit")
		
		$AnimatedSprite.animation = "explode"
		$AnimatedSprite.play()
		yield($AnimatedSprite, "animation_finished")
		
		$CollisionShape2D.set_deferred("disabled", true)
		$Hitbox/CollisionShape2D.set_deferred("disabled", true)
		
		queue_free()


### signals ###

# used to set a slight breather when the map starts
func _on_OperativeTimer_timeout() -> void:
	$AnimatedSprite.speed_scale = global_vars.levels[difficulty][5]
	
	if !started:
		$AnimatedSprite.modulate    = global_vars.levels[difficulty][7]
		
		# enabling collisions
		$CollisionShape2D.set_deferred("disabled", false)
		$Hitbox/CollisionShape2D.set_deferred("disabled", false)

		# playing the first animation
		$AnimatedSprite.animation = "stop"
		$AnimatedSprite.play()
		
		if in_berzerk:
			berzerk()
	
	operative = true
	
	if is_instance_valid(controller):
			controller.body_operative()

# used to explode on collisions with other robots
func _on_RobotDetector_body_entered(body : Node) -> void:
	if is_instance_valid(body) && !body.is_queued_for_deletion():
		
		if body != self && body.is_in_group(global_vars.ROBOT_GROUP):
			body.explode(self)
			explode(body)
		
		# workaround for robots not exploding on electified walls
		if electrified_walls && body.is_in_group("TileMaps"):
			explode(self)
