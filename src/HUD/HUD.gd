extends CanvasLayer

### exported vars ###
export (String)  var bonus_str               : String   = tr("BONUS") + ": "                    # bonus message string
export (Vector2) var life_size               : Vector2  = Vector2(35, 35)                       # life icon size
export (Vector2) var medium_life_size        : Vector2  = Vector2(32, 32)                       # medium life icon size
export (Vector2) var small_life_size         : Vector2  = Vector2(20, 20)                       # compact life icon size
export (int)     var max_big_lives           : int      = 5                                     # maximum amount of lives to use the standard icon size with
export (int)     var max_medium_lives        : int      = 12                                    # maximum amount of lives to use the medium icon size with
export (int)     var max_lives               : int      = 14                                    # maximum amount of lives to show
export (int)     var life_columns            : int      = 5                                     # how many columns we normally have in the lives grid
export (int)     var medium_life_columns     : int      = 6                                     # how many columns we have when medium lives are used
export (int)     var small_life_columns      : int      = 7                                     # how many columns we have when small lives are used

onready var global_vars         : Node =  get_node("/root/Global")

var player_one_countdown                     : bool                                             # if true we're updating a countdown, don't update points for player one
var player_two_countdown                     : bool                                             # if true we're updating a countdown, don't updtae points for player two

### preloaded items ###
var life : StreamTexture = preload("res://assets/art/hud/life-icon.png")

### HUD elements ###
onready var left_label       : Label         = $Main/BottomGrid/LeftLabel                       # used for player one score and countdowns in normal mode and player two lives in compact mode
onready var right_label      : Label         = $Main/BottomGrid/RightLabel                      # used for player two score and countdowns

onready var left_lives       : GridContainer = $Main/BottomGrid/LeftLives                       # player one lives in normal mode, not used in compact mode
onready var right_lives      : GridContainer = $Main/BottomGrid/RightLives/RightLivesGrid       # player two lives in normal mode, not used in compact mode

onready var left_ammo_label  : Label         = $Main/BottomGrid/LeftAmmoLabel                   # player one ammo label in normal mode, not used in compact mode
onready var right_ammo_label : Label         = $Main/BottomGrid/RightAmmoLabel                  # player two ammo label

onready var left_ammo_icon   : TextureRect   = $Main/BottomGrid/AmmoIconLeftCont/AmmoIconLeft   # player one ammo label in normal mode, hidden when not used and in compact mode
onready var right_ammo_icon  : TextureRect   = $Main/BottomGrid/AmmoIconRightCont/AmmoIconRight # player two ammo label, hidden when not needed
onready var top_ammo_icon    : TextureRect   = $Main/TopGrid/AmmoCont/AmmoIcon                  # top ammo icon, used in compact mode for player one when needed

onready var message_label    : Label         = $Main/BottomGrid/MessageLabel                    # multi-purpose message label, by now only used to show bonuses

onready var top_left_label   : Label         = $Main/TopGrid/LeftLabel                          # used in compact mode to show player one score and countdowns
onready var top_right_label  : Label         = $Main/TopGrid/RightLabel                         # used to show player one lives in compact mode
onready var top_ammo_label   : Label         = $Main/TopGrid/AmmoLabel                          # used to show player one ammo in compact mode, when needed

onready var bottom_grid      : GridContainer = $Main/BottomGrid                                 # main grid, used for the normal interface
onready var top_grid         : GridContainer = $Main/TopGrid                                    # normally hidden, used to display player one info in compact mode


### other vars ###
var player_one_color         : Color                                                            # stored player one color
var player_two_color         : Color                                                            # stored player two color

var player_two_score_ph      : String        = ""                                               # player two score placeholder
var zero_text                : String        = "0"                                              # zero used as placeholder for score labels

var compact_hud              : bool                                                             # if true, we're using the compact hud


# update player score
func update_score(score : Dictionary, clear_cd : bool = false) -> void:
	if clear_cd:
		player_one_countdown = false
		player_two_countdown = false
	
	if !player_one_countdown:
		if global_vars.P1 in score:
			if compact_hud:
				top_left_label.text = str(score[global_vars.P1])
			
			else:
				left_label.text = str(score[global_vars.P1])
		
		elif !compact_hud:
			left_label.text = zero_text
		
		else:
			top_left_label.text = zero_text
	
	if !player_two_countdown:
		if global_vars.P2 in score:
			right_label.text = str(score[global_vars.P2])
		
		else:
			right_label.text = player_two_score_ph


func update_count_down(p_id : int, count : int) -> void:
	if p_id == global_vars.P1:
		player_one_countdown = true
		
		if compact_hud:
			top_left_label.text  = str(count)
		
		else:
			left_label.text = str(count)
	
	else:
		player_two_countdown = true
		
		right_label.text = str(count)
	
	

# show how much bonus we got cleaning this room
func show_bonus(bonus : int) -> void:
	message_label.text = str(bonus_str, bonus)


# hide bonus message
func hide_bonus() -> void:
	message_label.text = ""


# set interface color taking into account compact interface
func set_hud_color(mod : Color, player_id : int) -> void:
	if player_id == global_vars.P1:
		player_one_color = mod
		
		if compact_hud:
			top_left_label.add_color_override("font_color", mod)
			top_right_label.add_color_override("font_color", mod)
			top_ammo_label.add_color_override("font_color", mod)
			top_ammo_icon.modulate = mod
		
		else:
			left_label.add_color_override("font_color", mod)
			left_ammo_label.add_color_override("font_color", mod)
			left_ammo_icon.modulate = mod
	
	else:
		player_two_color = mod
		
		right_label.add_color_override("font_color", mod)
		right_ammo_label.add_color_override("font_color", mod)
		right_ammo_icon.modulate = mod
		
		if compact_hud:
			left_label.add_color_override("font_color", mod)


# update lives interface
func update_lives(lives : Dictionary) -> void:
	var life_icon   : TextureRect
	var cur_grid    : GridContainer
	var life_color  : Color
	var lives_count : int
	var used_size   : Vector2
	
	for player_id in lives:
		
		if compact_hud:
			if player_id == global_vars.P1:
				top_right_label.text = str(lives[player_id])
			
			else:
				left_label.text      = str(lives[player_id])
		
		else:
			if player_id == global_vars.P1:
				cur_grid    = left_lives
				life_color  = player_one_color
			
			else:
				cur_grid    = right_lives
				life_color  = player_two_color
			
			lives_count = cur_grid.get_child_count()
			
			used_size = life_size
			
			if lives[player_id] > max_medium_lives:
				used_size        = small_life_size
				cur_grid.columns = small_life_columns
			
			elif lives[player_id] > max_big_lives:
				used_size        = medium_life_size
				cur_grid.columns = medium_life_columns
			
			else:
				cur_grid.columns = life_columns
			
			for c in cur_grid.get_children():
				c.set_custom_minimum_size(used_size)
			
			while lives_count < max_lives && lives[player_id] > lives_count:
				life_icon = TextureRect.new()
				
				life_icon.texture = life
				life_icon.set_stretch_mode(TextureRect.STRETCH_KEEP_ASPECT_CENTERED)
				life_icon.size_flags_vertical   = life_icon.SIZE_EXPAND_FILL
				life_icon.set_custom_minimum_size(used_size)
				
				cur_grid.add_child(life_icon)
				life_icon.modulate              = life_color
				
				lives_count += 1
			
			
			while lives[player_id] < lives_count && lives_count > 0:
				lives_count -= 1
				life_icon    = cur_grid.get_child(lives_count)
				
				if is_instance_valid(life_icon) && !life_icon.is_queued_for_deletion():
					life_icon.queue_free()


# show ammo interface
func show_ammo(amount : int) -> void:
	if compact_hud:
		top_ammo_icon.show()
		
		top_ammo_label.text = str(amount)
	
	else:
		left_ammo_icon.show()
		
		left_ammo_label.text = str(amount)
	
	
	if global_vars.num_players > 1:
		right_ammo_icon.show()
		
		right_ammo_label.text = str(amount)


# update ammo count
func update_ammo(player_id : int, ammo : int) -> void:
	if player_id == global_vars.P1:
		if compact_hud:
			top_ammo_label.text = str(ammo)
		
		else:
			left_ammo_label.text = str(ammo)
	
	else:
		right_ammo_label.text = str(ammo)


# go in split_screen mode
func split_screen() -> void:
	
	compact_hud = true
	
	top_grid.show()
	
	top_left_label.add_color_override("font_color", player_one_color)
	top_right_label.add_color_override("font_color", player_one_color)
	
	left_label.add_color_override("font_color", player_two_color)
	
	bottom_grid.margin_top = -top_grid.margin_bottom
