extends Control

### constants ###

enum {SOUND,
	  MUSIC,
	  VOICE}

const VALUE_X     : String  = "xVal"
const VALUE_Y     : String  = "yVal"
const JOYPAD      : String  = "_Joypad"
const KEYBOARD    : String  = "_Keyboard"
const TIP_NAME    : String  = "_tip"
const EDIT_TIP    : String  = "editable_tip"
const DESC_TIP    : String  = "editable_description_tip"
const SELECT_TEXT : String  = "CUSTOM"
const P1_GRID     : String  = "player_one_controls"
const P2_GRID     : String  = "player_two_controls"
const SHOW_TXT    : String  = "SAVE"
const HIDE_TXT    : String  = "HIDE"
const SAVED_TXT   : String  = "SAVE_MESSAGE"
const REMOVED_TXT : String  = "REMOVE_MESSAGE"
const ERROR_TXT   : String  = "SAVE_ERROR"
const SAVE_TXT    : String  = "SAVE"
const REMOVE_TXT  : String  = "REMOVE"

### exported vars ###

export (String)              var main_scene        : String   = "res://src/Main/Main.tscn"                    # main scene path, used to start a new game
export (String)              var hi_scores_scene   : String   = "res://src/HUD/ViewHighScores.tscn"           # hi scores scene path, so we can check them from the main menu
export (String)              var b_font_path       : String   = "res://assets/theme/fonts/Atlantis-hs.tres"   # path for the font we're going to use for custom options buttons
export (String)              var o_font_path       : String   = "res://assets/theme/fonts/Atlantis.tres"      # path for the font we're going to use for custom options
export (String)              var t_font_path       : String   = "res://assets/theme/fonts/Atlantis-s.tres"    # path for the font we're going to use for tips
export (String)              var s_font_path       : String   = "res://assets/theme/fonts/Atlantis-us.tres"   # path for the ultra small font
export (Color)               var section_color     : Color    = Color(0, 1, 0)                                # color for custom options sections
export (Color)               var option_color      : Color    = Color(1, 1, 1)                                # color for custom options sections
export (Color)               var tip_color         : Color    = Color(1, 1, 0)                                # color for custom options tips
export (int)                 var s_increase_limit  : int      = 100                                           # slow increase limit, when increase_ticks is higher than this, use fast increase
export (float)               var slow_increase     : float    = 0.02                                          # slow increase timer wait_time
export (float)               var fast_increase_f   : float    = 0.1                                           # minimum float increase value for fast increase
export (float)               var fast_increase_i   : float    = 10                                            # minimum int increase value for fast increase 

### resources ###

var option_font                                    : Resource = load(o_font_path)                              # font used for custom options
var button_font                                    : Resource = load(b_font_path)                              # font used for custom options
var tip_font                                       : Resource = load(t_font_path)                              # font used for custom options tips
var small_font                                     : Resource = load(s_font_path)                              # font used for custom options tips

var set_volume                                     : float                                                     # used to set volume only on confirm
var set_music                                      : float                                                     # used to set music volume on confirm
var set_voice                                      : float                                                     # used to set voices volume on confirm
var set_locale                                     : String                                                    # used to set locale only on confirm
var settings_populated                             : bool                                                      # used to populate settings just once
var language_changed                               : bool                                                      # used to update custom game modes only when needed

### global vars ###

onready var global_vars                            : Node     =  get_node("/root/Global")

### vars used in code ###

var last_positive                                  : bool     = false                                          # last button pressed was positive
var last_section                                   : String   = ""                                             # stored last section we tweaked
var last_config_name                               : String   = ""                                             # stored last config_name we tweaked
var last_idx                                       : int      = 0                                              # stored last index we tweaked
var last_game_config                               : bool     = false                                          # stored last game_config state to properly trigger inputs with the time
var repeat                                         : bool     = false                                          # used to reliably stop repeat timer
var mouse_hidden                                   : bool     = true                                           # used to check if the mouse was locally hidden
var set_vibration                                  : bool     = false                                          # used to set vibration on confirm
var increase_ticks                                 : int      = 0                                              # how many times the custom game mode timer increased a single value
var min_increase_f                                 : float    = 0                                              # currently used minimum increase value for floats
var min_increase_i                                 : int      = 0                                              # currently used minimum increase value for ints
var groups_to_hide                                 : Array    = []                                             # groups, not yet created, to hide according selected inputs
var controls_titles                                : Array    = ["ACTION", "KEYBOARD", "JOYPAD"]               # titles used in the controls grids
var custom_base                                    : VBoxContainer                                             # base node of the custom menu
var custom_scroll                                  : ScrollContainer                                           # currently used scroll container
var custom_selector                                : OptionButton                                              # currently used game_mode selector
var edit_action                                    : String                                                    # currently edited action
var control_button                                 : Button                                                    # currently used control edit button
var control_list                                   : Array                                                     # current action list
var control_index                                  : int                                                       # current action index 0 is for keyboard keys, 1 for joypad
var block_scroll                                   : bool                                                      # if true don't scroll to focus
var current_controls                               : Dictionary                                                # current controls dictionary, used to undo edits
var songs                                          : Array                                                     # songs array
var diff_keys                                      : Array                                                     # difficulty keys, used to track score difficulty editing
var multi_custom                                   : bool                                                      # are we editing a custom multiplayer game? used to return to the proper menu on difficulty editing
var current_levels                                 : Dictionary                                                # current difficulty levels, waiting to be sent to the global class 
var temp_levels                                    : Dictionary                                                # used to store temporary levels during custom game mode selection
var save_button                                    : Button                                                    # stored save button to re-enable it after saving
var save_line                                      : LineEdit                                                  # stored save line to clear it after displaying the message
var new_mode_name                                  : String                                                    # stored new mode name to be added to the interface
var stored_diff_key                                : int                                                       # currently edited difficulty key
var stored_b_state                                 : bool                                                      # stored state of the save button
var game_modes                                     : Array                                                     # stored game modes, used to delete one from outside the selector
var set_fullscreen                                 : bool                                                      # used to set fullscreen on confirm

### methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var config_result : bool = true
	
	VisualServer.set_default_clear_color(global_vars.BG_COLOR)
	
	# hide mouse cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	# initialize configuration
	if !global_vars.read_config:
		config_result = load_configs()
		global_vars.load_game_modes()
		global_vars.read_config = true
		
		# maintain retro-compatibility with the older version
		if global_vars.config.get_value("input", "vibration") != null:
			set_vibration         = global_vars.config.get_value("input", "vibration")
			global_vars.vibration = set_vibration
		
		else:
			global_vars.config.set_value("input", "vibration", true)
		
		$ToggleMusic/MusicCheckbox.pressed = set_music > 0
	
	# set default configuration even if the file doesn't exist
	if global_vars.read_config || !config_result:
		$ToggleMusic/MusicCheckbox.pressed = global_vars.play_music
		
		set_locale    = TranslationServer.get_locale()
		
		set_volume    = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Sounds"))
		set_volume    = db2linear(set_volume)
		
		set_music     = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
		set_music     = db2linear(set_music)
		
		set_voice     = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Voices"))
		set_voice     = db2linear(set_voice)
	
	# the configuration file doesn't exist, enable music output
	if !config_result:
		$ToggleMusic/MusicCheckbox.pressed         = set_music > 0
	
	set_vibration = global_vars.vibration
	$Controls/VibrationInput/Vibration.set_pressed_no_signal(set_vibration)
	
	# show the proper menu
	if global_vars.game_over:
		show_game_over()
	
	else:
		show_main_menu()
	
	randomize()
	
	
	set_process_unhandled_key_input(false)
	set_process_unhandled_input(false)


# used to remap joypad input
func _unhandled_input(event):
	if event is InputEventKey && event.pressed:
		control_button.pressed = false
		control_button.grab_focus()
	
	elif event is InputEventJoypadButton && event.pressed:
		remap_action_to(event)


# used to remap keyboard input
func _unhandled_key_input(event):
	if event.pressed:
		remap_action_to(event)


### base visual methods ###

func play_next_song():
	
	if len(songs) == 0:
		for c in $Music.get_children():
			songs.append(c)
		
		global_vars.song_index = randi() % len(songs)
	
	global_vars.song_index += 1
	
	if global_vars.song_index >= len(songs):
		global_vars.song_index = 0
	
	songs[global_vars.song_index].play()

# hide all submenus
func hide_submenus() -> void:
	for c in get_children():
		if c.name != "ToggleMusic":
			c.hide()


# show single player menu
func show_single_player() -> void:
	hide_submenus()
	$SinglePlayer.show()
	$SinglePlayer/SingleClassic.grab_focus()

# show single player menu
func show_multiplayer() -> void:
	hide_submenus()
	$MultiPlayer.show()
	$MultiPlayer/MultiClassic.grab_focus()

# show game over menu
func show_game_over() -> void:
	global_vars.game_over = false
	hide_submenus()
	$GameOver.show()
	$GameOver/GameOverRetry.grab_focus()


# show main menu
func show_main_menu() -> void:
	hide_submenus()
	$Main.show()
	$Main/SinglePlayer.grab_focus()
	
	global_vars.game_mode = global_vars.CLASSIC


### new game or new scene methods ###


# start new hardcoded game
func start_game(game_mode : int) -> void:
	global_vars.select_game_mode(game_mode)
	global_vars.reset_vars(true)
	get_tree().change_scene(main_scene)


# start custom single player game
func start_new_custom(game_mode : int) -> void:
	global_vars.reset_vars(true)
	start_game(game_mode)


# view high scores
func view_hiscores(multi : bool = false) -> void:
	if !multi:
		get_tree().change_scene(hi_scores_scene)


### custom game mode configuration methods ###


# show new custom game menu
func show_new_custom(multiplayer : bool = false) -> void:
	var main          : Node
	var focused       : Node
	var b_game_modes  : OptionButton
	
	hide_submenus()
	
	if multiplayer:
		main          = $MultiCustom
		b_game_modes  = $MultiCustom/CenterTop/GameModes
		focused       = $MultiCustom/Center/MCStart
		custom_base   = $MultiCustom/MCScroll/MCOptions
		custom_scroll = $MultiCustom/MCScroll
		save_button   = $MultiCustom/CenterTop/Save
		
	
	else:
		main          = $SingleCustom
		b_game_modes  = $SingleCustom/CenterTop/GameModes
		focused       = $SingleCustom/Center/SCStart
		custom_base   = $SingleCustom/SCScroll/SCOptions
		custom_scroll = $SingleCustom/SCScroll
		save_button   = $SingleCustom/CenterTop/Save
	
	#global_vars.reset_default_game_config()
	
	multi_custom = multiplayer
	
	if len(b_game_modes.items) == 0:
		populate_custom_selector(b_game_modes, multiplayer)
	
	if custom_base.get_child_count() == 0:
		populate_custom_section(custom_base, true, multiplayer)
	
	main.show()
	focused.grab_focus()


# add a new section to a specified configuration container
func add_custom_section(container : Node, txt : String) -> void:
	var new_section  = Label.new()
	
	new_section.text = tr(txt)
	
	new_section.size_flags_horizontal = SIZE_EXPAND_FILL
	new_section.align                 = HALIGN_CENTER
	
	new_section.add_font_override("font", option_font)
	new_section.add_color_override("font_color", section_color)
	
	new_section.add_to_group(txt)
	
	container.add_child(new_section)


# add a new custom configuration input to a specified container
func add_custom_input(container : Node, section : String, c_name : String, options : Array, game_config : bool) -> void:
	var h_cont         : HBoxContainer
	var c_lab          : Label
	var o_input        : Node
	var tip_label      : Label
	var tip_cont       : VBoxContainer
	var tip_text       : String
	var desc_text_add  : String
	var opts_list      : Array
	var n              : int
	var margin_value   : int = 15
	
	if len(options) == 0:
		return
	
	h_cont = HBoxContainer.new()
	c_lab  = Label.new()
	
	# configure the horizontal container
	h_cont.size_flags_horizontal = SIZE_EXPAND_FILL
	h_cont.name                  = section + c_name
	h_cont.add_to_group(section)
	
	# configure the config name label
	c_lab.text = tr(c_name)
	c_lab.size_flags_horizontal = SIZE_EXPAND_FILL
	c_lab.add_font_override("font", option_font)
	c_lab.add_color_override("font_color", option_color)
	
	# add the label to the container
	h_cont.add_child(c_lab)
	
	# adding the actual inputs
	if typeof(options[1]) == TYPE_VECTOR2:
		c_lab = Label.new()
		c_lab.add_font_override("font", option_font)
		c_lab.add_color_override("font_color", option_color)
		c_lab.text = "X:"
		
		h_cont.add_child(c_lab)
		
		o_input = Label.new()
		o_input.text = String(options[0].x)
		o_input.name = VALUE_X
		
		o_input.add_font_override("font", option_font)
		o_input.add_color_override("font_color", option_color)
		
		h_cont.add_child(o_input)
		
		add_n_tweak_controls(h_cont, section, c_name, 0, game_config)
		
		c_lab = Label.new()
		c_lab.add_font_override("font", option_font)
		c_lab.add_color_override("font_color", option_color)
		c_lab.text = "Y:"
		
		h_cont.add_child(c_lab)
		
		o_input = Label.new()
		o_input.text = String(options[0].y)
		o_input.name = VALUE_Y
		
		h_cont.add_child(o_input)
		
		add_n_tweak_controls(h_cont, section, c_name, 1, game_config)
		
		tip_text = tr(options[5])
	
	# the options needs to be a float or percent
	elif typeof(options[1]) == TYPE_REAL:
		o_input = Label.new()
		
		if options[6] == "%":
			o_input.text = String(options[0] * 100) + "%"
		
		else:
			o_input.text = String(options[0])
		
		o_input.name = VALUE_X
		
		h_cont.add_child(o_input)
		add_n_tweak_controls(h_cont, section, c_name, 0, game_config)
		
		tip_text = tr(options[5])
	
	# since we're working with a bool, we'll need way less stuff
	elif typeof(options[1]) == TYPE_BOOL:
		o_input = Label.new()
		h_cont.add_child(o_input)
		add_checkbox(h_cont, section, c_name, options[0], game_config)
		
		tip_text = tr(options[2])
	
	
	elif typeof(options[1]) == TYPE_INT && typeof(options[2]) == TYPE_INT:
		o_input = Label.new()
		o_input.name = VALUE_X
		o_input.text = String(options[0])
		h_cont.add_child(o_input)
		add_n_tweak_controls(h_cont, section, c_name, 0, game_config)
		
		tip_text = tr(options[5])
	
	elif typeof(options[1]) == TYPE_STRING || (typeof(options[1]) == TYPE_INT && typeof(options[2]) == TYPE_ARRAY):
		opts_list = []
		n         = 2
		
		o_input = OptionButton.new()
		o_input.align = HALIGN_RIGHT
		o_input.flat  = true
		
		o_input.name  = VALUE_X
		
		while n < len(options):
			if typeof(options[n]) == TYPE_ARRAY:
				o_input.add_item(options[n][0])
				opts_list.append(options[n])
				
				if options[n][2] != null && !options[0] in options[n] && !options[n][2] in groups_to_hide && len(options[n]) > 2 && !hide_group(options[n][2]):
					groups_to_hide.append(options[n][2])
				
				elif options[0] in options[n]:
					desc_text_add = tr(options[n][3])
				
				# select proper option
				if options[n][1] == options[0]:
					o_input.select(n - 2)
			
			elif typeof(options[n]) == TYPE_STRING:
				tip_text = tr(options[n])
			
			n += 1
		
		o_input.connect("item_selected", self, "_on_ItemList_item_activated", [section, c_name, opts_list, game_config])
		o_input.connect("focus_entered", self, "_on_Input_focus_entered", [o_input])
		
		h_cont.add_child(o_input)
	
	elif typeof(options[1]) == TYPE_COLOR:
		o_input = ColorPickerButton.new()
		o_input.text = "Color"
		
		if game_config:
			o_input.set_pick_color(global_vars.game_config[section][c_name][0])
		
		else:
			o_input.set_pick_color(global_vars.user_config[section][c_name][0])
			
		o_input.get_picker().presets_enabled = false
		o_input.get_picker().presets_visible = false
		
		o_input.connect("color_changed", self, "_on_ColorPicker_Changed", [section, c_name, game_config])
		
		o_input.connect("focus_entered", self, "_on_Input_focus_entered", [o_input])
		
		o_input.get_picker().set_deferred_mode(true)
		
		tip_text = tr(options[2])
		
		h_cont.add_child(o_input)
	
	else:
		print("UNKNOWN: ", options)
	
	if is_instance_valid(o_input):
		o_input.add_font_override("font", option_font)
		o_input.add_color_override("font_color", option_color)
	
	container.add_child(h_cont)
	
	if len(tip_text) > 0:
		tip_cont  = VBoxContainer.new()
		tip_label = Label.new()
		
		tip_cont.name = section + c_name + TIP_NAME
		
		tip_cont.size_flags_horizontal = SIZE_EXPAND_FILL
		tip_cont.add_to_group(section)
		
		tip_cont.set("custom_constants/margin_right", margin_value)
		
		tip_label.text = tip_text
		tip_label.autowrap = true
		tip_label.add_font_override("font", tip_font)
		tip_label.add_color_override("font_color", tip_color)
		
		tip_cont.add_child(tip_label)
		
		if len(desc_text_add) > 0:
			tip_label          = Label.new()
			tip_label.name     = DESC_TIP
			tip_label.text     = desc_text_add
			tip_label.autowrap = true
			tip_label.add_font_override("font", tip_font)
			tip_label.add_color_override("font_color", tip_color)
			tip_cont.add_child(tip_label)
		
		container.add_child(tip_cont)

func add_difficulty_section(container : Node, score : int, below : Node = self) -> GridContainer:
	var grid  : GridContainer = GridContainer.new()
	var t_lab : Label
	var line  : LineEdit
	
	grid.columns               = 10
	grid.size_flags_horizontal = SIZE_EXPAND_FILL
	
	for title in global_vars.levels_keys:
		t_lab                       = Label.new()
		t_lab.text                  = title
		t_lab.align                 = HALIGN_CENTER
		t_lab.size_flags_horizontal = SIZE_EXPAND_FILL
		t_lab.add_font_override("font", small_font)
		t_lab.add_color_override("font_color", section_color)
		grid.add_child(t_lab)
	
	line      = LineEdit.new()
	line.text = String(score)
	line.name = VALUE_X
	
	if score > 0:
		line.connect("text_changed", self, "_on_Score_Line_text_changed", [line])
		line.connect("text_entered", self, "_on_Score_Line_text_entered", [line])
		line.connect("focus_entered", self, "_on_Score_Line_focus_entered", [line])
		line.connect("focus_exited", self, "_on_Score_Line_focus_exited", [line])
	
	else:
		line.editable = false
	
	line.add_font_override("font", small_font)
	
	grid.add_child(line)
	
	if below == self:
		container.add_child(grid)
	
	else:
		container.add_child_below_node(below, grid)
	
	return grid

func add_difficulty_input(container : Node, index : int, option : Array) -> void:
	var is_float : bool = typeof(option[0]) == TYPE_REAL
	
	var line     : LineEdit
	var check    : CheckButton
	var color    : ColorPickerButton
	
	if typeof(option[0]) == TYPE_INT || is_float:
		line = LineEdit.new()
		line.text = String(option[0])
		line.add_font_override("font", small_font)
		
		line.connect("text_changed", self, "_on_Diff_Line_text_changed", [line, is_float])
		line.connect("text_entered", self, "_on_Diff_Line_text_entered", [line, index, is_float])
		line.connect("focus_exited", self, "_on_Diff_Line_focus_exited", [line, index, is_float])
		
		container.add_child(line)
	
	elif typeof(option[0]) == TYPE_BOOL:
		check         = CheckButton.new()
		check.pressed = option[0]
		
		check.connect("toggled", self, "_on_Diff_Checkbox_toggled", [check, index])
		
		container.add_child(check)
	
	elif typeof(option[0]) == TYPE_COLOR:
		color      = ColorPickerButton.new()
		color.text = "Color"
		
		color.set_pick_color(option[0])
		color.add_font_override("font", small_font)
		
		color.connect("color_changed", self, "_on_Diff_ColorPicker_Changed", [color, index])
		
		container.add_child(color)
	
	else:
		print("UNKNOWN: ", option[0])

# add a remove button for this difficulty level
func add_difficulty_add_remove(grid : GridContainer, index : int) -> void:
	var add_button : Button        = Button.new()
	var cont       : HBoxContainer = HBoxContainer.new()
	var line       : LineEdit      = grid.get_child(len(global_vars.levels_keys))
	
	var rem_button : Button
	
	cont.size_flags_horizontal = SIZE_EXPAND_FILL
	
	add_button.add_font_override("font", small_font)
	add_button.text = "+"
	add_button.size_flags_horizontal = SIZE_EXPAND_FILL
	
	add_button.connect("pressed", self, "_on_Diff_Add_pressed", [line])
	
	if index > 0:
		rem_button = Button.new()
		
		rem_button.size_flags_horizontal = SIZE_EXPAND_FILL
		rem_button.add_font_override("font", small_font)
		rem_button.text = "-"
		
		rem_button.connect("pressed", self, "_on_Diff_Remove_pressed", [line])
		
		cont.add_child(rem_button)
	
	cont.add_child(add_button)
	grid.add_child(cont)


# add a new checkbox to toggle a bool
func add_checkbox(container : Node, section : String, c_name : String, state : bool, game_config : bool) -> void:
	var c_box  : CheckButton = CheckButton.new()
	
	c_box.pressed = state
	
	c_box.name = VALUE_X
	
	c_box.connect("toggled", self, "_on_Checkbox_toggled", [section, c_name, game_config])
	
	c_box.connect("focus_entered", self, "_on_Input_focus_entered", [c_box])
	
	container.add_child(c_box)


# add tweak controls to tweak numbers
func add_n_tweak_controls(container : Node, section : String, c_name : String, idx : int, game_config : bool) -> void:
	var p_btn  : Button = Button.new()
	var m_btn  : Button = Button.new()
	
	p_btn.text = "+"
	m_btn.text = "-"
	
	p_btn.add_font_override("font", button_font)
	p_btn.add_color_override("font_color", option_color)
	p_btn.flat = true
	
	p_btn.connect("button_down", self, "_on_Plus_pressed", [section, c_name, idx, game_config])
	p_btn.connect("button_up", self, "_on_PlusMinus_released")
	p_btn.connect("focus_entered", self, "_on_Input_focus_entered", [p_btn])
	
	m_btn.add_font_override("font", button_font)
	m_btn.add_color_override("font_color", option_color)
	m_btn.connect("focus_entered", self, "_on_Input_focus_entered", [m_btn])
	m_btn.flat = true
	
	m_btn.connect("button_down", self, "_on_Minus_pressed", [section, c_name, idx, game_config])
	m_btn.connect("button_up", self, "_on_PlusMinus_released")
	
	container.add_child(m_btn)
	container.add_child(p_btn)


# redirect requests to proper functions - TODO: find a better way to handle this
func change_config_value(section : String, config_name : String, idx : int, positive : bool, game_config : bool) -> void:
	if game_config:
		change_game_config_value(section, config_name, idx, positive)
	
	else:
		change_user_config_value(section, config_name, idx, positive)


# change value of a specific game_config configuration
func change_game_config_value(section : String, config_name : String, idx: int, positive : bool) -> void:
	var i_value       : int
	var f_value       : float
	
	var i_min_value   : int
	var f_min_value   : float
	
	var i_max_value   : int
	var f_max_value   : float
	
	var i_incr        : int
	var f_incr        : float
	
	var max_key       : String
	var min_key       : String
	
	var percent       : bool
	var vect          : bool
	
	var cont          : Node    = custom_base.get_node(section + config_name)
	var input_name    : String
	var r_precision   : String
	var f_precision   : float
	
	if idx == 0:
		input_name = VALUE_X
	
	else:
		input_name = VALUE_Y
	
	# work with a 2d vector, start by assigning an int or float value to proper variables and proceed
	if typeof(global_vars.game_config[section][config_name][1]) == TYPE_VECTOR2:
			vect = true
			
			if idx == 0:
				f_value = global_vars.game_config[section][config_name][0].x
			
			else:
				f_value = global_vars.game_config[section][config_name][0].y
	
	# work with a float, a float coordinate of a vector or a percentual
	if vect || typeof(global_vars.game_config[section][config_name][1]) == TYPE_REAL:
		if !vect:
			r_precision = global_vars.game_config[section][config_name][6]
			f_value     = global_vars.game_config[section][config_name][0]
		
		# value should be percentual, pass it to the int processor
		if r_precision == "%":
			# needed else conversion goes quite wrong
			f_value = stepify(f_value * 100.0, 1)
			i_value = int(f_value)
			
			percent = true
		
		# value should be a float, work with it in place
		else:
			if !vect:
				f_value     = global_vars.game_config[section][config_name][0]
			
			f_incr      = float(global_vars.game_config[section][config_name][2])
			f_precision = f_incr
			f_incr      = max(f_incr, min_increase_f)
			
			# getting min_value
			if !vect && typeof(global_vars.game_config[section][config_name][3]) == TYPE_STRING:
				min_key = global_vars.game_config[section][config_name][3]
				
				if min_key in global_vars.game_config[section]:
					f_min_value = global_vars.game_config[section][min_key][0]
				
				else:
					f_min_value = global_vars.game_config[section][config_name][0]
			
			else:
				f_min_value = global_vars.game_config[section][config_name][3]
			
			# getting max value
			if !vect && typeof(global_vars.game_config[section][config_name][4]) == TYPE_STRING:
				max_key = global_vars.game_config[section][config_name][4]
				
				if max_key in global_vars.game_config[section]:
					f_max_value = global_vars.game_config[section][max_key][0]
				
				else:
					f_max_value = global_vars.game_config[section][config_name][0]
			
			else:
				f_max_value = global_vars.game_config[section][config_name][4]
			
			
			# capping float value
			f_value     = cap_to_float_value(f_value, f_incr, f_min_value, f_max_value, section, min_key, max_key, positive, true)
			f_value     = stepify(f_value, f_precision)
			
			if !vect:
				global_vars.game_config[section][config_name][0] = f_value
				cont.get_node(input_name).text                   = String(f_value)
	
	
	# work with an int or a percentual
	if percent || typeof(global_vars.game_config[section][config_name][1]) == TYPE_INT:
		if !percent:
			i_value = global_vars.game_config[section][config_name][0]
		
		i_incr  = global_vars.game_config[section][config_name][2]
		
		if i_incr == 1:
			i_incr = max(i_incr, min_increase_i)
		
		# getting min_value
		if typeof(global_vars.game_config[section][config_name][3]) == TYPE_STRING:
			min_key = global_vars.game_config[section][config_name][3]
			
			if min_key in global_vars.game_config[section]:
				i_min_value = global_vars.game_config[section][min_key][0]
			
			else:
				i_min_value = global_vars.game_config[section][config_name][0]
		
		else:
			i_min_value = global_vars.game_config[section][config_name][3]
		
		# getting max value
		if typeof(global_vars.game_config[section][config_name][4]) == TYPE_STRING:
			max_key = global_vars.game_config[section][config_name][4]
			
			if max_key in global_vars.game_config[section]:
				i_max_value = global_vars.game_config[section][max_key][0]
			
			else:
				i_max_value = global_vars.game_config[section][config_name][0]
		
		else:
			i_max_value = global_vars.game_config[section][config_name][4]
		
		# capping int value
		i_value = cap_to_int_value(i_value, i_incr, i_min_value, i_max_value, section, min_key, max_key, positive, true)
		
		if !percent:
			global_vars.game_config[section][config_name][0] = i_value
			cont.get_node(input_name).text                   = String(i_value)
	
	# we got a percentual value processed, work with it
	if percent:
		cont.get_node(input_name).text                   = String(i_value) + "%"
		f_value = float(i_value) * 0.01
		global_vars.game_config[section][config_name][0] = f_value
	
	# keep working with vectors
	if vect:
		if idx == 0:
			global_vars.game_config[section][config_name][0].x = f_value
			cont.get_node(input_name).text                     = String(f_value)
		
		else:
			global_vars.game_config[section][config_name][0].y = f_value
			cont.get_node(input_name).text                     = String(f_value)
		


# change value of a specific user_config configuration
func change_user_config_value(section : String, config_name : String, idx: int, positive : bool) -> void:
	var i_value       : int
	var f_value       : float
	
	var i_min_value   : int
	var f_min_value   : float
	
	var i_max_value   : int
	var f_max_value   : float
	
	var i_incr        : int
	var f_incr        : float
	
	var max_key       : String
	var min_key       : String
	
	var percent       : bool
	var vect          : bool
	
	var cont          : Node    = custom_base.get_node(section + config_name)
	
	var input_name    : String
	var r_precision   : String
	var f_precision   : float
	
	if idx == 0:
		input_name = VALUE_X
	
	else:
		input_name = VALUE_Y
	
	# work with a 2d vector, start by assigning an int or float value to proper variables and proceed
	if typeof(global_vars.user_config[section][config_name][1]) == TYPE_VECTOR2:
			vect = true
			
			if idx == 0:
				f_value = global_vars.user_config[section][config_name][0].x
			
			else:
				f_value = global_vars.user_config[section][config_name][0].y
	
	# work with a float, a float coordinate of a vector or a percentual
	if vect || typeof(global_vars.user_config[section][config_name][1]) == TYPE_REAL:
		if !vect:
			r_precision = global_vars.user_config[section][config_name][6]
			f_value = global_vars.user_config[section][config_name][0]
		
		# value should be percentual, pass it to the int processor
		if r_precision == "%":
			# needed else conversion goes quite wrong
			f_value = stepify(f_value * 100.0, 1)
			i_value = int(f_value)
			
			percent = true
		
		# value should be a float, work with it in place
		else:
			if !vect:
				f_value     = global_vars.user_config[section][config_name][0]
			
			f_incr      = float(global_vars.user_config[section][config_name][2])
			f_precision = f_incr
			f_incr      = max(f_incr, min_increase_f)
			
			# getting min_value
			if !vect && typeof(global_vars.user_config[section][config_name][3]) == TYPE_STRING:
				min_key = global_vars.user_config[section][config_name][3]
				
				if min_key in global_vars.user_config[section]:
					f_min_value = global_vars.user_config[section][min_key][0]
				
				else:
					f_min_value = global_vars.user_config[section][config_name][0]
			
			else:
				f_min_value = global_vars.user_config[section][config_name][3]
			
			# getting max value
			if !vect && typeof(global_vars.user_config[section][config_name][4]) == TYPE_STRING:
				max_key = global_vars.user_config[section][config_name][4]
				
				if max_key in global_vars.user_config[section]:
					f_max_value = global_vars.user_config[section][max_key][0]
				
				else:
					f_max_value = global_vars.user_config[section][config_name][0]
			
			else:
				f_max_value = global_vars.user_config[section][config_name][4]
			
			
			# capping float value
			f_value     = cap_to_float_value(f_value, f_incr, f_min_value, f_max_value, section, min_key, max_key, positive, false)
			f_value     = stepify(f_value, f_precision)
			
			if !vect:
				global_vars.user_config[section][config_name][0] = f_value
				cont.get_node(input_name).text                   = String(f_value)
	
	
	# work with an int or a percentual
	if percent || typeof(global_vars.user_config[section][config_name][1]) == TYPE_INT:
		if !percent:
			i_value = global_vars.user_config[section][config_name][0]
		
		i_incr  = global_vars.user_config[section][config_name][2]
		
		if i_incr == 1:
			i_incr = max(i_incr, min_increase_i)
		
		# getting min_value
		if typeof(global_vars.user_config[section][config_name][3]) == TYPE_STRING:
			min_key = global_vars.user_config[section][config_name][3]
			
			if min_key in global_vars.user_config[section]:
				i_min_value = global_vars.user_config[section][min_key][0]
			
			else:
				i_min_value = global_vars.user_config[section][config_name][0]
		
		else:
			i_min_value = global_vars.user_config[section][config_name][3]
		
		# getting max value
		if typeof(global_vars.user_config[section][config_name][4]) == TYPE_STRING:
			max_key = global_vars.user_config[section][config_name][4]
			
			if max_key in global_vars.user_config[section]:
				i_max_value = global_vars.user_config[section][max_key][0]
			
			else:
				i_max_value = global_vars.user_config[section][config_name][0]
		
		else:
			i_max_value = global_vars.user_config[section][config_name][4]
		
		# capping int value
		i_value = cap_to_int_value(i_value, i_incr, i_min_value, i_max_value, section, min_key, max_key, positive, false)
		
		if !percent:
			global_vars.user_config[section][config_name][0] = i_value
			cont.get_node(input_name).text                   = String(i_value)
	
	# we got a percentual value processed, work with it
	if percent:
		cont.get_node(input_name).text                   = String(i_value) + "%"
		f_value = float(i_value) * 0.01
		global_vars.user_config[section][config_name][0] = f_value
	
	# keep working with vectors
	if vect:
		if idx == 0:
			global_vars.user_config[section][config_name][0].x = f_value
			cont.get_node(input_name).text                     = String(f_value)
		
		else:
			global_vars.user_config[section][config_name][0].y = f_value
			cont.get_node(input_name).text                     = String(f_value)


# cap an int value
func cap_to_int_value(value : int, incr : int, min_value : int, max_value : int, section : String, min_key : String, max_key : String, positive : bool, game_config : bool) -> int:
	if positive:
		if value >= max_value && len(max_key) > 0:
			change_config_value(section, max_key, 0, true, game_config)
			value = min(value + incr, global_vars.game_config[section][max_key][0])
		
		else:
			value = min(max_value, value + incr)
	
	elif value <= min_value && len(min_key) > 0:
		change_config_value(section, min_key, 0, false, game_config)
		value = max(value - incr, global_vars.game_config[section][min_key][0])
	
	else:
		value = max(min_value, value - incr)
	
	return value


# cap a float value
func cap_to_float_value(value : float, incr : float, min_value : float, max_value : float, section : String, min_key : String, max_key : String, positive : bool, game_config : bool) -> float:
	if positive:
		if value >= max_value && len(max_key) > 0:
			change_config_value(section, max_key, 0, true, game_config)
			value = min(value + incr, global_vars.game_config[section][max_key][0])
		
		else:
			value = min(max_value, value + incr)
	
	elif value <= min_value && len(min_key) > 0:
		change_config_value(section, min_key, 0, false, game_config)
		value = max(value - incr, global_vars.game_config[section][min_key][0])
	
	else:
		value = max(min_value, value - incr)
	
	return value


# toggle the value of a specific variable
func toggle_boolean(state : bool, section : String, config_name : String, game_config : bool) -> void:
	if game_config:
		global_vars.game_config[section][config_name][0] = state
	
	else:
		global_vars.user_config[section][config_name][0] = state


# shows all member of a group returning true if group exists
func show_group(section : String) -> bool:
	if get_tree().has_group(section):
		for node in get_tree().get_nodes_in_group(section):
			node.show()
		
		return true
	
	return false


# hides all member of a group returning true if group exists
func hide_group(section : String) -> bool:
	if get_tree().has_group(section):
		for node in get_tree().get_nodes_in_group(section):
			node.hide()
		
		return true
	
	return false


# show settings menu
func show_settings() -> void:
	var lang_list     : Array = []
	var idx           : int
	var n             : int
	
	var lang_selector : OptionButton = $Settings/SettingsScroll/SettingsMenu/LanguageCenter/LanguageSelector
	var fs_button     : CheckButton  = $Settings/SettingsScroll/SettingsMenu/FullscreenInput/Fullscreen
	
	var sound_slider  : HSlider      = $Settings/SettingsScroll/SettingsMenu/SoundsInput/VolumeSlider
	var music_slider  : HSlider      = $Settings/SettingsScroll/SettingsMenu/MusicInput/VolumeSlider
	var voice_slider  : HSlider      = $Settings/SettingsScroll/SettingsMenu/VoicesInput/VolumeSlider
	
	var sound_volume  : float        = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Sounds"))
	var music_volume  : float        = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
	var voice_volume  : float        = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Voices"))
	
	sound_volume                     = stepify(db2linear(sound_volume) * 100.0, 1.0)
	music_volume                     = stepify(db2linear(music_volume) * 100.0, 1.0)
	voice_volume                     = stepify(db2linear(voice_volume) * 100.0, 1.0)
	
	hide_submenus()
	
	fs_button.pressed                = OS.window_fullscreen
	
	# populate settings
	for key in global_vars.languages:
		if global_vars.languages[key] == TranslationServer.get_locale():
			idx = n
		
		if !settings_populated:
			lang_list.append(global_vars.languages[key])
			lang_selector.add_item(key)
		
		n += 1
	
	lang_selector.select(idx)
	
	settings_populated = true
	
	if !lang_selector.is_connected("item_selected", self, "_on_LangSelector_activated"):
		lang_selector.connect("item_selected", self, "_on_LangSelector_activated", [lang_list])
	
	if !sound_slider.is_connected("value_changed", self, "_on_VolumeSlider_value_changed"):
		sound_slider.connect("value_changed", self, "_on_VolumeSlider_value_changed", [sound_slider.get_parent(), SOUND])
		music_slider.connect("value_changed", self, "_on_VolumeSlider_value_changed", [music_slider.get_parent(), MUSIC])
		voice_slider.connect("value_changed", self, "_on_VolumeSlider_value_changed", [voice_slider.get_parent(), VOICE])
	
	sound_slider.value  = sound_volume
	music_slider.value  = music_volume
	voice_slider.value  = voice_volume
	
	
	$Settings.show()
	$Settings/SettingsScroll/SettingsMenu/ControlCenter/Controls.grab_focus()


# show user configs
func show_user_configs():
	hide_submenus()
	
	custom_base   = $UserConfigs/UserScroll/UserOptions
	custom_scroll = $UserConfigs/UserScroll
	
	if custom_base.get_child_count() == 0:
		populate_custom_section(custom_base, false)
	
	$UserConfigs/Center/UserCancel.grab_focus()
	$UserConfigs.show()


# show difficulty editor
func show_difficulty():
	hide_submenus()
	
	custom_base    = $Difficulty/DifficultyScroll/DifficultyOptions
	custom_scroll  = $Difficulty/DifficultyScroll
	
	current_levels = global_vars.levels
	
	if custom_base.get_child_count() == 0:
		populate_custom_section(custom_base, false, true)
	
	$Difficulty/Center/DifficultyCancel.grab_focus()
	$Difficulty.show()


# show controls menu
func show_controls() -> void:
	#var main_cont : VBoxContainer = $Controls/ScrollControls/ControlsTarget
	var grid_cont : GridContainer
	var action    : String
	
	custom_base       = $Controls/ScrollControls/ControlsTarget
	custom_scroll     = $Controls/ScrollControls
	current_controls  = {}
	
	for player in global_vars.controls:
		grid_cont = new_controls_grid()
		grid_cont.set_columns(3)
		
		if player == global_vars.P1:
			grid_cont.name = P1_GRID
		
		else:
			grid_cont.name = P2_GRID
		
		add_custom_section(custom_base, global_vars.player_names[player])
		
		for control in global_vars.controls[player]:
			add_controls_buttons(grid_cont, global_vars.controls[player][control], control)
			
			action = global_vars.controls[player][control]
			
			current_controls[action] = InputMap.get_action_list(action)
		
		custom_base.add_child(grid_cont)
	
	hide_submenus()
	$Controls.show()
	$Controls/Center/ControlsBack.grab_focus()


# clear a custom section
func clear_custom_section(base : Node):
	for c in base.get_children():
		c.queue_free()


# clear all custom selectors
func clear_custom_selectors():
	$SingleCustom/CenterTop/GameModes.clear()
	$MultiCustom/CenterTop/GameModes.clear()


# add automatic game or user configuration
func populate_custom_section(base : Node, game_config : bool, alternative : bool = false) -> void:
	var t        : Timer = Timer.new()
	var i        : int
	var x        : int
	var section  : String
	var grid     : GridContainer
	
	t.set_wait_time(0.01)
	t.set_one_shot(true)
	
	$Timers.add_child(t)
	
	while base.get_child_count() > 0 && i < 500:
		t.start()
		yield(t, "timeout")
		i += 1
	
	if i >= 500:
		clear_custom_section(base)
		
		i = 0
		
		while base.get_child_count() > 0 && i < 500:
			t.start()
			yield(t, "timeout")
			i += 1
		
		if i >= 500:
			print("ERROR: unable to clean up the menu before before spawning new objects. Giving up.")
			return
	
	t.stop()
	t.queue_free()
	
	# game configuration
	if game_config:
		for key in global_vars.game_config:
			if alternative || key != "MULTIPLAYER":
				add_custom_section(custom_base, key)
				
				for config_name in global_vars.game_config[key]:
					add_custom_input(custom_base, key, config_name, global_vars.game_config[key][config_name], true)
			
		
		while len(groups_to_hide) > 0:
			section = groups_to_hide.pop_front()
			hide_group(section)
	
	# difficulty editor 
	elif alternative:
		diff_keys = current_levels.keys()
		i         = 0
		
		diff_keys.sort()
		
		for key in diff_keys:
			
			grid = add_difficulty_section(custom_base, key)
			
			
			x  = 0
			
			for config in current_levels[key]:
				add_difficulty_input(grid, x, [config])
				x += 1
			
			add_difficulty_add_remove(grid, i)
			
			i += 1
	
	# user configuration
	else:
		for key in global_vars.user_config:
			add_custom_section(custom_base, key)
			
			for config in global_vars.user_config[key]:
				add_custom_input(custom_base, key, config, global_vars.user_config[key][config], false)


# populate custom game mode selector
func populate_custom_selector(selector : OptionButton, multiplayer : bool) -> void:
	game_modes = []
	
	selector.clear()
	
	selector.add_item(SELECT_TEXT)
	
	# insert custom game modes and sort them
	for mode in global_vars.custom_modes:
		game_modes.append(mode)
	
	game_modes.sort()
	
	# insert standard game modes at the beginning
	# don't sort them since they're already hardcoded the right way
	for mode in global_vars.standard_modes:
		game_modes.insert(0, mode)
	
	# add them to the selector
	for mode in game_modes:
		selector.add_item(mode)
	
	if !selector.is_connected("item_selected", self, "_on_GameModes_item_selected"):
		selector.connect("item_selected", self, "_on_GameModes_item_selected", [selector, multiplayer])


# clear user configuration
func clear_user_configs():
	for c in $UserConfigs/UserScroll/UserOptions.get_children():
		c.queue_free()


# store user configurations in the configuration file
func store_user_configs():
	for section in global_vars.user_config:
		for config in global_vars.user_config[section]:
			global_vars.config.set_value("user", config, global_vars.user_config[section][config][0])
			global_vars.user_config[section][config][1] = global_vars.user_config[section][config][0]
	
	global_vars.config.save(global_vars.CONFIG_FILE)


# reset user configs, possibly with an hard reset
func reset_user_configs(hard_reset : bool) -> void:
	var default_idx : int = 1
	
	for section in global_vars.user_config:
		for config in global_vars.user_config[section]:
			if hard_reset:
				default_idx = len(global_vars.user_config[section][config]) - 1
			
			global_vars.user_config[section][config][0] = global_vars.user_config[section][config][default_idx]


# returns a new control grid with proper labels inside
func new_controls_grid() -> GridContainer:
	var new_grid : GridContainer = GridContainer.new()
	var label    : Label
	
	new_grid.set_columns(3)
	
	for title in controls_titles:
		label = Label.new()
		
		label.add_color_override("font_color", section_color)
		
		label.text                  = title
		label.size_flags_horizontal = SIZE_EXPAND_FILL
		label.align                 = HALIGN_CENTER
		
		new_grid.add_child(label)
	
	return new_grid


# add a controls button and bind it to the proper event
func add_controls_buttons(cont : GridContainer, action : String, control : int) -> void:
	var c_title   : Label  = Label.new()
	var a_list    : Array  = InputMap.get_action_list(action)
	var c_button  : Button
	var joypad    : bool
	
	c_title.text = global_vars.human_dirs[control]
	
	cont.add_child(c_title)
	
	if len(a_list) == 1:
		if a_list[0] is InputEventKey:
			a_list.append("J")
		
		else:
			a_list.insert(0, "K")
	
	for act in a_list:
		c_button = Button.new()
		
		c_button.toggle_mode = true
		
		if typeof(act) == TYPE_STRING:
			if act == "K":
				joypad = false
				c_button .name = action + KEYBOARD
			
			else:
				joypad = true
				c_button.name = action + JOYPAD
			
			c_button.text = "NONE"
		
		elif act is InputEventJoypadButton:
			joypad = true
			c_button.text = "Joypad: " +String(act.button_index)
			c_button.name = action + JOYPAD
		
		else:
			joypad = false
			c_button.text = OS.get_scancode_string(act.scancode)
			c_button.name = action + KEYBOARD
		
		c_button.connect("toggled", self, "_on_Control_button_toggled", [joypad, action, c_button])
		c_button.connect("focus_entered", self, "_on_Input_focus_entered", [c_button, false])
		cont.add_child(c_button)


# remap an action to a specific key
func remap_action_to(event : InputEvent) -> void:
	var old_action   : String
	var old_list     : Array
	var old_button   : Button
	
	for player in global_vars.controls:
		if len(old_action) > 0:
			break
		
		for control in global_vars.controls[player]:
			if global_vars.controls[player][control] != edit_action && InputMap.event_is_action(event, global_vars.controls[player][control]):
				old_action = global_vars.controls[player][control]
				break
	
	# event was already used, switch them
	if len(old_action) > 0:
		old_list = InputMap.get_action_list(old_action)
		old_list[control_index] = control_list[control_index]
		
		InputMap.action_erase_events(old_action)
		
		if old_action == global_vars.SELECT_ACTION:
			InputMap.action_erase_events(global_vars.ACCEPT_ACTION)
		
		for ev in old_list:
			if ev != null:
				InputMap.action_add_event(old_action, ev)
				
				if old_action == global_vars.SELECT_ACTION:
					InputMap.action_add_event(global_vars.ACCEPT_ACTION, ev)
		
		# release the action so it's not pressed on switch
		Input.action_release(old_action)
		
		if old_action == global_vars.SELECT_ACTION:
			Input.action_release(global_vars.SELECT_ACTION)
		
		# edit old button text
		if control_index == 0:
			old_button      = control_button.get_parent().get_node(old_action + KEYBOARD)
			
			# the button wasn't found, find it in the other player grid
			if !is_instance_valid(old_button):
				if control_button.get_parent().name == P1_GRID:
					old_button = custom_base.get_node(P2_GRID).get_node(old_action + KEYBOARD)
				
				else:
					old_button = custom_base.get_node(P1_GRID).get_node(old_action + KEYBOARD)
				
				if !is_instance_valid(old_button):
					print("ERROR: unable to find previous action button.")
					return
			
			if control_list[control_index] != null:
				old_button.text = OS.get_scancode_string(control_list[control_index].scancode)
			
			else:
				old_button.text = "NONE"
			
		else:
			old_button = control_button.get_parent().get_node(old_action + JOYPAD)
			
			# the button wasn't found, find it in the other player grid
			if !is_instance_valid(old_button):
				if control_button.get_parent().name == P1_GRID:
					old_button = custom_base.get_node(P2_GRID).get_node(old_action + JOYPAD)
				
				else:
					old_button = custom_base.get_node(P1_GRID).get_node(old_action + JOYPAD)
				
				if !is_instance_valid(old_button):
					print("ERROR: unable to find previous action button.")
					return
			
			if control_list[control_index] != null:
				old_button.text = "Joypad: " + String(control_list[control_index].button_index)
			
			else:
				old_button.text = "NONE"
	
	control_list[control_index] = event
	
	InputMap.action_erase_events(edit_action)
	
	if edit_action == global_vars.SELECT_ACTION:
		InputMap.action_erase_events(global_vars.ACCEPT_ACTION)
	
	for ev in control_list:
		if ev != null:
			InputMap.action_add_event(edit_action, ev)
		
			if edit_action == global_vars.SELECT_ACTION:
				InputMap.action_add_event(global_vars.ACCEPT_ACTION, ev)
	
	control_button.pressed = false
	
	block_scroll = true
	
	control_button.grab_focus()


# clear controls menu to show them properly next time
func clear_controls() -> void:
	var cont : VBoxContainer = $Controls/ScrollControls/ControlsTarget
	
	for c in cont.get_children():
		c.queue_free()


# undo controls edit
func undo_controls() -> void:
	for action in current_controls:
		InputMap.action_erase_events(action)
		
		if action == global_vars.SELECT_ACTION:
			InputMap.action_erase_events(global_vars.ACCEPT_ACTION)
		
		for ev in current_controls[action]:
			InputMap.action_add_event(action, ev)
			
			if action == global_vars.SELECT_ACTION:
				InputMap.action_add_event(global_vars.ACCEPT_ACTION, ev)


# reload controls from the configuration file
func reload_controls() -> void:
	var c_key   : String
	var action  : String
	var c_list  : Array
	
	if global_vars.config.has_section("controls"):
		for player in global_vars.controls:
			for control in global_vars.controls[player]:
				c_key = get_control_config_key(player, control)
				
				if global_vars.config.has_section_key("controls", c_key):
					c_list = global_vars.config.get_value("controls", c_key)
					
					action = get_action_from_key(c_key)
					
					InputMap.action_erase_events(action)
					
					if action == global_vars.SELECT_ACTION:
						InputMap.action_erase_events(global_vars.ACCEPT_ACTION)
					
					for event in c_list:
						InputMap.action_add_event(action, event)
						
						if action == global_vars.SELECT_ACTION:
							InputMap.action_add_event(global_vars.ACCEPT_ACTION, event)


# store controls in the configuration file
func confirm_controls() -> void:
	var c_list : Array
	
	global_vars.config.set_value("input", "vibration", set_vibration)
	global_vars.vibration = set_vibration
	
	for player in global_vars.controls:
		for control in global_vars.controls[player]:
			c_list = InputMap.get_action_list(global_vars.controls[player][control])
			global_vars.config.set_value("controls", get_control_config_key(player, control), c_list)
	
	global_vars.config.save(global_vars.CONFIG_FILE)


# return a proper control config key
func get_control_config_key(player : int, control : int) -> String:
	var key : String = global_vars.player_names[player].replace(" ",  "_")
	
	key += "_" + global_vars.human_dirs[control]
	
	return key


# return an action name from a key
func get_action_from_key(key : String) -> String:
	var action : String
	var k_list : Array
	var p_name : String
	var p_id   : int
	var d_id   : int
	
	k_list = key.split("_", false)
	
	p_name = k_list[0] + " " + k_list[1]
	
	for key in global_vars.player_names:
		if p_name == global_vars.player_names[key]:
			p_id = key
			break
	
	for dir in global_vars.human_dirs:
		if global_vars.human_dirs[dir] == k_list[2]:
			d_id = dir
			break
	
	action = global_vars.controls[p_id][d_id]
	
	return action


# confirm settings edit
func confirm_settings() -> void:
	# clean custom game mode menus only if needed
	if language_changed:
		language_changed = false
		
		for c in $SingleCustom/SCScroll/SCOptions.get_children():
			c.queue_free()
		
		for c in $MultiCustom/MCScroll/MCOptions.get_children():
			c.queue_free()
		
		clear_user_configs()
	
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear2db(set_volume))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(set_music))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Voices"), linear2db(set_voice))
	
	TranslationServer.set_locale(set_locale)
	
	OS.window_fullscreen = set_fullscreen
	
	$ToggleMusic/MusicCheckbox.pressed = set_music > 0
	
	global_vars.config.set_value("audio", "volume", set_volume)
	global_vars.config.set_value("audio", "music", set_music)
	global_vars.config.set_value("audio", "voices", set_voice)
	global_vars.config.set_value("interface", "language", set_locale)
	global_vars.config.set_value("interface", "fullscreen", set_fullscreen)
	global_vars.config.save(global_vars.CONFIG_FILE)


# load configurations from the files
func load_configs() -> bool:
	var err     : int
	
	global_vars.config = ConfigFile.new()
	
	err = global_vars.config.load(global_vars.CONFIG_FILE)
	
	if err == OK:
		if global_vars.config.has_section_key("audio", "volume"):
			set_volume = global_vars.config.get_value("audio", "volume")
			
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear2db(set_volume))
		
		else:
			set_volume = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Sounds"))
			set_volume = stepify(db2linear(set_volume) * 100.0, 1.0)
		
		if global_vars.config.has_section_key("audio", "music"):
			set_music = global_vars.config.get_value("audio", "music")
			
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(set_music))
		
		else:
			set_music = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
			set_music = stepify(db2linear(set_music) * 100.0, 1.0)
		
		if global_vars.config.has_section_key("audio", "voices"):
			set_voice = global_vars.config.get_value("audio", "voices")
			
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Voices"), linear2db(set_voice))
		
		else:
			set_voice = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Voices"))
			set_voice = stepify(db2linear(set_voice) * 100.0, 1.0)
		
		if global_vars.config.has_section_key("interface", "language"):
			set_locale = global_vars.config.get_value("interface", "language")
			
			TranslationServer.set_locale(set_locale)
		
		if global_vars.config.has_section_key("interface", "fullscreen"):
			OS.window_fullscreen = global_vars.config.get_value("interface", "fullscreen")
		
		for section in global_vars.user_config:
			for config in global_vars.user_config[section]:
				if global_vars.config.has_section_key("user", config):
					global_vars.user_config[section][config][0] = global_vars.config.get_value("user", config)
					global_vars.user_config[section][config][1] = global_vars.user_config[section][config][0]
		
		reload_controls()
		
		return true
	
	return false


# select a custom game mode and update the interface
func select_custom_game_mode(mode_key : String, multiplayer : bool, main : bool = true) -> void:
	var index      : int = multiplayer
	var s_mode     : int = global_vars.game_mode
	var levels_set : bool
	
	global_vars.reset_default_game_config()
	
	temp_levels = {}
	
	if mode_key in global_vars.standard_modes:
		global_vars.select_game_mode(global_vars.standard_modes[mode_key][index])
		global_vars.game_mode = s_mode
	
	elif mode_key in global_vars.custom_modes:
		
		# link to other game modes, so we don't have to hardcode every option every time
		if global_vars.OTHER_GAME in global_vars.custom_modes[mode_key]:
			select_custom_game_mode(global_vars.custom_modes[mode_key][global_vars.OTHER_GAME], multiplayer, false)
		
		for section in global_vars.custom_modes[mode_key]:
			# difficulty isn't additive but shouldn't be a problem with the way we'll store it
			if section == global_vars.LEVELS_KEY:
				if main:
					global_vars.select_new_difficulty(global_vars.custom_modes[mode_key][section])
					levels_set = true
				
				else:
					temp_levels = global_vars.custom_modes[mode_key][section]
			
			elif section != global_vars.OTHER_GAME:
				for config_name in global_vars.custom_modes[mode_key][section]:
					global_vars.game_config[section][config_name][0] = global_vars.custom_modes[mode_key][section][config_name]
	
	if main:
		# our class haven't edited difficulty, set it now
		if !levels_set:
			if len(temp_levels) > 0:
				global_vars.select_new_difficulty(temp_levels)
			
			else:
				global_vars.select_new_difficulty()
		
		clear_custom_section(custom_base)
		populate_custom_section(custom_base, true, multiplayer)


# return key for this difficulty edit line
func get_diff_key(input : Node) -> int:
	var key       : int      = -1
	var key_input : LineEdit = input.get_parent().get_node(VALUE_X)
	
	if is_instance_valid(key_input):
		key = int(key_input.text)
	
	return key


# display an error message to the user
func handle_custom_save(success : bool, multiplayer : bool, remove : bool = false) -> void:
	if multiplayer:
		save_line  = $MultiCustom/CenterTop/SaveName
	
	else:
		save_line  = $SingleCustom/CenterTop/SaveName
	
	save_line.editable = false
	
	if success:
		if remove:
			save_line.text = tr(REMOVED_TXT)
		
		else:
			save_line.text = tr(SAVED_TXT)
		
		clear_custom_selectors()
		populate_custom_selector(custom_selector, multiplayer)
		
		custom_selector = null
	
	else:
		save_line.text = tr(ERROR_TXT)
	
	$Timers/SaveTimer.start()


### signals ###


# user inputs, show mouse only when needed
func _input(event : InputEvent) -> void:
	var e_class : String = event.get_class()
	
	if mouse_hidden && e_class == "InputEventMouseMotion":
		mouse_hidden = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	elif !mouse_hidden && e_class != "InputEventMouseMotion" && e_class != "InputEventMouseButton":
		mouse_hidden = true
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

### buttons ###

func _on_SinglePlayer_pressed() -> void:
	show_single_player()

func _on_MultiPlayer_pressed() -> void:
	show_multiplayer()

func _on_Settings_pressed() -> void:
	show_settings()

func _on_Exit_pressed() -> void:
	get_tree().quit()

func _on_SingleClassic_pressed() -> void:
	global_vars.reset_default_game_config()
	global_vars.select_new_difficulty()
	start_game(global_vars.CLASSIC)

func _on_SingleHardcore_pressed() -> void:
	global_vars.reset_default_game_config()
	global_vars.select_new_difficulty()
	start_game(global_vars.HARDCORE)

func _on_MultiClassic_pressed():
	global_vars.reset_default_game_config()
	global_vars.select_new_difficulty()
	start_game(global_vars.M_CLASSIC)

func _on_MultiHardcore_pressed():
	global_vars.reset_default_game_config()
	global_vars.select_new_difficulty()
	start_game(global_vars.M_HARDCORE)

func _on_SingleCustom_pressed() -> void:
	show_new_custom()

func _on_MultiCustom_pressed() -> void:
	show_new_custom(true)

func _on_FirstLevelBack_pressed() -> void:
	show_main_menu()

func _on_SettingsUndo_pressed():
	language_changed = false
	show_main_menu()

func _on_SCBack_pressed() -> void:
	show_single_player()

func _on_MCBack_pressed() -> void:
	show_multiplayer()

func _on_HiScores_pressed() -> void:
	view_hiscores()

func _on_GameOverBack_pressed() -> void:
	show_main_menu()

func _on_GameOverRetry_pressed() -> void:
	global_vars.reset_vars(true)
	start_game(global_vars.game_mode)

func _on_SCStart_pressed() -> void:
	start_new_custom(global_vars.CUSTOM)

func _on_MCStart_pressed() -> void:
	start_new_custom(global_vars.M_CUSTOM)

func _on_Controls_pressed() -> void:
	show_controls()

func _on_ControlsConfirm_pressed() -> void:
	clear_controls()
	confirm_controls()
	show_settings()

func _on_ControlsBack_pressed() -> void:
	clear_controls()
	undo_controls()
	show_settings()

func _on_SettingsConfirm_pressed() -> void:
	confirm_settings()
	show_main_menu()


func _on_UserConfirm_pressed() -> void:
	show_settings()
	store_user_configs()


func _on_UserCancel_pressed() -> void:
	reset_user_configs(false)
	show_settings()
	clear_user_configs()
	populate_custom_section(custom_base, false)


func _on_UserConfig_pressed() -> void:
	show_user_configs()


func _on_UserReset_pressed() -> void:
	reset_user_configs(true)
	clear_user_configs()
	populate_custom_section(custom_base, false)


func _on_Difficulty_pressed():
	show_difficulty()


func _on_DifficultyCancel_pressed():
	clear_custom_section(custom_base)
	
	if multi_custom:
		show_new_custom(true)
	
	else:
		show_new_custom()


func _on_DifficultyReset_pressed():
	global_vars.select_new_difficulty()
	
	current_levels = global_vars.levels
	
	clear_custom_section(custom_base)
	populate_custom_section(custom_base, false, true)


func _on_DifficultyConfirm_pressed():
	global_vars.select_new_difficulty(current_levels)
	
	_on_DifficultyCancel_pressed()


func _on_GameModes_item_selected(index : int, selector : OptionButton, multiplayer : bool) -> void:
	if index > 0:
		index -= 1
		
		select_custom_game_mode(game_modes[index], multiplayer)
		
		if game_modes[index] in global_vars.standard_modes || game_modes[index] in global_vars.hardcoded_modes:
			stored_b_state = true
		
		else:
			stored_b_state = false
		
		custom_selector = selector
	
	else:
		stored_b_state = true
	
	save_button.disabled = stored_b_state


# a plus button was pressed
func _on_Plus_pressed(section : String, config_name : String, idx : int, game_config : bool) -> void:
	change_config_value(section, config_name, idx, true, game_config)
	last_positive    = true
	last_game_config = game_config
	last_section     = section
	last_config_name = config_name
	last_idx         = idx
	$Timers/ButtonTimer.stop()
	$Timers/ButtonTimer.start()
	
	if is_instance_valid(custom_selector):
		custom_selector.select(0)
		custom_selector = null

# a minus button was pressed
func _on_Minus_pressed(section : String, config_name : String, idx : int, game_config : bool) -> void:
	change_config_value(section, config_name, idx, false, game_config)
	last_positive    = false
	last_game_config = game_config
	last_section     = section
	last_config_name = config_name
	last_idx         = idx
	$Timers/ButtonTimer.stop()
	$Timers/ButtonTimer.start()
	
	if is_instance_valid(custom_selector):
		custom_selector.select(0)
		custom_selector = null

# a plus or minus button was released
func _on_PlusMinus_released() -> void:
	$Timers/ButtonTimer.stop()
	$Timers/RepeatTimer.stop()
	increase_ticks = 0
	min_increase_f = 0
	min_increase_i = 0
	#repeat = false

# a checkbox was toggled
func _on_Checkbox_toggled(state: bool, section : String, config_name : String, game_config : bool) -> void:
	toggle_boolean(state, section, config_name, game_config)
	
	if is_instance_valid(custom_selector):
		custom_selector.select(0)
		custom_selector = null

# a control settings input was toggled
func _on_Control_button_toggled(pressed: bool, joypad : bool , action : String, button : Button):
	var act    : InputEvent
	var a_list : Array
	
	if joypad:
		set_process_unhandled_input(pressed)
		set_process_unhandled_key_input(false)
		
	else:
		set_process_unhandled_key_input(pressed)
		set_process_unhandled_input(false)
	
	if pressed:
		edit_action    = action
		control_button = button
		control_list   = InputMap.get_action_list(action)
		control_index  = joypad
		button.text    = tr("NEW") + "..."
		button.release_focus()
		
		if len(control_list) == 1:
			if (control_list[0] is InputEventKey && joypad) || !joypad:
				control_list.append(null)
				
			else:
				control_list.insert(0, null)
	
	else:
		if joypad:
			a_list = InputMap.get_action_list(action)
			
			if len(a_list) > 1:
				act = a_list[1]
				button.text = "Joypad: " + String(act.button_index)
			
			else:
				button.text = "NONE"
		
		else:
			act = InputMap.get_action_list(action)[0]
			button.text = OS.get_scancode_string(act.scancode)


# used to limit score lines to integrals
func _on_Score_Line_text_changed(new_text : String, input : LineEdit) -> void:
	var value : int = int(new_text)
	var index : int = diff_keys.find(stored_diff_key)
	
	if new_text != "":
		if value < 0:
			value = -value
		
		if value == 0:
			
			value           = diff_keys[index]
			input.text      = String(diff_keys[index])
		
		elif new_text != String(value):
			input.text = String(value)

# used to limit other lines at numbers
func _on_Diff_Line_text_changed(new_text : String, input : LineEdit, is_float : bool) -> void:
	var value : float
	var prec  : float
	
	if is_float:
		prec = 0.01
	
	else:
		prec = 1.0
	
	value = stepify(float(new_text), prec)
	
	if new_text != "" && (!is_float || !new_text.ends_with(".")):
		if value < 0:
			value = -value
		
		if value == 0:
			value      = 0
			input.text = "0"
		
		elif new_text != String(value):
			input.text = String(value)

# a difficulty level remove button was pressed
func _on_Diff_Remove_pressed(line : LineEdit) -> void:
	var key : int = int(line.text)
	
	diff_keys.erase(key)
	current_levels.erase(key)
	
	line.get_parent().queue_free()

# a difficulty level add button was pressed
func _on_Diff_Add_pressed(line : LineEdit) -> void:
	var key   : int = int(line.text)
	var index : int = diff_keys.find(key) + 1
	
	var grid  : GridContainer
	var x     : int
	
	key += 1
	
	if index >= len(diff_keys) || diff_keys[index] > key:
		diff_keys.insert(index, key)
		
		current_levels[key] = []
		
		for d in current_levels[key - 1]:
			current_levels[key].append(d)
		
		grid = add_difficulty_section(custom_base, key, line.get_parent())
		
		
		for config in current_levels[key]:
			add_difficulty_input(grid, x, [config])
			x += 1
		
		add_difficulty_add_remove(grid, index)


# update the label on enter
func _on_Score_Line_text_entered(_text : String, input : LineEdit) -> void:
	_on_Score_Line_focus_exited(input)

# store diff key on focus
func _on_Score_Line_focus_entered(input : LineEdit) -> void:
	stored_diff_key = int(input.text)

# set value on unfocus with value cap to nearby ones
func _on_Score_Line_focus_exited(input : LineEdit) -> void:
	var value   : int = int(input.text)
	var index   : int = diff_keys.find(stored_diff_key)
	var old_key : int
	
	# cap value to nearby ones
	if value <= diff_keys[index - 1]:
		value = diff_keys[index - 1] + 1
		input.text = String(value)
	
	elif index < len(diff_keys) - 1 && value >= diff_keys[index + 1]:
		value = diff_keys[index + 1] - 1
		input.text = String(value)
	
	old_key = diff_keys[index]
	
	if value != diff_keys[index]:
		stored_diff_key       = value
		diff_keys[index]      = value
		
		current_levels[value] = current_levels[old_key]
		
		current_levels.erase(old_key)
		
		#for key in current_levels:
		#	print(key, " -> ", current_levels[key])

# update the label on enter
func _on_Diff_Line_text_entered(_text : String, input : LineEdit, index : int, is_float : bool) -> void:
	_on_Diff_Line_focus_exited(input, index, is_float)

# set value on unfocus with value cap to nearby ones
func _on_Diff_Line_focus_exited(input : LineEdit, index : int, is_float : bool) -> void:
	var f_value   : float
	var i_value   : int
	var key       : int   = get_diff_key(input)
	var conn_idx  : int
	
	if key > -1:
		conn_idx = global_vars.level_values[index][2]
		
		f_value = float(input.text)
		
		f_value = max(f_value, global_vars.level_values[index][0])
		f_value = min(f_value, global_vars.level_values[index][1])
		
		if conn_idx > 0:
			if conn_idx > index:
				f_value = min(f_value, current_levels[key][conn_idx])
			
			else:
				f_value = max(f_value, current_levels[key][conn_idx])
		
		if is_float:
			current_levels[key][index] = f_value
			
			input.text = String(f_value)
		
		else:
			i_value = int(f_value)
			current_levels[key][index] = i_value
			
			input.text = String(i_value)
		


func _on_Diff_Checkbox_toggled(state : bool, input : CheckButton, index : int) -> void:
	var key       : int = get_diff_key(input)
	
	if key > -1:	
		current_levels[key][index] = state

func _on_Diff_ColorPicker_Changed(color : Color, input : ColorPickerButton, index : int) -> void:
	var key       : int = get_diff_key(input)
	
	if key > -1:
		current_levels[key][index] = color

# an game config input was focused
func _on_Input_focus_entered(input : Node, use_parent : bool = true) -> void:
	var cont : Node = input.get_parent()
	var pos  : float
	
	if block_scroll:
		block_scroll = false
		return
	
	if use_parent:
		_on_PlusMinus_released()
	
	if is_instance_valid(cont) && mouse_hidden:
		pos = cont.get_rect().position.y
		
		if use_parent:
			pos -= cont.get_rect().size.y
		
		else:
			pos += input.get_rect().position.y
			pos -= input.get_rect().size.y
		
		custom_scroll.set_v_scroll(pos)
		custom_scroll.update()

# delay between the click and the start of RepeatTimer
func _on_ButtonTimer_timeout() -> void:
	$Timers/RepeatTimer.start()

# timer used to rapidly increase or decrease a numeral value
func _on_RepeatTimer_timeout() -> void:
	var t_time : float = slow_increase
	
	change_config_value(last_section, last_config_name, last_idx, last_positive, last_game_config)
	
	if increase_ticks > s_increase_limit:
		t_time         = slow_increase
		min_increase_f = fast_increase_f
		min_increase_i = fast_increase_i
	
	else:
		increase_ticks += 1
	
	$Timers/RepeatTimer.wait_time = t_time
	$Timers/RepeatTimer.start()

# an item in the language selector was activated
func _on_LangSelector_activated(index : int, options : Array) -> void:
	set_locale       = options[index]
	language_changed = true

# an item in an itemlist was selected, we need to update its corresponding option
func _on_ItemList_item_activated(index : int, section : String, config_name : String, options : Array, game_config : bool) -> void:
	var cont : Node  = custom_base.get_node(section + config_name + TIP_NAME)
	var n    : int   = 0
	var desc : Label = cont.get_node(DESC_TIP)
	
	if is_instance_valid(desc):
		desc.text = options[index][3]
	
	if game_config:
		global_vars.game_config[section][config_name][0] = options[index][1]
	
	else:
		global_vars.user_config[section][config_name][0] = options[index][1]
	
	if options[n][2] != null:
		while n < len(options):
			if n == index:
				show_group(options[n][2])
			
			else:
				hide_group(options[n][2])
			
			n += 1
	
	if is_instance_valid(custom_selector):
		custom_selector.select(0)
		custom_selector = null

# used to set colors
func _on_ColorPicker_Changed(color : Color, section : String, config_name : String, game_config : bool) -> void:
	if game_config:
		global_vars.game_config[section][config_name][0] = color
	
	else:
		global_vars.user_config[section][config_name][0] = color

# set sound volume
func _on_VolumeSlider_value_changed(value : float, input : HBoxContainer, channel : int) -> void:
	input.get_node("VolumeLabel").text = String(value) + "%"
	
	if channel == SOUND:
		set_volume = value / 100.0
	
	elif channel == MUSIC:
		set_music = value / 100.0
	
	elif channel == VOICE:
		set_voice = value / 100.0

func _on_Song_finished() -> void:
	if global_vars.play_music:
		$Timers/NextSongTimer.start()

func _on_NextSongTimer_timeout() -> void:
	play_next_song()

func _on_MusicCheckbox_toggled(pressed : bool) -> void:
	var music_volume  : float        = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
	
	music_volume                     = stepify(db2linear(music_volume) * 100.0, 1.0)
	
	if music_volume <= 0:
		$ToggleMusic/MusicCheckbox.pressed = false
		return
	
	global_vars.play_music = pressed
	
	if !pressed:
		$Timers/NextSongTimer.stop()
		
		if len(songs) > 0:
			songs[global_vars.song_index].stream_paused = true
			
	
	else:
		if len(songs) > 0:
			songs[global_vars.song_index].stream_paused = false
		
		elif $Timers/NextSongTimer.is_stopped():
			$Timers/NextSongTimer.start()

func _on_Single_Custom_Game_Save_pressed(mode_name : String = "") -> void:
	save_button.disabled = true
	custom_selector      = $SingleCustom/CenterTop/GameModes
	
	if mode_name == "":
		mode_name = $SingleCustom/CenterTop/SaveName.text
	
	if mode_name != "":
		new_mode_name        = mode_name
		handle_custom_save(global_vars.add_game_mode(mode_name), false)
	
	elif custom_selector.selected > 0:
		mode_name = game_modes[custom_selector.selected - 1]
		
		handle_custom_save(global_vars.remove_game_mode(mode_name), false, true)


func _on_Multi_Custom_Game_Save_pressed(mode_name : String = "") -> void:
	save_button.disabled = true
	custom_selector      = $MultiCustom/CenterTop/GameModes
	
	if mode_name == "":
		mode_name = $MultiCustom/CenterTop/SaveName.text
	
	if mode_name != "":
		new_mode_name        = mode_name
		handle_custom_save(global_vars.add_game_mode(mode_name), true)
	
	elif custom_selector.selected > 0:
		mode_name = game_modes[custom_selector.selected - 1]
		
		handle_custom_save(global_vars.remove_game_mode(mode_name), true, true)

func _on_SaveTimer_timeout() -> void:
	stored_b_state       = true
	save_button.disabled = stored_b_state
	save_button.text     = REMOVE_TXT
	save_line.text       = ""
	save_line.editable   = true

func _on_SaveName_text_changed(new_text : String, multiplayer : bool) -> void:
	
	if len(new_text) == 0:
		save_button.disabled = stored_b_state
		
		if multiplayer:
			$MultiCustom/CenterTop/Save.text = REMOVE_TXT
		
		else:
			$SingleCustom/CenterTop/Save.text = REMOVE_TXT
	
	else:
		save_button.disabled = false
		
		if multiplayer:
			$MultiCustom/CenterTop/Save.text = SAVE_TXT
		
		else:
			$SingleCustom/CenterTop/Save.text = SAVE_TXT

func _on_Fullscreen_toggled(button_pressed) -> void:
	set_fullscreen = button_pressed

func _on_Vibration_toggled(button_pressed) -> void:
	set_vibration = button_pressed
	
	if button_pressed:
		Input.start_joy_vibration(0, 0, 1.0, 0.5)
