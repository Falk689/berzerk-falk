extends VBoxContainer

### global vars ###
onready var global_vars               : Node   = get_node("/root/Global")

### exported vars not edited by the code ###

export (String)        var next_scene : String = "res://src/HUD/ViewHighScores.tscn"                  # next scene to show after we inputed the initials


export (Array, String) var letters    : Array  = ["A", "B", "C", "D", "E", "F", "G",                  # possible initials
												  "H", "I", "J", "K", "L", "M", "N",
												  "O", "P", "Q", "R", "S", "T", "U",
												  "V", "W", "X", "Y", "Z"]

### vars used by the code ###

var letter_index                      : int    = 0                                                    # index of currently selected initial
var init_index                        : int    = 0                                                    # index of current initial
var score                             : int    = 0                                                    # score we're working with
var score_id                          : int    = -1                                                   # player_id we're working with
var scores_checked                    : int    = 0                                                    # how many scores we've checked
var confirm_tip                       : String = "HIGH_SCORE_TIP_TWO"              # tip to show before confirming initials
onready var init_array                : Array  = [$Initials/V1/L1, $Initials/V2/L2, $Initials/V3/L3]  # array of initials labels
onready var default_tip               : String = $Tip.text                                            # default tip message
onready var congrat_text              : String = $Congratulations.text

### standard methods ###

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	VisualServer.set_default_clear_color(global_vars.BG_COLOR)
	
	select_next_score()

### custom methods ###

# select next score and if it's an hi score let the player insert their initials
func select_next_score():
	var err : int
	
	update_tip(default_tip)
	
	# select the highest score
	if score_id < 0:
		for player in global_vars.score:
			if global_vars.score[player] > score:
				score    = global_vars.score[player]
				score_id = player
	
	# the highest was player 1, now selct player 2
	elif score_id == global_vars.P1:
		score    = global_vars.score[global_vars.P2]
		score_id = global_vars.P2
	
	# the highest was player 2, now select player 1
	else:
		score = global_vars.score[global_vars.P1]
		score_id = global_vars.P1
	
	# update the interface
	if is_hi_score(score):
		$Congratulations.text = tr(congrat_text) + " " + global_vars.player_names[score_id]
		clear_initials()
	
	else:
		err = get_tree().change_scene(next_scene)
		
		if err != 0:
			print("err: ", err)

# return true if the score is an hi score
func is_hi_score(c_score : int) -> bool:
	var low : int
	var gm  : int = global_vars.game_mode
	
	# try harder next time
	if c_score <= 0:
		return false
	
	# we don't have high standards, just add whatever isn't zero by now
	if !gm in global_vars.current_hs || global_vars.current_hs[gm] < global_vars.max_hs:
		return true
	
	# ok now we can start to be selective
	low = global_vars.get_lower_hi_score()
	
	if c_score >= low:
		return true
	
	return false

# clear the initials labels
func clear_initials() -> void:
	var idx : int
	
	init_index   = 0
	letter_index = 0
	
	for label in init_array:
		if idx == 0:
			label.text = "A"
		
		else:
			label.text = " "
		
		idx += 1

# change letter of the currently selected initial
func change_letter(up : bool = true) -> void:
	if init_index >= len(init_array):
		return
	
	# select a new index
	if up:
		letter_index += 1
	
	else:
		letter_index -= 1
	
	# clamp it to letters array
	if letter_index > len(letters) - 1:
		letter_index = 0
	
	elif letter_index < 0:
		letter_index = len(letters) - 1
	
	# change the letter accordingly
	update_label()

# update current label with the right letter
func update_label() -> void:
	init_array[init_index].text = letters[letter_index]

# confirm current initial
func confirm_letter() -> void:
	var initials : String = ""
	
	init_index += 1
	
	# update the tip and wait for another input6
	if init_index == len(init_array):
		update_tip(confirm_tip)
	
	# initials confirmed, update high scores
	elif init_index > len(init_array):
		for n in init_array:
			initials += n.text
		
		update_high_scores(initials)
	
	# just update the next label
	else:
		update_label()

# cancel current letter and edit it again
func cancel_letter() -> void:
	var new_index : int
	
	if init_index > 0:
		if init_index < len(init_array):
			init_array[init_index].text = " "
		
		else:
			update_tip(default_tip)
		
		init_index = min(len(init_array), init_index) - 1
		
		# search for the current letter index and restart from there
		new_index = letters.find(init_array[init_index].text)
		
		if new_index > -1:
			letter_index = new_index

# update tip text
func update_tip(text : String) -> void:
	$Tip.text = text

func update_high_scores(initials : String) -> void:
	var err     : int
	var f       : File       = File.new()
	var low     : int        = 0
	var found   : bool       = false
	var s_dict  : Dictionary = {global_vars.game_mode : {score : initials}}
	var gm      : int        = global_vars.game_mode
	
	# no high scores file found, create a new one
	if not f.file_exists(global_vars.HISCORES):
		err = f.open_encrypted_with_pass(global_vars.HISCORES, File.WRITE, global_vars.PASSWORD)
		
		if err != OK:
			print("ERROR: unable to store high scores, code: ", err)
		
		else:
			f.store_line(to_json(s_dict))
			f.close()
			
			global_vars.hi_scores[gm]        = {}
			global_vars.hi_scores[gm][score] = [initials]
	
	# add high score to existing file
	else:
		# if it doesn't exist, add a new voice to our dictionary
		if !gm in global_vars.hi_scores:
			global_vars.hi_scores[gm] = {}
		
		if len(global_vars.hi_scores[gm]) > 0:
			for key in global_vars.hi_scores[gm]:
				
				if key == score:
					global_vars.hi_scores[gm][key].push_front(initials)
					found = true
					break
		
		if !found:
			global_vars.hi_scores[gm][score] = [initials]
		
		#print(global_vars.hi_scores)
		if gm in global_vars.current_hs:
			global_vars.current_hs[gm] += 1
		
		else:
			global_vars.current_hs[gm] = 1
		
		global_vars.trim_hi_scores()
		
		err = f.open_encrypted_with_pass(global_vars.HISCORES, File.WRITE, global_vars.PASSWORD)
		
		if err != 0:
			print("WARNING: error in storing high scores, code: ", err)
		
		f.store_line(to_json(global_vars.hi_scores))
		f.close()
	
	scores_checked += 1
	
	if scores_checked < len(global_vars.score):
		select_next_score()
	
	else:
		err = get_tree().change_scene(next_scene)
		
		if err != 0:
			print("err: ", err)

### signals ###

# user inputs selecting proper player input
func _input(event : InputEvent) -> void:
	
	if score_id < 0:
		return
	
	if event.is_action_pressed(global_vars.controls[score_id][global_vars.UP]):
	  change_letter()
	
	elif event.is_action_pressed(global_vars.controls[score_id][global_vars.DN]):
		change_letter(false)
	
	elif event.is_action_pressed(global_vars.controls[score_id][global_vars.FIRE]):
		confirm_letter()
	
	elif event.is_action_pressed(global_vars.controls[score_id][global_vars.CANCEL]):
		cancel_letter()
