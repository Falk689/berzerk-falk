extends Popup

enum {SOUND,
	  VOICE}

onready var global_vars : Node     =  get_node("/root/Global")

### signals definitions ###
signal pause_restart                                      # the player wants to restart the game
signal main_menu                                          # the player wants to go to the main menu


### vars used in code ###
var can_unpause         : bool  = false                   # used to add a slight delay to unpause
var mouse_hidden        : bool  = true                    # used to hide and show the cursor

var sound_volume        : float                           # stored sounds volume
var voice_volume        : float                           # stored voices volume
var set_fullscreen      : bool                            # stored fullscreen state


# show pause menu
func show_pause_menu() -> void:
	can_unpause = false
	popup_centered()
	$Menu/Ready.grab_focus()
	$UnpauseTimer.start()


# show settings menu and set default values
func show_settings() -> void:
	sound_volume                             = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Sounds"))
	voice_volume                             = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Voices"))
	
	sound_volume                             = db2linear(sound_volume)
	voice_volume                             = db2linear(voice_volume)
	
	set_fullscreen                           = OS.window_fullscreen
	
	$Settings/SoundsInput/VolumeSlider.value = stepify(sound_volume * 100.0, 1.0)
	$Settings/VoicesInput/VolumeSlider.value = stepify(voice_volume * 100.0, 1.0)
	$Settings/Fullscreen.pressed             = set_fullscreen
	
	$Menu.hide()
	$Settings.show()
	$Settings/Cancel.grab_focus()


# save current settings
func confirm_settings() -> void:
	OS.window_fullscreen = set_fullscreen
	
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear2db(sound_volume))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Voices"), linear2db(voice_volume))
	
	global_vars.config.set_value("audio", "volume", sound_volume)
	global_vars.config.set_value("audio", "voices", voice_volume)
	global_vars.config.set_value("interface", "fullscreen", set_fullscreen)
	
	global_vars.config.save(global_vars.CONFIG_FILE)

### signals ###

# remove pause state
func _on_Ready_pressed() -> void:
	hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

# restart the game
func _on_Restart_pressed() -> void:
	hide()
	emit_signal("pause_restart")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

# quit the game
func _on_Exit_pressed() -> void:
	get_tree().quit()


func _on_MainMenu_pressed() -> void:
	emit_signal("main_menu")


# user inputs, show mouse only when needed and remove pause state
func _input(event : InputEvent) -> void:
	var e_class : String
	
	if get_tree().paused:
		if can_unpause && (event.is_action_pressed("ui_cancel") || event.is_action_pressed("p2_cancel")):
			_on_SettingsCancel_pressed()
			_on_Ready_pressed()
		
		e_class = event.get_class()
		
		if mouse_hidden && e_class == "InputEventMouseMotion":
			mouse_hidden = false
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
		elif !mouse_hidden && e_class != "InputEventMouseMotion" && e_class != "InputEventMouseButton":
			mouse_hidden = true
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


func _on_UnpauseTimer_timeout() -> void:
	can_unpause = true


func _on_Settings_pressed() -> void:
	show_settings()


func _on_SettingsCancel_pressed() -> void:
	$Settings.hide()
	$Menu.show()
	$Menu/Ready.grab_focus()


func _on_SettingsConfirm_pressed() -> void:
	confirm_settings()
	_on_SettingsCancel_pressed()


func _on_VolumeSlider_value_changed(value : int, channel : int) -> void:
	
	if channel == SOUND:
		$Settings/SoundsInput/VolumeLabel.text = str(value) + "%"
		sound_volume = value / 100.0
	
	else:
		$Settings/VoicesInput/VolumeLabel.text = str(value) + "%"
		voice_volume = value / 100.0


func _on_Fullscreen_toggled(state : bool) -> void:
	set_fullscreen = state
