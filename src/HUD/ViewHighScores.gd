extends VBoxContainer

### global vars ###
onready var global_vars        : Node = get_node("/root/Global")


### exported vars not edited by code ###
export (String) var next_scene : String  = "res://src/HUD/MainMenu.tscn"                 # next scene to show after we inputed the initials
export (String) var font_path  : String  = "res://assets/theme/fonts/Atlantis-hs.tres"   # path for the font we're going to use for the hiscore labels
export (Color)  var idx_color  : Color   = Color(.9, 0, 0)                               # color for the high score index
export (Color)  var scr_color  : Color   = Color(.9, 1, .15)                             # color for the high score itself
export (Color)  var ini_color  : Color   = Color(.3, .1, .3)                             # color for the high score initials

### resources ###
var s_font                     : Resource = load(font_path)                               # font for our dynamic high scores

### vars used in code ###
onready var cur_hs             : int      = global_vars.game_mode                         # currently shown high scores
onready var header_text        : String   = $Header.text                                  # base header text

### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	VisualServer.set_default_clear_color(global_vars.BG_COLOR)
	
	if !cur_hs in global_vars.current_hs || global_vars.current_hs[cur_hs] <= 0:
		global_vars.read_hi_scores()
	
	#if cur_hs in global_vars.current_hs && global_vars.current_hs[cur_hs] > 0:
	show_hi_scores(cur_hs)
	
	$LoopTimer.start()


### custom methods ###

# add a new high score
func add_hi_score(ini : String, idx : int, score : int) -> void:
	var hi_cont : HBoxContainer = HBoxContainer.new()
	var i_label : Label = Label.new()
	var s_label : Label = Label.new()
	var n_label : Label = Label.new()
	
	# setting label fonts
	i_label.add_font_override("font", s_font)
	s_label.add_font_override("font", s_font)
	n_label.add_font_override("font", s_font)
	
	# setting label colors
	i_label.add_color_override("font_color", idx_color)
	s_label.add_color_override("font_color", scr_color)
	n_label.add_color_override("font_color", ini_color)
	
	# setting label text
	i_label.text = String(idx)
	s_label.text = String(score)
	n_label.text = String(ini)
	
	# container settings
	hi_cont.set_alignment(ALIGN_CENTER)
	hi_cont.margin_left = 0
	hi_cont.margin_right = 0
	
	# adding labels to the container
	hi_cont.add_child(i_label)
	hi_cont.add_child(s_label)
	hi_cont.add_child(n_label)
	
	# adding the container to its parent
	$HiScoreCont/HiScores.add_child(hi_cont)

# clear hi scores to show more
func clear_hi_scores() -> void:
	for c in $HiScoreCont/HiScores.get_children():
		c.queue_free()

# show currently stored hi scores
func show_hi_scores(mode : int) -> void:
	var idx  : int = 1
	var keys : Array
	
	# no hi scores are present, just exit
	if len(global_vars.hi_scores) < 1:
		exit_scene()
		return
	
	# current hi score class not found, select a proper one
	if !mode in global_vars.hi_scores:
		mode   = global_vars.hi_scores.keys()[0]
		cur_hs = mode
	
	keys = global_vars.hi_scores[mode].keys()
	
	keys.sort()
	keys.invert()
	
	$Header.text = tr(global_vars.mode_text[cur_hs]) + " - " + tr(header_text)
	$Header.add_color_override("font_color", global_vars.mode_color[cur_hs])
	
	for key in keys:
		for ini in global_vars.hi_scores[mode][key]:
			add_hi_score(ini, idx, key)
			idx += 1

# skips to the next scene
func exit_scene() -> void:
	var err = get_tree().change_scene(next_scene)
	
	if err != 0:
		print("Error while chaging scene, code: ", err)


# show next game mode high score
func show_next_hs(next : bool = true) -> void:
	var hs_classes : Array = global_vars.hi_scores.keys()
	var idx        : int   = hs_classes.find(cur_hs)
	
	if next:
		idx += 1
	
	else:
		idx -= 1
		
		if idx < 0:
			idx = len(hs_classes) - 1
	
	if len(hs_classes) == 0:
		exit_scene()
		return
	
	elif idx < len(hs_classes):
		cur_hs = hs_classes[idx]
	
	elif len(hs_classes) > 0:
		cur_hs = 0
	
	clear_hi_scores()
	show_hi_scores(cur_hs)


# stop the loop timer, checking if it's running
func stop_loop_timer(toggle : bool = false) -> void:
	if !$LoopTimer.is_stopped():
		$LoopTimer.stop()
		
		if !toggle:
			$LoopTimer.start()
	
	elif toggle:
		$LoopTimer.start()

### signals ###

# user inputs
func _input(event : InputEvent) -> void:
	if event.is_action_pressed("ui_select") || event.is_action_pressed("ui_cancel") || event.is_action_pressed("ui_click"):
		exit_scene()
	
	elif event.is_action_pressed("ui_left"):
		stop_loop_timer()
		show_next_hs(false)
	
	elif event.is_action_pressed("ui_right"):
		stop_loop_timer()
		show_next_hs()
	
	elif event.is_action_pressed("ui_up") || event.is_action_pressed("ui_down"):
		stop_loop_timer(true)

# automatically loop through hi scores
func _on_LoopTimer_timeout() -> void:
	show_next_hs()
	$LoopTimer.start()
