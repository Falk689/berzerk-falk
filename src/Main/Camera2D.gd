extends Camera2D

### global vars ###
onready var global_vars               : Node     =  get_node("/root/Global")

var targets        : Array   = []                                  # targets array if two targets are present, we'll try to follow them both
var max_distance   : float   = 10000                               # max distance between targets to use the standard zoom
var zoom_multi     : float   = 0.000008                            # zoom out multiplier, makes zooming out smooth and as consistent as possible with targets distance
var min_zoom       : float   = 1.0                                 # minimum zoom expressed in a float
var min_zoom_v     : Vector2 = Vector2(min_zoom, min_zoom)         # minimum zoom expressed in a vector
var max_zoom       : float   = 1.5                                 # maximum zoom expressed in a float
var active         : bool    = true                                # used from the main class to decide what camera to use the room exit effect
var start_limit    : Vector2 = Vector2(limit_left, limit_top)      # start limit, top left corner
var end_limit      : Vector2 = Vector2(limit_right, limit_bottom)  # end limit, bottom right corner
var start_pos      : Vector2                                       # used to setup a start position in fixed mode

func _physics_process(_delta : float) -> void:
	var l          : int
	
	if targets == []:
		return
	
	for t in targets:
		if !is_instance_valid(t):
			targets.remove(l)
		
		else:
			l += 1
	
	l = len(targets)
	
	if l == 1:
		if is_instance_valid(targets[0]):
			position = targets[0].position
			setup_camera_zoom(position, targets[0].position)
	
	elif l > 0:
		if is_instance_valid(targets[0]) && is_instance_valid(targets[1]):
			position = (targets[0].position + targets[1].position) / 2
			
			setup_camera_zoom(targets[0].position, targets[1].position)


# setup zoom based on two vectors
func setup_camera_zoom(pos_a : Vector2, pos_b : Vector2) -> void:
	var distance : float   = pos_a.distance_squared_to(pos_b)
	var v        : Vector2
	
	if distance > max_distance:
		v.x  = distance - max_distance
		v.y += distance - max_distance
		
		distance = min(1.0 + v.x * zoom_multi, max_zoom)
		
		v.x = distance
		v.y = distance
		
		set_zoom(v)
	
	elif zoom != min_zoom_v:
		set_zoom(min_zoom_v)


# stop tracking targets
func stop_tracking() -> void:
	targets = []


# setup camera limits
func set_camera_limits(start : Vector2, end : Vector2) -> void:
	# warning-ignore:narrowing_conversion
	limit_left   = start.x
	# warning-ignore:narrowing_conversion
	limit_top    = start.y
	# warning-ignore:narrowing_conversion
	limit_right  = end.x
	# warning-ignore:narrowing_conversion
	limit_bottom = end.y


# reset camera limits to vanilla
func reset_camera_limits(map_size : Vector2) -> void:
	var camera_center : Vector2 = global_vars.ORIG_SIZE / 2
	
	# setting up proper position since we're disabling limits
	if position.x - camera_center.x < 0:
		position.x = camera_center.x
	
	elif position.x - map_size.x + camera_center.x > 0:
		position.x = map_size.x - camera_center.x
	
	if position.y - camera_center.y < 0:
		position.y = camera_center.y
	
	elif position.y - map_size.y + camera_center.y > 0:
		position.y = map_size.y - camera_center.y
	
	set_camera_limits(start_limit, end_limit)

# add a target to this camera checking if it's already present
func add_target(target : Node2D):
	if !target in targets:
		targets.append(target)
