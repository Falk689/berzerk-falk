extends Node

### constants ###

enum {P1,                                                                              # players ids
	  P2}

enum {STOP,                                                                            # how we'll refer to directions in every class of the code
	  RX,
	  LX,
	  UP,
	  DN,
	  RX_UP,
	  LX_UP,  
	  RX_DN,
	  LX_DN,
	  FIRE,
	  CANCEL}

enum {CLASSIC,                                                                         # game modes, used to select the proper high score
	  HARDCORE,
	  CUSTOM,
	  M_CLASSIC,
	  M_HARDCORE,
	  M_CUSTOM}

enum {EXTRA_LIVES_DEFAULT,                                                             # states for the multiplayer lives award
	  EXTRA_LIVES_DEAD,
	  EXTRA_LIVES_DEAD_ONLY,
	  EXTRA_LIVES_ALIVE}

const ORIG_FPS          : float     = 60.0                                                               # original framerate of the arcade version of the game
const ORIG_SIZE         : Vector2   = Vector2(324, 224)                                                  # override size for viewports
const PASSWORD          : String    = "%&-.~Berzerk,+;689;+,Falk~.-&%"                                   # high scores encryption key
const HISCORES          : String    = "user://hiscores.bin"                                              # high scores file name
const CONFIG_FILE       : String    = "user://settings.cfg"                                              # settings file name
const GAMEMODE_FILE     : String    = "user://gamemodes.bin"

const ROBOT_GROUP       : String = "Robot"
const PLAYER_GROUP      : String = "Player"
const PLAYER_HIT_GROUP  : String = "PlayerHitbox"
const ENEMY_GROUP       : String = "Enemy"
const LASER_GROUP       : String = "Laser"
const OTTO_GROUP        : String = "Otto"
const HITBOX_GROUP      : String = "Hitbox"
const ENEMY_LASER_GROUP : String = "Enemy_Laser"
const PATH_GROUP        : String = "Path_Group_"
const SELECT_ACTION     : String = "ui_select"
const ACCEPT_ACTION     : String = "ui_accept"
const OTHER_GAME        : String = "OTHER_GAME_MODE"
const LEVELS_KEY        : String = "LEVELS"
const BG_COLOR          : Color  = Color(0, 0, 0)

### configuration vars ###

## base ##
var levels_keys                 = ["SCORE",
								   "MAX_LASERS",
								   "FAST_LASERS",
								   "FIRE_RATE_SHORT",
								   "SPEED_SHORT",
								   "BERZERK_SPEED_SHORT",
								   "ANIM_SPEED",
								   "BERZERK_ANIM_SPEED",
								   "COLOR",
								   "REM_ADD"]

var level_values : Array        =          [[0,   50,   -1],      # max lasers  - [minimum value, maximum value, connected index]
											null,                 # fast lasers
											[0.2, 5.0,  -1],      # fire rate
											[5,   100,  4],       # walk speed
											[5,   100,  3],       # berzerk walk speed
											[0.1, 10.0, 6],       # anim speed
											[0.1, 10.0, 5]]       # berzerk anim speed

var levels : Dictionary         = {0     : [0, false, 1.66, 8,  15, 0.5, 1.5, Color(0.42, 0.42, 0)],          # difficulty levels list. Needed points : [max_lasers,                                                              
								   300   : [1, false, 1.66, 10, 20, 0.8, 2.0, Color(1, 0, 0)],                #                                          fast_lasers,
								   1500  : [2, false, 0.5,  10, 40, 1.0, 2.5, Color(0, 0.42, 0.42)],          #                                          fire_rate,
								   3000  : [3, false, 0.5,  15, 50, 1.5, 3.0, Color(0, 1, 0)],                #                                          walk_speed,
								   4500  : [4, false, 0.5,  15, 50, 1.5, 3.0, Color(0.42, 0, 0.42)],          #                                          berzerk_speed,
								   6000  : [5, false, 0.5,  15, 50, 1.5, 3.0, Color(1, 1, 0)],                #                                          anim_speed,
								   7500  : [1, true,  0.83, 15, 50, 1.5, 3.0, Color(1, 1, 1)],                #                                          berzerk_anim_speed,
								   9000  : [1, true,  0.66, 15, 50, 1.5, 3.0, Color(1, 1, 1)],                #                                          robot_color]
								   10000 : [2, true,  0.5,  15, 50, 1.5, 3.0, Color(0, 0.42, 0.42)],          # 
								   11000 : [3, true,  0.5,  15, 50, 1.5, 3.0, Color(1, 0, 1)],                # default values are as close as vanilla as possible
								   13000 : [4, true,  0.5,  15, 50, 1.5, 3.0, Color(0.42, 0.42, 0.42)],
								   15000 : [5, true,  0.5,  15, 50, 1.5, 3.0, Color(0.42, 0.42, 0.42)],
								   17000 : [5, true,  0.5,  15, 50, 1.5, 3.0, Color(1, 0, 0)],
								   19000 : [5, true,  0.5,  15, 50, 1.5, 3.0, Color(0, 1, 1)]}

var default_levels : Dictionary = {0     : [0, false, 1.66,  8,  15, 0.5, 1.5, Color(0.42, 0.42, 0)],         # default values for the levels
								   300   : [1, false, 1.66,  10, 20, 0.8, 2.0, Color(1, 0, 0)],
								   1500  : [2, false, 0.5,   10, 40, 1.0, 2.5, Color(0, 0.42, 0.42)],
								   3000  : [3, false, 0.5,   15, 50, 1.5, 3.0, Color(0, 1, 0)],
								   4500  : [4, false, 0.5,   15, 50, 1.5, 3.0, Color(0.42, 0, 0.42)],
								   6000  : [5, false, 0.5,   15, 50, 1.5, 3.0, Color(1, 1, 0)],
								   7500  : [1, true,  0.83,  15, 50, 1.5, 3.0, Color(1, 1, 1)],
								   9000  : [1, true,  0.66,  15, 50, 1.5, 3.0, Color(1, 1, 1)],
								   10000 : [2, true,  0.5,   15, 50, 1.5, 3.0, Color(0, 0.42, 0.42)],
								   11000 : [3, true,  0.5,   15, 50, 1.5, 3.0, Color(1, 0, 1)],
								   13000 : [4, true,  0.5,   15, 50, 1.5, 3.0, Color(0.42, 0.42, 0.42)],
								   15000 : [5, true,  0.5,   15, 50, 1.5, 3.0, Color(0.42, 0.42, 0.42)],
								   17000 : [5, true,  0.5,   15, 50, 1.5, 3.0, Color(1, 0, 0)],
								   19000 : [5, true,  0.5,   15, 50, 1.5, 3.0, Color(0, 1, 1)]}

var win_tgt_size : Vector2    = Vector2(972, 672)                                                # initial window target size in pixels

## score ##
var max_hs       : int        = 10                                                               # max stored hi scores per game mode

var human_dirs   : Dictionary = {RX         : "RIGHT",                                           # human readable directions
								 LX         : "LEFT",
								 UP         : "UP",
								 DN         : "DOWN",
								 FIRE       : "FIRE",
								 CANCEL     : "CANCEL"}

var mode_text    : Dictionary = {CLASSIC    : "CLASSIC",                                         # how we'll refer to different modes in a human readable way
								 HARDCORE   : "HARDCORE",
								 CUSTOM     : "CUSTOM",
								 M_CLASSIC  : tr("MULTIPLAYER") + " " + tr("CLASSIC"),
								 M_HARDCORE : tr("MULTIPLAYER") + " " + tr("HARDCORE"),
								 M_CUSTOM   : tr("MULTILAYER") + " " + tr("CUSTOM")}

var mode_color   : Dictionary = {CLASSIC    : Color(0, 1, 0),                                    # colors for the interface when we'll refer to different modes
								 HARDCORE   : Color(1, 0, 0),
								 CUSTOM     : Color(.9, 1, .15),
								 M_CLASSIC  : Color(0, .42, .42),
								 M_HARDCORE : Color(1, 1, 0),
								 M_CUSTOM   : Color(.3, .1, .3)}

var player_names : Dictionary = {P1 : tr("PLAYER") + " 1",                            # names used to refer to the players
								 P2 : tr("PLAYER") + " 2"}

var languages    : Dictionary = {"English"             : "en",                        # selectable languages
								 "Italiano"            : "it"}

var controls     : Dictionary = {P1 : {UP     : "ui_up",                              # player actions corresponding to local controls
									   DN     : "ui_down",
									   LX     : "ui_left",
									   RX     : "ui_right",
									   FIRE   : "ui_select",
									   CANCEL : "ui_cancel"},
								
								 P2 : {UP     : "p2_up",
									   DN     : "p2_down",
									   LX     : "p2_left",
									   RX     : "p2_right",
									   FIRE   : "p2_fire",
									   CANCEL : "p2_cancel"}
}

# game mode configuration dictionary - config_class : {config-name : [value, default_value, tweak_increment, min_value, max_value, tooltip description, optional additional arguments]}
var game_config  : Dictionary = { "MAP"    : {"MAP_SIZE"             : [Vector2(6, 4), Vector2(6, 4), 1, 4, 128, "MAP_SIZE_DESCRIPTION"],
											  "ELECTRIFIED_WALLS"    : [true, true, "ELECTRIFIED_WALLS_DESCRIPTION"],
											  "AREA_SIZE"            : [Vector2(16, 16), Vector2(16, 16), 1, 8, 32, "AREA_SIZE_DESCRIPTION"],
											  "RANDOM_DOORS"         : [false, false, "RANDOM_DOORS_DESCRIPTION"],
											  "WALL_SPAWN_CHANCE"    : [0.8, 0.8, 1, 0, 100, "WALL_SPAWN_CHANCE_DESCRIPTION", "%"]},
									
								  "PLAYER" : {"SPEED"                : [50, 50, 1, 10, 80,  "PLAYER_SPEED_DESCRIPTION"],
											  "V_MOVE_SPEED"         : [0.9, 0.9, 1, 50, 100, "V_MOVE_SPEED_DESCRIPTION", "%"],
											  "FIRE_RATE"            : [0.5, 0.5, 0.01, 0.2, 2.0, "FIRE_RATE_DESCRIPTION", "f"],
											  "LASER_SPEED"          : [220, 220, 1, 100, 1000, "LASER_SPEED_DESCRIPTION"],
											  "INITIAL_LIVES"        : [3, 3, 1, 0, 30, "INITIAL_LIVES_DESCRIPTION"],
											  "AWARD_EXTRA_LIVES"    : [true, true, "AWARD_EXTRA_LIVES_DESCRIPTION"],
											  "EXTRA_LIVES_SCORE"    : [5000, 5000, 500, 500, 100000, "EXTRA_LIVES_SCORE_DESCRIPTION"],
											  "EXTRA_LIVES_AMOUNT"   : [1, 1, 1, 1, 5, "EXTRA_LIVES_AMOUNT_DESCRIPTION"],
											  "EXTRA_LIVES_AGAIN"    : [false, false, "EXTRA_LIVES_AGAIN_DESCRIPTION"],
											  "RANDOM_INIT_DOOR"     : [false, false, "RANDOM_INIT_DOOR_DESCRIPTION"],
											  "LIMITED_AMMO"         : [false, false, "LIMITED_AMMO_DESCRIPTION"],
											  "INITIAL_AMMO"         : [10, 10, 1, 0, 60, "INITIAL_AMMO_DESCRIPTION"],
											  "AMMO_PER_ROBOT"       : [1.2, 1.2, 0.1, 0.2, 5.0, "AMMO_PER_ROBOT_DESCRIPTION", "f"],
											  "EXTRA_AMMO"           : [5, 5, 1, 0, 60, "EXTRA_AMMO_DESCRIPTION"],
											  "EXTRA_AMMO_FIRST"     : [0.5, 0.5, 1, 0, 100, "EXTRA_AMMO_FIRST_DESCRIPTION", "%"],
											  "EXTRA_AMMO_MIN_RECUR" : [2, 2, 1, 1, "EXTRA_AMMO_MAX_RECUR", "EXTRA_AMMO_MIN_RECUR_DESCRIPTION"],
											  "EXTRA_AMMO_MAX_RECUR" : [4, 4, 1, "EXTRA_AMMO_MIN_RECUR", 10, "EXTRA_AMMO_MAX_RECUR_DESCRIPTION"],
											  "MAX_PLAYER_AMMO"      : [24, 24, 1, "INITIAL_AMMO", 128, "MAX_PLAYER_AMMO_DESCRIPTION"],
											  "RANDOM_AMMO"          : [3, 3, 1, 0, 30, "RANDOM_AMMO_DESCRIPTION"],
											  "MIN_AMMO_PICKUP"      : [1, 1, 1, 1, "MAX_AMMO_PICKUP", "MIN_AMMO_PICKUP_DESCRIPTION"],
											  "MAX_AMMO_PICKUP"      : [5, 5, 1, "MIN_AMMO_PICKUP", 10, "MAX_AMMO_PICKUP_DESCRIPTION"],
											  "NO_EMPTY_ON_CHANGE"   : [true, true, "NO_EMPTY_ON_CHANGE_DESCRIPTION"],
											  "MIN_REFILL_ON_CHANGE" : [2, 2, 1, 1, "MAX_REFILL_ON_CHANGE", "MIN_REFILL_ON_CHANGE_DESCRIPTION"],
											  "MAX_REFILL_ON_CHANGE" : [5, 5, 1, "MIN_REFILL_ON_CHANGE", 10, "MAX_REFILL_ON_CHANGE_DESCRIPTION"],
											  "AMMO_SPAWN_TIME"      : [10.0, 10.0, 0.1, 2.0, 30.0, "AMMO_SPAWN_TIME_DESCRIPTION", "f"],
											  "MAX_AMMO_AT_ONCE"     : [6, 6, 1, 2, 30, "MAX_AMMO_AT_ONCE_DESCRIPTION"],
											  "MIN_AMMO_SPAWN"       : [3, 3, 1, 1, "MAX_AMMO_SPAWN", "MIN_AMMO_SPAWN_DESCRIPTION"],
											  "MAX_AMMO_SPAWN"       : [6, 6, 1, "MIN_AMMO_SPAWN", "MAX_AMMO_AT_ONCE", "MAX_AMMO_SPAWN_DESCRIPTION"],
											  "AMMO_IN_ROBOTS"       : [0.6, 0.6, 1, 0, 100, "AMMO_IN_ROBOTS_DESCRIPTION", "%"],
											  "ROBOT_AMMO_CHANCE"    : [0.5, 0.5, 1, 20, 100, "ROBOT_AMMO_CHANCE_DESCRIPTION", "%"]},
									
							 "MULTIPLAYER" : {"FRIENDLY_FIRE"        : [false, false, "FRIENDLY_FIRE_DESCRIPTION"],
											  "PLAYER_COLLISIONS"    : [false, false, "PLAYER_COLLISIONS_DESCRIPTION"],
											  "ADD_SCORE_WHILE_DEAD" : [true, true, "ADD_SCORE_WHILE_DEAD_DESCRIPTION"],
											  "RESPAWN_OVER_TIME"    : [false, false, "RESPAWN_OVER_TIME_DESCRIPTION"],
											  "RESPAWN_TIME"         : [10, 10, 1, 2, 120, "RESPAWN_TIME_DESCRIPTION"],
											  "RESPAWN_ZONE"         : [Vector2(2, 2), Vector2(2, 2), 1, 2, 12, "RESPAWN_ZONE_DESCRIPTION"],
											  "EXTRA_LIVES_BOTH"     : [EXTRA_LIVES_DEFAULT, EXTRA_LIVES_DEFAULT, ["EXTRA_LIVES_DEFAULT", EXTRA_LIVES_DEFAULT, null, "EXTRA_LIVES_DEFAULT_DESCRIPTION"], ["EXTRA_LIVES_DEAD", EXTRA_LIVES_DEAD, null, "EXTRA_LIVES_DEAD_DESCRIPTION"], ["EXTRA_LIVES_DEAD_ONLY", EXTRA_LIVES_DEAD_ONLY, null, "EXTRA_LIVES_DEAD_ONLY_DESCRIPTION"], ["EXTRA_LIVES_ALIVE", EXTRA_LIVES_ALIVE, null, "EXTRA_LIVES_ALIVE_DESCRIPTION"], "EXTRA_LIVES_BOTH_DESCRIPTION"]},
									
								  "ROBOT"  : {"MIN_ROBOTS"           : [4, 4, 1, 1, "MAX_ROBOTS", "MIN_ROBOTS_DESCRIPTION"],
											  "MAX_ROBOTS"           : [11, 11, 1, "MIN_ROBOTS", 1024, "MAX_ROBOTS_DESCRIPTION"],
											  "SPAWN_MORE"           : [false, false, "SPAWN_MORE_DESCRIPTION"],
											  "MAX_ROBOTS_AT_ONCE"   : [11, 11, 1, 4, 128, "MAX_ROBOTS_AT_ONCE_DESCRIPTION"],
											  "UPDATE_EVERY_SPAWN"   : [false, false, "UPDATE_EVERY_SPAWN_DESCRIPTION"],
											  "SPAWN_TIME"           : [5.0, 5.0, 0.5, 0.1, 10.0, "SPAWN_TIME_DESCRIPTION", "f"],
											  "SPAWN_DELAY"          : [1.0, 1.0, 0.1, 0.0, 2, "SPAWN_DELAY_DESCRIPTION", "f"],
											  "SPAWN_AROUND_PLAYERS" : [false, false, "SPAWN_AROUND_PLAYERS_DESCRIPTION"],
											  "SPAWN_ZONE"           : [Vector2(4, 4), Vector2(4, 4), 1, 1, 12, "SPAWN_ZONE_DESCRIPTION"],
											  "TELEPORT"             : [false, false, "TELEPORT_DESCRIPTION"],
											  "TELEPORT_FAR_ROBOTS"  : [false, false, "TELEPORT_FAR_ROBOTS_DESCRIPTION"],
											  "TELEPORT_DISTANCE"    : [5, 5, 1, 3, 64, "TELEPORT_DISTANCE_DESCRIPTION"],
											  "TELEPORT_TIME"        : [5.0, 5.0, 0.1, 2.0, 10.0, "TELEPORT_TIME_DESCRIPTION", "f"],
											  "TELEPORT_AMOUNT"      : [12, 12, 4, 4, 128, "TELEPORT_AMOUNT_DESCRIPTION"],
											  "TELEPORT_ZONE"        : [Vector2(4, 4), Vector2(4, 4), 1, 2, 12, "TELEPORT_ZONE_DESCRIPTION"],
											  "CONTROLLER_CLASS"     : ["res://src/Actors/Advanced_Controller.tscn", "res://src/Actors/Advanced_Controller.tscn", ["ADVANCED", "res://src/Actors/Advanced_Controller.tscn", "ADVANCED_ROBOT_CONTROLLER", "ADVANCED_CONTROLLER_DESCRIPTION"], ["LEGACY", "res://src/Actors/Controller.tscn", "LEGACY_ROBOT_CONTROLLER", "LEGACY_CONTROLLER_DESCRIPTION"], "CONTROLLER_CLASS_DESCRIPTION"],
											  "BERZERK_START_MIN"    : [1, 1, 1, 1, "BERZERK_START_MAX", "BERZERK_START_MIN_DESCRIPTION"],
											  "BERZERK_START_MAX"    : [1, 1, 1, "BERZERK_START_MIN", 128, "BERZERK_START_MAX_DESCRIPTION"],
											  "V_MOVE_SPEED"         : [1.0, 1.0, 1, 50, 100,  "V_MOVE_SPEED_DESCRIPTION", "%"],
											  "SLOW_LASER_SPEED"     : [80, 80, 1, 60, "FAST_LASER_SPEED", "SLOW_LASER_SPEED_DESCRIPTION"],
											  "FAST_LASER_SPEED"     : [160, 160, 1, "SLOW_LASER_SPEED", 1000, "FAST_LASER_SPEED_DESCRIPTION"]},
						
				"LEGACY_ROBOT_CONTROLLER"  : {"VANILLA_COL_DETECT"   : [true, true, "VANILLA_COL_DETECT_DESCRIPTION"],
											  "BARREL_AIM_ALIGN"     : [false, false, "BARREL_AIM_ALIGN_DESCRIPTION"],
											  "BERZERK_ADD_LASERS"   : [0, 0, 1, 0, 24, "BERZERK_ADD_LASERS_DESCRIPTION"],
											  "AMBIDEXTROUS"         : [false, false, "AMBIDEXTROUS_DESCRIPTION"],
											  "FREE_WHEN_STUCK"      : [false, false, "FREE_WHEN_STUCK_DESCRIPTION"],
											  "AVOID_ROBOT_COLLS"    : [false, false, "AVOID_ROBOT_COLLS_DESCRIPTION"]},
						
			   "ADVANCED_ROBOT_CONTROLLER" : {"VANILLA_COL_DETECT"   : [true, true, "VANILLA_COL_DETECT_DESCRIPTION"],
											  "PATH_NAVIGATION"      : [false, false, "PATH_NAVIGATION_DESCRIPTION"],
											  "PATH_MIN_LENGTH"      : [8, 8, 4, 4, "PATH_MAX_LENGTH", "PATH_MIN_LENGTH_DESCRIPTION"],
											  "PATH_MAX_LENGTH"      : [32, 32, 4, "PATH_MIN_LENGTH", 128, "PATH_MAX_LENGTH_DESCRIPTION"],
											  "PATH_MAX_MULTIPLIER"  : [2.5, 2.5, 0.1, 1.0, 3.0, "PATH_MAX_MULTIPLIER_DESCRIPTION", "f"],
											  "LINE_OF_SIGHT"        : [false, false, "LINE_OF_SIGHT_DESCRIPTION"],
											  "BARREL_AIM_ALIGN"     : [false, false, "BARREL_AIM_ALIGN_DESCRIPTION"],
											  "BERZERK_ADD_LASERS"   : [0, 0, 1, 0, 24, "BERZERK_ADD_LASERS_DESCRIPTION"],
											  "AMBIDEXTROUS"         : [false, false, "AMBIDEXTROUS_DESCRIPTION"],
											  "AVOID_ROBOT_COLLS"    : [false, false, "AVOID_ROBOT_COLLS_DESCRIPTION"]},
						
								  "OTTO"   : {"OTTO_SPAWN"           : [true, true, "OTTO_SPAWN_DESCRIPTION"],
											  "NORMAL_SPEED"         : [20, 20, 1, 10, "BERZERK_SPEED", "NORMAL_SPEED_DESCRIPTION"],
											  "BERZERK_SPEED"        : [50, 50, 1, "NORMAL_SPEED", 120, "BERZERK_SPEED_DESCRIPTION"],
											  "BOUNCE_AMOUNT"        : [50, 50, 1, 30, 80, "BOUNCE_AMOUNT_DESCRIPTION"],
											  "NORMAL_BOUNCE_SPEED"  : [2.0, 2.0, 0.1, 1.0, "BERZERK_BOUNCE_SPEED", "NORMAL_BOUNCE_SPEED_DESCRIPTION", "f"],
											  "BERZERK_BOUNCE_SPEED" : [4.0, 4.0, 0.1, "NORMAL_BOUNCE_SPEED", 8.0, "BERZERK_BOUNCE_SPEED_DESCRIPTION", "f"],
											  "TIMER_MULTI"          : [40, 40, 1, 1, 200, "TIMER_MULTI_DESCRIPTION"],
											  "TIME_PER_ROBOT"       : [1, 1, 1, 1, 120, "TIME_PER_ROBOT_DESCRIPTION"],
											  "COUNT_DOWN"           : [5, 5, 1, 0, 600, "COUNT_DOWN_DESCRIPTION"],
											  "KILL_T_BONUS"         : [1.33, 1.33, 0.01, 0, 10, "KILL_T_BONUS_DESCRIPTION", "f"]}
										  }

# User configuration dictionary
var user_config  : Dictionary    = {"VISUAL"   :  {"USE_SHADERS"         : [true, true, "USE_SHADERS_DESCRIPTION", false, false],
												   "PLAYER_ONE_COLOR"    : [Color(0, 1, 0), Color(0, 1, 0), "PLAYER_ONE_COLOR_DESCRIPTION", true, Color(0, 1, 0)],
												   "PLAYER_TWO_COLOR"    : [Color(.42, 0, .42), Color(.42, 0, .42), "PLAYER_TWO_COLOR_DESCRIPTION", true, Color(.42, 0, .42)],
												   "BACKGROUND_COLOR"    : [Color(0, 0, 0), Color(0, 0, 0), "BACKGROUND_COLOR_DESCRIPTION", true, Color(0, 0, 0)],
												   "WALLS_COLOR"         : [Color(0, 0, 0.42), Color(0, 0, 0.42), "WALLS_COLOR_DESCRIPTION", true, Color(0, 0, 0.42)],
												   "FREEZE_COLOR"        : [Color(0, 0, 0.42), Color(0, 0, 0.42), "FREEZE_COLOR_DESCRIPTION", true, Color(0, 0, 0.42)]},
												
									
									"CONTROLS" :  {"PLAYER_ONE_DZ"       : [0.04, 0.04, 0.01, 0, 0.1, "FIRE_DZ_DESCRIPTION", "f", true, 0.04],
												   "PLAYER_TWO_DZ"       : [0.04, 0.04, 0.01, 0, 0.1, "FIRE_DZ_DESCRIPTION", "f", true, 0.04]},
									
								 "MULTIPLAYER" : {"SPLIT_SCREEN"         : [false, false, "SPLIT_SCREEN_DESCRIPTION", false, false]}
								   }


# standard game modes to add to the custom interface
# the order with HARDCORE before CLASSIC is intended
var standard_modes : Dictionary = {"HARDCORE"          : [HARDCORE, M_HARDCORE],
								   "CLASSIC"           : [CLASSIC, M_CLASSIC]}

# preset custom game modes for the player to select in custom game mode
var custom_modes   : Dictionary = {"MEDIUM_MAP_NORMAL" : {"MAP"   : {"MAP_SIZE"              : Vector2(16, 12),
																	 "RANDOM_DOORS"          : true},
														  
														 "PLAYER" : {"EXTRA_LIVES_AGAIN"     : true},
														
													"MULTIPLAYER" : {"EXTRA_LIVES_BOTH"      : EXTRA_LIVES_DEAD},
														
														  "ROBOT" : {"MIN_ROBOTS"            : 12,
																	 "MAX_ROBOTS"            : 32,
																	 "SPAWN_MORE"            : true, 
																	 "MAX_ROBOTS_AT_ONCE"    : 12,
																	 "UPDATE_EVERY_SPAWN"    : true,
																	 "SPAWN_AROUND_PLAYERS"  : true,
																	 "SPAWN_ZONE"            : Vector2(6, 6),
																	 "TELEPORT"              : true,
																	 "TELEPORT_FAR_ROBOTS"   : true,
																	 "BERZERK_START_MAX"     : 2},
														
									   "ADVANCED_ROBOT_CONTROLLER" : {"VANILLA_COL_DETECT"   : false,
																	  "BARREL_AIM_ALIGN"     : true,
																	  "AVOID_ROBOT_COLLS"    : true},
														
															"OTTO" : {"TIMER_MULTI"          : 60,
																	  "TIME_PER_ROBOT"       : 2,
																	  "KILL_T_BONUS"         : 0.8}},
														
								   "MEDIUM_MAP_HARD"   : {OTHER_GAME          : "MEDIUM_MAP_NORMAL",
														  
														  "PLAYER" : {"EXTRA_LIVES_AGAIN"     : false},
														
														  "ROBOT" : {"MIN_ROBOTS"            : 24,
																	 "MAX_ROBOTS"            : 48,
																	 "BERZERK_START_MAX"     : 5},
														
									   "ADVANCED_ROBOT_CONTROLLER" : {"LINE_OF_SIGHT"        : true,
																	  "PATH_NAVIGATION"      : true,
																	  "AMBIDEXTROUS"         : true},
														
															"OTTO" : {"TIMER_MULTI"          : 50,
																	  "KILL_T_BONUS"         : 0.5,
																	  "BERZERK_BOUNCE_SPEED" : 6.0,
																	  "BERZERK_SPEED"        : 80}},
										
								   "BIG_MAP_NORMAL"    : {OTHER_GAME          : "MEDIUM_MAP_NORMAL",
														
														  "MAP"               : {"MAP_SIZE"          : Vector2(32, 24),
																				 "RANDOM_DOORS"      : true}},
								   
								   "BIG_MAP_HARD"      : {OTHER_GAME          : "MEDIUM_MAP_HARD",
														
														  "MAP"               : {"MAP_SIZE"          : Vector2(32, 24),
																				 "RANDOM_DOORS"      : true}}
								}

var hardcoded_modes = []                                                                            # names of the hardcoded custom modes, populated automatically before loading the file

### vars used in code ###

## base ##
var game_mode     : int           = CLASSIC                                                          # are we currently playing in hardcore mode?
var game_over     : bool          = false                                                            # if true show the game over menu
var difficulty    : int           = 0                                                                # current difficulty level
var read_config   : bool          = false                                                            # used to check if we already initialized the configuration dictionaries
var config        : ConfigFile                                                                       # current configuration instance

## sound ##
var play_music    : bool                                                                             # if true, play music during the main menu
var song_index    : int                                                                              # currently played song index

## score ##
var score         : Dictionary    = {}                                                               # current players score
var current_hs    : Dictionary    = {}                                                               # currently stored hi scores
var hi_scores     : Dictionary    = {}                                                               # current high scores

## player ##
var num_players   : int           = 1                                                                # how many players to spawn
var players_left  : int           = 0                                                                # how many players are left
var cur_door      : int           = LX                                                               # current door to spawn us from
var room          : int           = 0                                                                # what room are we in
var p2_color      : Color         = Color(.42, 0, .42)                                               # player 2 color
var friendly_fire : bool          = false                                                            # is friendly fire active? read by lasers
var vibration     : bool          = true                                                             # if true each controller able to vibrate will

## robots ##
var lasers        : int           = 0                                                                # how many lasers robots currently fired
var path_groups   : int           = 0                                                                # how many path groups we have created this room

# reset vars to initial values
func reset_vars(diff : bool = false) -> void:
	cur_door    = LX
	lasers      = 0
	room        = 0
	path_groups = 0
	score       = {}
	
	if diff:
		difficulty = 0


# populate the hi scores dictionary
func read_hi_scores() -> void:
	var node_data : Dictionary
	var mode_key  : int
	var score_key : int
	var err       : int
	var f         : File      = File.new()
	var dir       : Directory = Directory.new()
	
	current_hs = {}
	
	# first we check if the file exist and try to open it
	if f.file_exists(HISCORES):
		err        = f.open_encrypted_with_pass(HISCORES, File.READ, PASSWORD)
		
		hi_scores  = {}
		
		# if we're successful, we read the hi scores
		if err == OK:
			while f.get_position() < f.get_len():
				node_data = parse_json(f.get_line())
				
				# scores are stored into a dictionary
				for key in node_data:
					mode_key = int(key)
					
					# tiny bugfix to prevent issues when updating from old versions, may remove it after deploy
					if mode_key > M_CUSTOM:
						continue
					
					# each key (game mode) corresponds to another dictionary containing scores as keys
					# and a list of initials as value, we also store the length of said list into current_hs
					# for each key
					hi_scores[mode_key]  = {}
					current_hs[mode_key] = 0
					
					# looping through initials and adding them to the main dictionary
					for s_key in node_data[key]:
						score_key = int(s_key)
						
						hi_scores[mode_key][score_key] = []
						
						if typeof(node_data[key][s_key]) == TYPE_STRING:
							hi_scores[mode_key][score_key].append(node_data[key][s_key])
							
						else:
							for s in node_data[key][s_key]:
								hi_scores[mode_key][score_key].append(s)
						
							
						current_hs[mode_key] += len(hi_scores[mode_key][score_key])
			
			f.close()
			
			trim_hi_scores()
			
			# we haven't found a single high score, delete the file since it may be damaged
			if len(hi_scores) == 0:
				print("Deleting damaged high scores file...")
# warning-ignore:return_value_discarded
				dir.remove(HISCORES)
		
		else:
			print("WARNING: error in reading high scores, code: ", err)


# check if there are too many high scores stored and trim them
func trim_hi_scores() -> void:
	var low : int
	
	for key in current_hs:
		while current_hs[key] > max_hs:
			low = get_lower_hi_score(key)
			
			if low < 0:
				print("WARNING: unable to trim high scores, high score file may be damaged.")
				break
			
			if len(hi_scores[key][low]) == 1:
				hi_scores[key].erase(low)
			
			else:
				hi_scores[key][low].pop_back()
			
			current_hs[key] -= 1



# get current mode hi scores or an empty dictionary
func get_current_hi_scores() -> Dictionary:
	if game_mode in hi_scores:
		return hi_scores[game_mode]
	
	return {}


# get lower high score
func get_lower_hi_score(key : int = -1) -> int:
	var low   : int   = -1
	var t_key : int   = game_mode
	
	if key > -1:
		t_key = key
	
	if t_key in hi_scores:
		for key in hi_scores[t_key]:
			if low == -1 || key < low:
				low = key
	
	return low


# select a new game mode
func select_game_mode(new_mode : int) -> void:
	if new_mode != game_mode:
		if new_mode == HARDCORE || new_mode == M_HARDCORE:
			game_config["ADVANCED_ROBOT_CONTROLLER"]["VANILLA_COL_DETECT"][0]  = false
			game_config["ADVANCED_ROBOT_CONTROLLER"]["PATH_NAVIGATION"][0]     = true
			game_config["ADVANCED_ROBOT_CONTROLLER"]["AMBIDEXTROUS"][0]        = true
			game_config["ADVANCED_ROBOT_CONTROLLER"]["BARREL_AIM_ALIGN"][0]    = true
			game_config["ADVANCED_ROBOT_CONTROLLER"]["AVOID_ROBOT_COLLS"][0]   = true
			game_config["ADVANCED_ROBOT_CONTROLLER"]["LINE_OF_SIGHT"][0]       = true
			game_config["ADVANCED_ROBOT_CONTROLLER"]["BERZERK_ADD_LASERS"][0]  = 3
			
			game_config["PLAYER"]["EXTRA_LIVES_SCORE"][0]                      = 10000
			game_config["PLAYER"]["LIMITED_AMMO"][0]                           = true
			game_config["PLAYER"]["RANDOM_INIT_DOOR"][0]                       = true
			
			game_config["ROBOT"]["BERZERK_START_MAX"][0]                       = 3
			game_config["ROBOT"]["MIN_ROBOTS"][0]                              = 6
			game_config["ROBOT"]["MAX_ROBOTS"][0]                              = 24
			game_config["ROBOT"]["SPAWN_MORE"][0]                              = true
			game_config["ROBOT"]["UPDATE_EVERY_SPAWN"][0]                      = true
			game_config["ROBOT"]["SLOW_LASER_SPEED"][0]                        = 90
			game_config["ROBOT"]["FAST_LASER_SPEED"][0]                        = 170
			game_config["ROBOT"]["TELEPORT"][0]                                = true
			
			game_config["OTTO"]["NORMAL_SPEED"][0]                             = 40
			game_config["OTTO"]["BERZERK_SPEED"][0]                            = 80
			game_config["OTTO"]["NORMAL_BOUNCE_SPEED"][0]                      = 3.0
			game_config["OTTO"]["BERZERK_BOUNCE_SPEED"][0]                     = 6.0
			
			game_config["MAP"]["RANDOM_DOORS"][0]                              = true
		
		if new_mode == M_HARDCORE:
			game_config["MULTIPLAYER"]["FRIENDLY_FIRE"][0]                     = true
			game_config["MULTIPLAYER"]["PLAYER_COLLISIONS"][0]                 = true
			game_config["MULTIPLAYER"]["ADD_SCORE_WHILE_DEAD"][0]              = false
			  
		
		if new_mode == M_CLASSIC || new_mode == M_HARDCORE || new_mode == M_CUSTOM:
			num_players  = 2
			players_left = 0
		
		else:
			num_players  = 1
			players_left = 0
		
		game_mode      = new_mode


# clear levels and select new difficulty, defaulting on reset
func select_new_difficulty(new_levels : Dictionary = default_levels) -> void:
	var new_keys : Array = new_levels.keys()
	
	levels = {}
	
	new_keys.sort()
	
	for key in new_keys:
		levels[key] = []
		
		for setting in new_levels[key]:
			levels[key].append(setting)
	


# get a game mode configuration from the dictionary
func get_game_config(key : String, cfg : String) -> Array:
	if key in game_config && cfg in game_config[key]:
		return game_config[key][cfg]
	
	return [-1]


# get an user configuration from the dictionary
func get_user_config(key : String, cfg : String) -> Array:
	if key in user_config && cfg in user_config[key]:
		return user_config[key][cfg]
	
	return [-1]


# set default configurations to start a non custom mode
func reset_default_game_config() -> void:
	for key in game_config:
		
		num_players = 1
		
		for config_value in game_config[key]:
			game_config[key][config_value][0] = game_config[key][config_value][1]


# update current difficulty level
func update_difficulty() -> void:
	for key in levels:
		for player in score:
			if score[player] >= key:
				difficulty = key


# return a list of cardinal directions
func cardinal_directions(fill : Array = [], idx : int = -1, rand : bool = false) -> Array:
	var dirs : Array = range(RX, DN + 1)
	
	if len(fill) > 0:
		if rand:
			dirs.shuffle()
		
		for dir in dirs:
			if !dir in fill:
				if idx > 0:
					fill.insert(idx, dir)
					
				else:
					fill.append(dir)
		
		return fill
	
	return dirs


# save current custom game modes
func save_game_modes() -> bool:
	var err         : int
	var f           : File       = File.new()
	var saved_modes : Dictionary = {}
	
	err               = f.open_encrypted_with_pass(GAMEMODE_FILE, File.WRITE, PASSWORD)
	
	if err == OK:
		
		for mode in custom_modes:
			if !mode in hardcoded_modes:
				saved_modes[mode] = custom_modes[mode]
		
		
		f.store_line(to_json(saved_modes))
		f.close()
		return true
	
	print("ERROR: unable to open game modes file, code: ", err)
	
	f.close()
	return false

# save current custom game modes
func add_game_mode(new_name : String) -> bool:
	var opt_idx   : int
	
	if !new_name in hardcoded_modes:
		custom_modes[new_name] = {}
		
		for section in game_config:
			custom_modes[new_name][section]    = {}
			
			for cfg in game_config[section]:
				
				# set only if the setting wasn't standard
				if game_config[section][cfg][0] != game_config[section][cfg][1]:
					custom_modes[new_name][section][cfg] = game_config[section][cfg][0]
			
			if len(custom_modes[new_name][section]) == 0:
				custom_modes[new_name].erase(section)
		
		# levels were edited in an obvious way, store them
		if len(levels) != len(default_levels):
			custom_modes[new_name][LEVELS_KEY] = levels
		
		# check if levels were edited in a less obvious way
		else:
			for scr in levels:
				
				# a score was edited, store them and break
				if !scr in default_levels:
					custom_modes[new_name][LEVELS_KEY] = levels
					break
				
				opt_idx = 0
				
				while opt_idx < len(levels[scr]):
					# at least one option was edited, store them and return
					if levels[scr][opt_idx] != default_levels[scr][opt_idx]:
						custom_modes[new_name][LEVELS_KEY] = levels
						
						return save_game_modes()
					
					opt_idx += 1
		
		return save_game_modes()
	
	print("ERROR: Mode name reserved, try another one.")
	
	return false

# remove a game mode from the list
func remove_game_mode(mode_name : String) -> bool:
	
	if !mode_name in hardcoded_modes && mode_name in custom_modes:
		# warning-ignore:return_value_discarded
		custom_modes.erase(mode_name)
		
		return save_game_modes()
	
	print("ERROR: Nothing to remove.")
	
	return false

# load game modes from the file
func load_game_modes() -> void:
	var err       : int
	var diff_idx  : int
	var score_idx : int
	var f         : File = File.new()
	var node_data : Dictionary
	
	# store hardcoded modes so we don't overwrite them
	hardcoded_modes = []
	
	for key in custom_modes:
		hardcoded_modes.append(key)
	
	if f.file_exists(GAMEMODE_FILE):
		
		#custom_modes = {}
		
		err = f.open_encrypted_with_pass(GAMEMODE_FILE, File.READ, PASSWORD)
		
		if err == OK:
			while f.get_position() < f.get_len():
				node_data = parse_json(f.get_line())
				
				for key in node_data:
					
					custom_modes[key] = {}
					
					for section in node_data[key]:
						
						if section in game_config:
							
							custom_modes[key][section] = {}
							
							for cfg in node_data[key][section]:
								
								if cfg in game_config[section]:
									
									if typeof(game_config[section][cfg][1]) == TYPE_BOOL:
										custom_modes[key][section][cfg] = bool(node_data[key][section][cfg])
									
									elif typeof(game_config[section][cfg][1]) == TYPE_INT:
										custom_modes[key][section][cfg] = int(node_data[key][section][cfg])
									
									elif typeof(game_config[section][cfg][1]) == TYPE_REAL:
										custom_modes[key][section][cfg] = float(node_data[key][section][cfg])
									
									elif typeof(game_config[section][cfg][1]) == TYPE_VECTOR2:
										custom_modes[key][section][cfg] = str2var("Vector2" + node_data[key][section][cfg])
									
									elif typeof(game_config[section][cfg][1]) == TYPE_COLOR:
										custom_modes[key][section][cfg] = str2var("Color(" + node_data[key][section][cfg] + ")")
									
									elif typeof(game_config[section][cfg][1]) == TYPE_BOOL:
										custom_modes[key][section][cfg] = bool(node_data[key][section][cfg])
									
									elif typeof(game_config[section][cfg][0]) == TYPE_STRING:
										custom_modes[key][section][cfg] = node_data[key][section][cfg]
									
									else:
										print("UNKNOWN: ", " ", key, " ", section, " ", cfg, " ", node_data[key][section][cfg])
						
						# load difficulty level
						elif section == LEVELS_KEY:
							custom_modes[key][section] = {} 
							
							#print(typeof(node_data[key][section][0]) == TYPE_ARRAY)
							
							for scr in node_data[key][section]:
								score_idx                             = int(scr)
								custom_modes[key][section][score_idx] = []
								diff_idx                              = 0
								
								while diff_idx < len(levels[0]):
									
									if typeof(levels[0][diff_idx]) == TYPE_INT:
										custom_modes[key][section][score_idx].append(int(node_data[key][section][scr][diff_idx]))
									
									elif typeof(levels[0][diff_idx]) == TYPE_REAL:
										custom_modes[key][section][score_idx].append(float(node_data[key][section][scr][diff_idx]))
									
									elif typeof(levels[0][diff_idx]) == TYPE_BOOL:
										custom_modes[key][section][score_idx].append(bool(node_data[key][section][scr][diff_idx]))
									
									elif typeof(levels[0][diff_idx]) == TYPE_COLOR:
										custom_modes[key][section][score_idx].append(str2var("Color(" + node_data[key][section][scr][diff_idx] + ")"))
									
									else:
										print("UNKNOWN: ", custom_modes[key][scr][score_idx])
									
									diff_idx += 1
							
						# this value already is a string, just store it as is
						elif section == OTHER_GAME:
							custom_modes[key][section] = node_data[key][section]
						
						else:
							print("UNKNOWN: ", section)
		
		else:
			print("WARNING: Error while opening game modes file, code: ", err)
	
	f.close()

# return a list of diagonal directions
func diagonal_directions() -> Array:
	return range(RX_UP, LX_DN + 1)


# return a list of every possible direction
func directions() -> Array:
	return range(RX, LX_DN + 1)


# returns the opposite direction
func reversed_direction(dir : int) -> int:
	if dir == RX:
		return LX
	
	if dir == LX:
		return RX
	
	if dir == UP:
		return DN
	
	if dir == DN:
		return UP
	
	if dir == RX_UP:
		return LX_DN
	
	if dir == LX_UP:
		return RX_DN
	
	if dir == RX_DN:
		return LX_UP
	
	if dir  == LX_DN:
		return RX_UP
	
	return STOP


# get a new name for a path group
func get_new_path_group() -> String:
	var new_name : String = PATH_GROUP + str(path_groups)
	
	path_groups += 1
	
	return new_name
