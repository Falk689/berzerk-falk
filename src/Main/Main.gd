extends Node

enum {CHAT,                                                                                                                               # robot talking states
	  CLEAR,
	  CHICKEN}

const FALK        : int     = 689                                                                                                         # jolly number to use around the code
const MARK_AREA   : Vector2 = Vector2(3, 4)                                                                                               # size of the area of walls to mark nearby the player
const P_COL_CHECK : Vector2 = Vector2(1, 2)                                                                                               # collision check for the player, for the doors
const MARGIN      : int     = 1                                                                                                           # margin to spawn the player
const ORIG_FPS    : float   = 60.0                                                                                                        # original framerate of the arcade version of the game
const TILEMAP_S   : int     = 3                                                                                                           # tilemap size

### global vars ###
onready var global_vars         : Node =  get_node("/root/Global")


### exported vars ###


## scenes ##
export (PackedScene)          var Robot            : PackedScene                                                                         # robot class
export (PackedScene)          var Ammo             : PackedScene                                                                         # ammo pack class
export (PackedScene)          var Otto             : PackedScene                                                                         # evil otto class
export (PackedScene)          var OttoController   : PackedScene                                                                         # otto current controller class
export (PackedScene)          var Player           : PackedScene                                                                         # player class

## scene names ##
export (String)               var set_hs_scene     : String    = "res://src/HUD/NewHighScore.tscn"                                       # location of high score setting screen
export (String)               var main_menu_scene  : String    = "res://src/HUD/MainMenu.tscn"                                           # location of the main menu
export (String)               var view_hs_scene    : String    = "res://src/HUD/ViewHighScores.tscn"                                     # location of high score viewing screen

## player ##
export (int)                  var kill_score       : int       = 50                                                                      # points awarded on kill
export (int)                  var clear_score      : int       = 10                                                                      # points awarded on room clear (per robot killed)
export (bool)                 var play_life_sound  : bool      = true                                                                    # if true, we play a sound when a life is awarded
export (bool)                 var vanilla_laser    : bool      = true                                                                    # if true, activate vanilla laser color same color as the enemies

## visual ##
export (int)                  var camera_speed     : int       = 140                                                                     # camera transition speed in pixels per second
export (bool)                 var exit_effect      : bool      = true                                                                    # if true we play the map transition effect
export (Vector2)              var exit_margin      : Vector2   = Vector2(10, 10)                                                         # margin for the transition effect
export (int)                  var laser_dist       : int       = 15                                                                      # laser highlight distance

## audio ##
export (float)                var e_voice_delay    : float     = 0.2                                                                     # event voice delay, used to trigger the sound almost imediately
export (float)                var voice_min_pitch  : float     = 0.8                                                                     # voice minimum pitch
export (float)                var voice_max_pitch  : float     = 1.3                                                                     # voice maximum pitch
export (float)                var alert_chance     : float     = 0.15                                                                    # alert sentence chance
export (float)                var chicken_timer    : float     = 0.4                                                                     # delay to start player taunt so we don't overlap sounds
export (int)                  var min_voice_delay  : int       = 4                                                                       # minimum delay between voices after the first tick
export (int)                  var max_voice_delay  : int       = 15                                                                      # maximum delay between voices

export (Array, Array, String) var chat_words       : Array     = [["attack",                                                             # normal chat words, humanoid and intruder are used the escape sentence too
																   "charge",
																   "destroy",
																   "get",
																   "stop",
																   "shoot"],
																
																  ["it",
																   "the"],
																
																  ["humanoid",
																   "intruder",
																   "chicken"]]         

export (Array, String)       var alert_sentence    : Array      = ["intruder",                                                           # words used for the alert
																	"alert",
																	"intruder",
																	"alert"]

export (Array, String)       var chicken_sentence  : Array      = ["chicken",                                                            # words used for the taunt
																	"fight",
																	"like",
																	"a",
																	"robot"]

export (Array, String)       var escape_words      : Array       = ["the",                                                               # words used when a player exits a cleared room
																	"random",
																	"must",
																	"nawt",
																	"escape"]

export (Array, String)       var escape_names      : Array       = ["humanoid",                                                          # how robots call you while exiting a room
																	"intruder"]


### vars used in code ###

## map ##
onready var map_size                               : Vector2    = global_vars.get_game_config("MAP", "MAP_SIZE")[0]                      # map size in areas
onready var area_size                              : Vector2    = global_vars.get_game_config("MAP", "AREA_SIZE")[0]                     # area size in tiles
onready var wall_chance                            : float      = global_vars.get_game_config("MAP", "WALL_SPAWN_CHANCE")[0]             # select chance to spawn walls
onready var electrified_walls                      : bool       = global_vars.get_game_config("MAP", "ELECTRIFIED_WALLS")[0]                  # are the walls deadly?
onready var random_doors                           : bool       = global_vars.get_game_config("MAP", "RANDOM_DOORS")[0]                  # toggle random room doors
onready var freeze_color                           : Color      = global_vars.get_user_config("VISUAL", "FREEZE_COLOR")[0]               # color to assign to the entities when we freeze time
onready var walls_color                            : Color      = global_vars.get_user_config("VISUAL", "WALLS_COLOR")[0]                # color to assign to non marked walls
onready var background_color                       : Color      = global_vars.get_user_config("VISUAL", "BACKGROUND_COLOR")[0]           # color for the background

onready var mapgen                                 : Node       = $Viewports/ViewportContainer1/Viewport/MapGen                          # this should make easier to select the mapgen
onready var actors                                 : Node       = $Viewports/ViewportContainer1/Viewport/Actors                          # easily selectable actors container
onready var camera_one                             : Camera2D   = $Viewports/ViewportContainer1/Viewport/Camera2D                        # easy access to the first camera
onready var camera_two                             : Camera2D   = $Viewports/ViewportContainer2/Viewport/Camera2D                        # easy access to the second camera

var         exit_camera                            : Camera2D                                                                            # camera used for the exit effect
var         exit_position                          : Vector2                                                                             # exit camera start position

onready var viewport_one                                        = $Viewports/ViewportContainer1/Viewport
onready var viewport_two                                        = $Viewports/ViewportContainer2/Viewport
onready var viewcont_one                                        = $Viewports/ViewportContainer1
onready var viewcont_two                                        = $Viewports/ViewportContainer2

# vars used to replace map timer
var max_trans_ticks                                : int        = 30                                                                     # maximum process ticks to wait before trying to clean the map again
var room_transition                                : bool                                                                                # if true we start transitioning to another room
var room_trans_ticks                               : int                                                                                 # current ticks waited for the map to be clear

## player ##
var players                                        : Dictionary                                                                          # players dictionary
var players_ready                                  : bool       = false                                                                  # used to avoid processing until players are ready
var player_cameras                                 : bool       = false                                                                  # if true we need to add a camera on the player at respawn
var last_extra_life                                : Dictionary                                                                          # when, in points, we got our last extra life
var lives                                          : Dictionary                                                                          # players lives left
var ammo                                           : Dictionary                                                                          # stored ammo amount
var ammo_left                                      : int                                                                                 # ammo left to spawn
var spawned_ammo_packs                             : int                                                                                 # currently spawned ammo packs
var ammo_positions                                 : Array                                                                               # stored ammo packs positions
var robot_ammo_left                                : int                                                                                 # ammo left to spawn from robots
var next_extra_room                                : int                                                                                 # number of the next room where we'll spawn extra ammo

onready var initial_lives                          : int        = global_vars.get_game_config("PLAYER", "INITIAL_LIVES")[0]              # initial lives amount
onready var should_award_life                      : bool       = global_vars.get_game_config("PLAYER", "AWARD_EXTRA_LIVES")[0]          # award extra life only if true
onready var award_life_score                       : int        = global_vars.get_game_config("PLAYER", "EXTRA_LIVES_SCORE")[0]          # when we reach this score, award an extra life to the player
onready var award_life_amount                      : int        = global_vars.get_game_config("PLAYER", "EXTRA_LIVES_AMOUNT")[0]         # how many lives to award when we reached the points
onready var award_life_recur                       : bool       = global_vars.get_game_config("PLAYER", "EXTRA_LIVES_AGAIN")[0]          # if true, award a life every award_life_score points
onready var p_speed                                : int        = global_vars.get_game_config("PLAYER", "SPEED")[0]                      # player walk speed
onready var p_vanilla_m                            : float      = global_vars.get_game_config("PLAYER", "V_MOVE_SPEED")[0]               # vanilla movement multiplier (makes you slower while moving on the y axis)
onready var p_fire_rate                            : float      = global_vars.get_game_config("PLAYER", "FIRE_RATE")[0]                  # player fire rate
onready var p_laser_speed                          : int        = global_vars.get_game_config("PLAYER", "LASER_SPEED")[0]                # standard laser speed
onready var limited_ammo                           : bool       = global_vars.get_game_config("PLAYER", "LIMITED_AMMO")[0]               # if true we'll use limited ammo
onready var no_empty_on_change                     : bool       = global_vars.get_game_config("PLAYER", "NO_EMPTY_ON_CHANGE")[0]         # give us some ammo on room change if empty
onready var min_refill_on_change                   : int        = global_vars.get_game_config("PLAYER", "MIN_REFILL_ON_CHANGE")[0]       # minimum ammo to give to the player on room change if empty
onready var max_refill_on_change                   : int        = global_vars.get_game_config("PLAYER", "MAX_REFILL_ON_CHANGE")[0]       # maximum ammo to give to the player on room change if empty
onready var extra_ammo                             : int        = global_vars.get_game_config("PLAYER", "EXTRA_AMMO")[0]                 # extra ammo amount
onready var extra_ammo_first                       : float      = global_vars.get_game_config("PLAYER", "EXTRA_AMMO_FIRST")[0]           # chance for the first room to spawn extra ammo
onready var ammo_per_robot                         : float      = global_vars.get_game_config("PLAYER", "AMMO_PER_ROBOT")[0]             # ammo spawned per robot (final number will be rounded to an int)
onready var initial_ammo                           : int        = global_vars.get_game_config("PLAYER", "INITIAL_AMMO")[0]               # initial ammo amount
onready var random_ammo                            : int        = global_vars.get_game_config("PLAYER", "RANDOM_AMMO")[0]                # random amount of ammo every room, can be either positive or negative to add some randomness
onready var extra_ammo_min_recur                   : int        = global_vars.get_game_config("PLAYER", "EXTRA_AMMO_MIN_RECUR")[0]       # minimum amount of rooms to change to get an extra ammo recurrence
onready var extra_ammo_max_recur                   : int        = global_vars.get_game_config("PLAYER", "EXTRA_AMMO_MAX_RECUR")[0]       # maximum amount of rooms to change to get an extra ammo recurrence
onready var ammo_in_robots                         : float      = global_vars.get_game_config("PLAYER", "AMMO_IN_ROBOTS")[0]             # percentage of ammo found in dead robots instead of random spawns
onready var robot_ammo_chance                      : float      = global_vars.get_game_config("PLAYER", "ROBOT_AMMO_CHANCE")[0]          # base robot ammo spawn chance
onready var ammo_spawn_time                        : float      = global_vars.get_game_config("PLAYER", "AMMO_SPAWN_TIME")[0]            # time to wait before a next ammo batch is spawned
onready var max_ammo_at_once                       : int        = global_vars.get_game_config("PLAYER", "MAX_AMMO_AT_ONCE")[0]           # maximum amount of random ammo packs found in the map
onready var min_ammo_pickup                        : int        = global_vars.get_game_config("PLAYER", "MIN_AMMO_PICKUP")[0]            # minimum amount of ammo found in pickups
onready var max_ammo_pickup                        : int        = global_vars.get_game_config("PLAYER", "MAX_AMMO_PICKUP")[0]            # maximum amount of ammo found in pickups
onready var min_ammo_spawn                         : int        = global_vars.get_game_config("PLAYER", "MIN_AMMO_SPAWN")[0]             # minimum amount of ammo packs to place every spawn
onready var max_ammo_spawn                         : int        = global_vars.get_game_config("PLAYER", "MAX_AMMO_SPAWN")[0]             # maximum amount of ammo packs to place every spawn
onready var random_init_door                       : bool       = global_vars.get_game_config("PLAYER", "RANDOM_INIT_DOOR")[0]           # is the door in the first room random?
onready var player_one_color                       : Color      = global_vars.get_user_config("VISUAL", "PLAYER_ONE_COLOR")[0]           # color to assign to the entities when we freeze time
onready var player_one_dz                          : float      = global_vars.get_user_config("CONTROLS", "PLAYER_ONE_DZ")[0]            # fire deadzone for the player one
onready var player_two_dz                          : float      = global_vars.get_user_config("CONTROLS", "PLAYER_TWO_DZ")[0]            # fire deadzone for the player two

## multiplayer ##
onready var friendly_fire                          : bool       = global_vars.get_game_config("MULTIPLAYER", "FRIENDLY_FIRE")[0]         # toggle friendly fire
onready var player_colls                           : bool       = global_vars.get_game_config("MULTIPLAYER", "PLAYER_COLLISIONS")[0]     # toggle collisions between players
onready var add_score_while_dead                   : bool       = global_vars.get_game_config("MULTIPLAYER", "ADD_SCORE_WHILE_DEAD")[0]  # add shared score to dead players
onready var extra_lives_both                       : int        = global_vars.get_game_config("MULTIPLAYER", "EXTRA_LIVES_BOTH")[0]      # defines the behavior of extra lives awars
onready var respawn_over_time                      : bool       = global_vars.get_game_config("MULTIPLAYER", "RESPAWN_OVER_TIME")[0]     # toggle additional over time respawn
onready var respawn_time                           : int        = global_vars.get_game_config("MULTIPLAYER", "RESPAWN_TIME")[0]          # time to wait to respawn dead player
onready var respawn_zone                           : Vector2    = global_vars.get_game_config("MULTIPLAYER", "RESPAWN_ZONE")[0]          # zone to use for the respawn, expressed in areas
onready var split_screen                           : bool       = global_vars.get_user_config("MULTIPLAYER", "SPLIT_SCREEN")[0]          # toggle split screen
onready var player_two_color                       : Color      = global_vars.get_user_config("VISUAL", "PLAYER_TWO_COLOR")[0]           # define player two color

onready var respawn_count_down                     : int        = respawn_time                                                           # current count down for the respawn
var exited_player                                  : int        = -1                                                                     # stored id of the player who exited a door in multiplayer

var dead_player                                    : int                                                                                 # stored id of the dead player
var in_split_screen                                : bool                                                                                # if true we're in a split screen configuration

## robots ##
var total_robots                                   : int        = 0                                                                      # how many robots we will spawn this room
var spawned_robots                                 : int        = 0                                                                      # how many robots we currently have spawned
var robots_left                                    : int        = 0                                                                      # how many robots are left in this room
var total_spawned                                  : int        = 0                                                                      # how many robots we spawned since the beginning of this room
var in_berzerk                                     : bool       = false                                                                  # are robots in this room in berzerk?
var path_queue                                     : Array      = []                                                                     # controllers that requested a path lie here waiting for the green light

onready var min_robots                             : int        = global_vars.get_game_config("ROBOT", "MIN_ROBOTS")[0]                  # minimum amount of robots per room
onready var max_robots                             : int        = global_vars.get_game_config("ROBOT", "MAX_ROBOTS")[0]                  # maximum amount of robots per room
onready var max_robots_at_once                     : int        = global_vars.get_game_config("ROBOT", "MAX_ROBOTS_AT_ONCE")[0]          # don't spawn robots unless they're less than this number
onready var robots_spawn_time                      : float      = global_vars.get_game_config("ROBOT", "SPAWN_TIME")[0]                  # time to wait between robot spawns
onready var spawn_more                             : bool       = global_vars.get_game_config("ROBOT", "SPAWN_MORE")[0]                  # spawn more robots only if this is true, else cap them to max spawned
onready var update_spawn                           : bool       = global_vars.get_game_config("ROBOT", "UPDATE_EVERY_SPAWN")[0]          # toggle updates every spawn instead of per room
onready var spawn_delay                            : float      = global_vars.get_game_config("ROBOT", "SPAWN_DELAY")[0]                 # time to keep robot inactive after a spawn (unless it's the first spawn of the room)
onready var spawn_around_players                   : bool       = global_vars.get_game_config("ROBOT", "SPAWN_AROUND_PLAYERS")[0]        # if true, spawn robots around players
onready var spawn_zone                             : Vector2    = global_vars.get_game_config("ROBOT", "SPAWN_ZONE")[0]                  # zone around players to spawn robots
onready var teleport                               : bool       = global_vars.get_game_config("ROBOT", "TELEPORT")[0]                    # if true, teleport robots if needed
onready var teleport_dist                          : int        = global_vars.get_game_config("ROBOT", "TELEPORT_DISTANCE")[0]           # distance from the player, in areaas, to start teleporting robots
onready var teleport_zone                          : Vector2    = global_vars.get_game_config("ROBOT", "TELEPORT_ZONE")[0]               # zone, around the player, to use for teleports
onready var teleport_amount                        : int        = global_vars.get_game_config("ROBOT", "TELEPORT_AMOUNT")[0]             # max number of robots to teleport at once
onready var teleport_far                           : bool       = global_vars.get_game_config("ROBOT", "TELEPORT_FAR_ROBOTS")[0]         # if true, teleport far robots, else only accept teleport requests
onready var berzerk_start_min                      : int        = global_vars.get_game_config("ROBOT", "BERZERK_START_MIN")[0]           # minimum amount of robots to make them go berzerk
onready var berzerk_start_max                      : int        = global_vars.get_game_config("ROBOT", "BERZERK_START_MAX")[0]           # maximum amount of robots to make them go berzerk

onready var berzerk_start                          : int        = berzerk_start_min                                                      # how many robots left we'll need for them to go berzerk

onready var controller_path                        : String     = global_vars.get_game_config("ROBOT", "CONTROLLER_CLASS")[0]
onready var robot_controller                       : Resource   = load(controller_path)                                                  # our robot controller class

## otto ##
var evil_otto                                      : Node2D                                                                              # current evil otto instance
var otto_spawn                                     : Vector2    = Vector2()                                                              # otto spawn position
onready var otto_c_down                            : int        = global_vars.get_game_config("OTTO", "COUNT_DOWN")[0]                   # additional time, in frames, added to otto timer at room entrance
onready var kill_t_bonus                           : float      = global_vars.get_game_config("OTTO", "KILL_T_BONUS")[0]                 # time bonus for killing a robot, adds to Otto spawn timer
onready var time_per_robot                         : float      = global_vars.get_game_config("OTTO", "TIME_PER_ROBOT")[0]               # additional time per robot spawned at room change
onready var timer_multi                            : float      = global_vars.get_game_config("OTTO", "TIMER_MULTI")[0]                  # otto timer multiplier

## visual ##
var marked_cells                                   : Dictionary = {}                                                                     # stored marked cells
var marked_lasers                                  : Dictionary = {}                                                                     # stored marked lasers with their original colors
var config_size                                    : Vector2    = Vector2()                                                              # screen size as set in project config
var split_screen_size                              : Vector2    = Vector2()                                                              # size of the viewport container in split screen
var effect_size                                    : Vector2    = Vector2()                                                              # exit transition effect amount
var c_modulate                                     : Color      = Color(1, 1, 1)                                                         # current room modulate
var can_pause                                      : bool       = true                                                                   # if true we can pause pressing a button
onready var move_camera                            : int        = global_vars.STOP                                                       # direction to move the camera

## audio ##
var taunt_player                                   : bool       = false                                                                  # should we taunt the player?                
var talking                                        : bool       = false                                                                  # are we already talking?
var sentence_idx                                   : int        = 0                                                                      # current sentence index
var sentence                                       : Array      = []                                                                     # current random sentence
var chicken                                        : bool       = false                                                                  # should we include chicken in the chat?

### standard methods ###


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var d_map_size  : Vector2
	var d_area_size : Vector2
	
	global_vars.reset_vars()
	global_vars.friendly_fire = friendly_fire
	
	set_background_color(background_color)
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	viewport_two.world_2d = viewport_one.world_2d
	
	get_tree().get_root().connect('size_changed', self, '_on_viewport_size_changed')
	
	# setting up window configuration size
	config_size.x = ProjectSettings.get_setting("display/window/size/width")
	config_size.y = ProjectSettings.get_setting("display/window/size/height")
	
	split_screen_size = Vector2(config_size.x, config_size.y * 0.5 - 2)
	
	effect_size   = global_vars.ORIG_SIZE
	
	effect_size.x += exit_margin.x
	effect_size.y += exit_margin.y
	
	#randomize()
	
	# initial camera setup
	d_map_size  = global_vars.get_game_config("MAP", "MAP_SIZE")[1]
	d_area_size = global_vars.get_game_config("MAP", "AREA_SIZE")[1]
	
	# setup cameras. TODO: try to fix camera code on narrow maps
	if (global_vars.num_players == 1 || !split_screen) && (map_size.x * area_size.x > d_map_size.x * d_area_size.x || map_size.y * area_size.y > d_map_size.y * d_area_size.y):
		setup_single_camera()
	
	elif global_vars.num_players == 1 || (map_size <= d_map_size && area_size <= d_area_size):
		setup_fixed_camera()
	
	elif split_screen:
		player_cameras = true
		$HUD.split_screen()
	
	# prepare the map
	respawn_players(true)
	
	# show a zero in the player two score label instead of nothing, if needed
	if global_vars.num_players > 1:
		$HUD.player_two_score_ph = $HUD.zero_text
	
	# update HUD
	$HUD.update_score(global_vars.score)
	$HUD.update_lives(lives)
	
	if limited_ammo:
		$HUD.show_ammo(initial_ammo)
	
	# hide mouse cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	var t_pos        : Vector2
	var t_id         : int
	var h_id         : int
	var tile         : Vector2
	var laser_color  : Color
	var rem          : Array     = []
	var velocity     : Vector2   = Vector2()
	var n            : int       = 0
	
	# wait for the map to be clean then create next room
	if room_transition:
		if room_trans_ticks >= max_trans_ticks:
			room_trans_ticks = 0
			clean_map()
		
		elif actors.get_child_count() == 0:
			room_transition     = false
			
			camera_one.position = Vector2()
			camera_two.position = Vector2()
			
			# prepare to move the camera instantly
			camera_one.smoothing_enabled = false
			camera_two.smoothing_enabled = false
			
			respawn_players()
			return
		
		else:
			room_trans_ticks += 1
			return
	
	# map transition effect
	if move_camera != global_vars.STOP:
		if move_camera == global_vars.LX:
			if exit_camera.position.x > -effect_size.x:
				velocity.x -= 1
			
			else:
				move_camera = global_vars.STOP
		
		elif move_camera == global_vars.RX:
			if exit_camera.position.x - exit_position.x < effect_size.x:
				velocity.x += 1
			
			else:
				move_camera = global_vars.STOP
		
		elif move_camera == global_vars.UP:
			if exit_camera.position.y > -effect_size.y:
				velocity.y -= 1
			
			else:
				move_camera = global_vars.STOP
		
		else:
			if exit_camera.position.y - exit_position.y < effect_size.y:
				velocity.y += 1
			
			else:
				move_camera = global_vars.STOP
		
		if move_camera != global_vars.STOP:
			velocity = velocity.normalized() * camera_speed
			exit_camera.position += velocity * delta
	
	# don't do anything until ready
	if !players_ready:
		return
	
	# laser highlight and deletion
	for c in get_tree().get_nodes_in_group(global_vars.LASER_GROUP):
		if is_instance_valid(c) && !c.is_queued_for_deletion():
			if c.is_in_group(global_vars.ENEMY_LASER_GROUP):
				n += 1
				
				if !check_destroy_laser(c):
					for player in players:
						
						if is_instance_valid(players[player][0]) && !players[player][0].is_queued_for_deletion() && players[player][0].global_position.distance_to(c.position) < laser_dist:
							if !n in marked_lasers:
								laser_color   = c.get_color()
								marked_lasers[n]    = [c, laser_color, true, player]
								c.change_color(players[player][0].get_color())
						
						elif n in marked_lasers && marked_lasers[n][3] == player:
							marked_lasers[n][2] = false
			
			elif c.is_in_group(global_vars.LASER_GROUP):
				check_destroy_laser(c)
	
	# remove marked lasers from the dictionary
	
	for l in marked_lasers:
		
		if !is_instance_valid(marked_lasers[l][0]) || marked_lasers[l][0].is_queued_for_deletion():
			marked_lasers.erase(l)
		
		elif !marked_lasers[l][2]:
			marked_lasers[l][0].change_color(marked_lasers[l][1])
			marked_lasers.erase(l)
	
	# tile highlight and exit doors checks
	for p_id in players:
		if is_instance_valid(players[p_id][0]) && !players[p_id][0].is_queued_for_deletion() && players[p_id][0].ready:
			
			tile = mapgen.map.world_to_map(players[p_id][0].global_position)
			
			# don't compute this stuff more than once per cell
			if tile == players[p_id][1]:
				continue
			
			players[p_id][1] = tile
			
			# actual exit door collision happens here
			for _x in range(-P_COL_CHECK.x, P_COL_CHECK.x):
				
				if !players[p_id][0].ready:
					break
					
				for _y in range(-P_COL_CHECK.y + 1, P_COL_CHECK.y):
					
					t_id = mapgen.map.get_cell(players[p_id][1].x, players[p_id][1].y)
					
					if t_id == mapgen.DOOR:
						players[p_id][0].ready = false
						
						door_exited(p_id, players[p_id][1])
						break
				
			
			# tile highlight 
			for x in range(-MARK_AREA.x + 1, MARK_AREA.x):
				for y in range(-MARK_AREA.y +1, MARK_AREA.y):
					t_pos =  Vector2(tile.x + x, tile.y + y)
					
					h_id = mapgen.highlight.get_cell(t_pos.x, t_pos.y)
					
					if h_id == -1:
						t_id = mapgen.map.get_cell(t_pos.x, t_pos.y)
					
						if t_id > -1 && t_id < mapgen.DOOR:
							if !p_id in marked_cells:
								marked_cells[p_id] = []
							
							if ![t_pos, t_id] in marked_cells[p_id]:
								marked_cells[p_id].append([t_pos, t_id])
			
			n = 0
			
			if p_id in marked_cells:
				for cell in marked_cells[p_id]:
					if cell[0].x > tile.x + MARK_AREA.x - 1 || cell[0].x < tile.x - MARK_AREA.x + 1 || cell[0].y > tile.y + MARK_AREA.y - 1 || cell[0].y < tile.y - MARK_AREA.y + 1:
						rem.append(n)
					
					
					else:
						mapgen.highlight.set_cell(cell[0].x, cell[0].y, cell[1] + TILEMAP_S * p_id)
					
					n += 1
			
			rem.invert()
			
			for r in rem:
				if p_id in marked_cells && r < len(marked_cells[p_id]):
					mapgen.highlight.set_cell(marked_cells[p_id][r][0].x, marked_cells[p_id][r][0].y, -1)
					marked_cells[p_id].remove(r)
			
			#print(marked_cells)

# handle pause state here
func _input(event : InputEvent) -> void:
	if can_pause && (event.is_action_pressed("ui_cancel") || event.is_action_pressed("p2_cancel")):
		pause()

### custom methods ###

# setup a normal fixed camera viewing the whole map
func setup_fixed_camera():
	viewport_one.size = get_viewport().size
	
	viewport_one.set_size_override(true, global_vars.ORIG_SIZE)
	viewport_one.set_size_override_stretch(true)

# setup a single camera that follows the player
func setup_single_camera():
	viewport_one.size = get_viewport().size
	
	viewport_one.set_size_override(true, global_vars.ORIG_SIZE)
	viewport_one.set_size_override_stretch(true)
	player_cameras = true

# setup a split screen camera
func setup_split_screen():
	var new_size  : Vector2  = get_viewport().size
	var view_size : Vector2  = global_vars.ORIG_SIZE
	
	new_size.y  = floor(new_size.y  * .5)
	view_size.y = floor(view_size.y * .5)
	
	in_split_screen = true
	
	viewcont_one.set_size(split_screen_size)
	viewcont_two.set_size(split_screen_size)
	
	viewcont_one.show()
	viewcont_two.show()
	
	camera_one.active = true
	camera_two.active = true
	
	viewport_one.size = new_size
	viewport_one.set_size_override(true, view_size)
	viewport_one.set_size_override_stretch(true)
	player_cameras  = true
	
	viewport_two.size = new_size
	viewport_two.set_size_override(true, view_size)
	viewport_two.set_size_override_stretch(true)
	
	
	#$HUD.split_screen()

# set background color and change invisible tiles color
func set_background_color(color : Color) -> void:
	VisualServer.set_default_clear_color(color)
	mapgen.set_tiles_color([mapgen.DOOR], color)

# destroy a laser that escaped the screen
func check_destroy_laser(laser : Single_Laser) -> bool:
	var pos : Vector2 = mapgen.map.world_to_map(laser.global_position)
	
	if pos.x < 0 || pos.x > mapgen.map_size.x:
		laser.laser_destroy()
		return true
	
	if pos.y < 0 || pos.y > mapgen.map_size.y:
		laser.laser_destroy()
		return true
		
	return false


# clean the map from remaining robots and lasers
func clean_map() -> void:
	for c in actors.get_children():
		if is_instance_valid(c):
			c.queue_free()


# initialize robots at the start of a new room
func initialize_robots() -> void:
	# should return an int between min_robots and max_robots
	spawned_robots = 0
	total_spawned  = 0
	total_robots   = randi() % (max_robots - (min_robots - 1)) + min_robots
	robots_left    = total_robots
	
	# maybe I'll move this somewhere else, it's needed to show otto color
	c_modulate     = global_vars.levels[global_vars.difficulty][7]


# spawn the next robots squad
func spawn_robots(first : bool = false) -> void:
	var robot        : Standard_Robot
	var control      : Base_Controller
	var s_points     : Array = []
	var players_pos  : Array = []
	var enemy_pos    : Array = []
	var spawn_amount : int   = min(robots_left, max_robots_at_once)
	
	# cap robots to max_at_once
	if spawned_robots + spawn_amount > max_robots_at_once:
		spawn_amount = max_robots_at_once - spawned_robots
	
	# cap again to prevent spawning too many robots
	if total_spawned + spawn_amount > total_robots:
		spawn_amount = total_spawned + spawn_amount - total_robots
	
	# nothing to spawn, return
	if spawn_amount <= 0:
		return;
	
	# make them go berzerk
	if first && robots_left <= berzerk_start:
		in_berzerk = true
	
	# populate the player positions list
	if !first:
		for player in players:
			if is_instance_valid(players[player][0]):
				players_pos.append(players[player][1])
	
	# populate the enemy positions list
	for c in get_tree().get_nodes_in_group(global_vars.ENEMY_GROUP):
			if c.is_in_group(global_vars.ROBOT_GROUP):
				if c.last_tile == Vector2():
					enemy_pos.append(mapgen.map.world_to_map(c.global_position))
				
				else:
					enemy_pos.append(c.last_tile)
			
			elif c.is_in_group(global_vars.OTTO_GROUP):
				enemy_pos.append(mapgen.map.world_to_map(c.global_position))
	
	# should return an int between min_robots and max_robots
	#spawned_robots  = randi() % (max_robots - (min_robots - 1)) + min_robots
	
	s_points = mapgen.get_spawn_points(spawn_amount, players_pos, enemy_pos, spawn_around_players, spawn_zone)
	
	#robots_left     = spawned_robots
	
	for point in s_points:
		spawned_robots      += 1
		total_spawned       += 1
		
		if total_spawned > total_robots:
			break
		
		robot                   = Robot.instance()
		robot.robot_id          = total_spawned
		robot.electrified_walls = electrified_walls
		control                 = robot_controller.instance()
		
		control.start(robot, mapgen)
		control.connect("wants_path", self, "_on_controller_wants_path", [control])
		
		robot.add_child(control)
		robot.connect("enemy_hit", self, "_on_enemy_hit", [robot])
		#robot.add_to_group(global_vars.ENEMY_GROUP)
		#robot.add_to_group(global_vars.ROBOT_GROUP)
		
		point = mapgen.tile_center(point)
		actors.add_child(robot)
		
		if first:
			robot.start(point, in_berzerk)
		
		else:
			robot.start(point, in_berzerk, spawn_delay)
	
	if !spawn_more:
		total_robots = total_spawned
		robots_left  = total_spawned
	
	elif total_spawned < total_robots:
		$RobotSpawnTimer.wait_time = global_vars.get_game_config("ROBOT", "SPAWN_TIME")[0]
		$RobotSpawnTimer.start()
	
	# start teleport timer after the first spawn
	if first && teleport:
		$TeleportTimer.wait_time   = global_vars.get_game_config("ROBOT", "TELEPORT_TIME")[0]
		$TeleportTimer.start()

# spawn ammo packs
func spawn_random_ammo() -> void:
	var color        : Color = global_vars.levels[global_vars.difficulty][7]
	
	var s_points     : Array
	var players_pos  : Array
	var packs_amount : int
	
	# we spawned enough ammo, return
	if spawned_ammo_packs >= max_ammo_at_once || ammo_left <= 0:
		return
	
	#print("AMMO_LEFT: ", ammo_left, " SPAWNED: ", spawned_ammo_packs)
	
	for player in players:
		if is_instance_valid(players[player][0]):
			players_pos.append(players[player][1])
	
	if min_ammo_spawn == max_ammo_spawn:
		packs_amount = max_ammo_spawn
	
	else:
		packs_amount = randi() % (max_ammo_spawn - min_ammo_spawn) + min_ammo_spawn
	
	#print("INITIAL AMOUNT: ", packs_amount)
	
	packs_amount = min(max_ammo_at_once - spawned_ammo_packs, packs_amount)
	
	#print("CAPPED AMOUNT: ", packs_amount)
	
	s_points = mapgen.get_spawn_points(packs_amount, players_pos, ammo_positions, false, Vector2())
	
	#print("FINAL AMOUNT: ", len(s_points))
	
	if len(s_points) > 0:
		for point in s_points:
			if ammo_left == 0:
				if !$AmmoTimer.is_stopped():
					$AmmoTimer.stop()
				return
			
			spawn_ammo_pack(point, color)


# spawn an ammo pack to a given position
func spawn_ammo_pack(point : Vector2, color : Color, amount : int = 0, robot : bool = false) -> void:
	var ammo_pack    : Node2D = Ammo.instance()
	var ammo_in_pack : int
	
	actors.add_child(ammo_pack)
	actors.move_child(ammo_pack, 0)
	
	if amount <= 0:
		if min_ammo_pickup == max_ammo_pickup:
			ammo_in_pack = max_ammo_pickup
		
		else:
			ammo_in_pack = randi() % (max_ammo_pickup - min_ammo_pickup) + min_ammo_pickup
	
	else:
		ammo_in_pack = amount
	
	if robot:
		ammo_in_pack = min(robot_ammo_left, ammo_in_pack)
	
	else:
		ammo_in_pack = min(ammo_left, ammo_in_pack)
	
	#print("AMMO IN PACK: ", ammo_in_pack)
	
	ammo_pack.start(mapgen.tile_center(point), ammo_in_pack, color, point)
	
	ammo_positions.append(point)
	
	if robot:
		robot_ammo_left -= ammo_in_pack
	
	else:
		ammo_left -= ammo_in_pack


# check if some robot is too far away from the player and teleport it
func check_teleports() -> void:
	var close       : bool
	var distance    : Vector2
	var robot       : Standard_Robot
	var t_pos       : Vector2
	var s_points    : Array
	var t_robots    : Array
	var players_pos : Array
	var enemy_pos   : Array
	var n           : int
	
	# populate players positions
	for player in players:
		if is_instance_valid(players[player][0]) && !players[player][0].is_queued_for_deletion():
			players_pos.append(players[player][1])
	
	for c in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		close  = false
		
		if is_instance_valid(c) && !c.is_queued_for_deletion():
			
			# this robot wants to teleport, make it happy
			if c.wants_teleport:
				t_robots.append(c)
				n += 1
				continue
			
			
			if c.last_tile != Vector2():
				t_pos = c.last_tile
				enemy_pos.append(c.last_tile)
			
			else:
				t_pos = mapgen.map.world_to_map(c.global_position)
				enemy_pos.append(t_pos)
			
			# teleport far robots only if the player selected the right option
			if teleport_far:
				for player in players:
					distance = mapgen.get_area_distance(t_pos, players[player][1])
					
					if distance.x <= teleport_dist && distance.y <= teleport_dist:
						close = true
						break
				
				# we need to teleport
				if !close:
					t_robots.append(c)
					n += 1
					
					# we reached the maximum teleport amount, try again next time
					if n >= teleport_amount:
						break
	
	if len(t_robots) > 0:
		s_points = mapgen.get_spawn_points(len(t_robots), players_pos, enemy_pos, true, teleport_zone)
		
		for point in s_points:
			robot = t_robots.pop_front()
			point = mapgen.tile_center(point)
			
			robot.teleport(point, spawn_delay)


# return true if dead player is successfully respawned or we want to force the timer off
func in_game_respawn() -> bool:
	var p_id       : int      = -1
	var t_pos      : Vector2
	var s_point    : Array
	var enemy_pos  : Array
	var player_pos : Array
	
	# populate players positions
	for player in players:
		if is_instance_valid(players[player][0]) && !players[player][0].is_queued_for_deletion():
			player_pos.append(players[player][1])
		
		elif p_id == -1:
			p_id = player
		
		else:
			print("Error: Both players appear to be dead... Giving up.")
			return true
	
	# populate robot positions
	for c in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		
		if is_instance_valid(c) && !c.is_queued_for_deletion():
			
			if c.last_tile != Vector2():
				t_pos = c.last_tile
				enemy_pos.append(c.last_tile)
			
			else:
				t_pos = mapgen.map.world_to_map(c.global_position)
				enemy_pos.append(t_pos)
	
	# add evil otto if needed
	if is_instance_valid(evil_otto):
		t_pos = mapgen.map.world_to_map(evil_otto.global_position)
		enemy_pos.append(t_pos)	
	
	s_point = mapgen.get_spawn_points(1, player_pos, enemy_pos, true, respawn_zone)
	
	if len(s_point) > 0:
		spawn_player(p_id, mapgen.tile_center(s_point[0]), false)
		
		if $CameraTimer.is_stopped():
			$CameraTimer.start()
		
		return true
	
	return false


# spawn and initialize a single player
func spawn_player(p_id : int, spawn_pos : Vector2, first : bool = false) -> void:
	var player        : Node2D
	var used_deadzone : float
	var ammo_amount   : int
	
	player           = Player.instance()
	player.player_id = p_id
	players[p_id]    = [player, mapgen.map.world_to_map(spawn_pos)]
	
	actors.add_child(player)
	
	if vanilla_laser:
		player.change_laser_color(global_vars.levels[global_vars.difficulty][7])
	
	#player.add_to_group(global_vars.PLAYER_GROUP)
	player.connect("hit", self, "_on_Player_hit", [player])
	player.connect("dead", self, "_on_Player_dead", [player.player_id])
	
	if limited_ammo:
		player.connect("ammo_update", self, "_on_Player_ammo_update")
	
	if p_id == global_vars.P1:
		used_deadzone = player_one_dz
		player.change_color(player_one_color)
		
		if first:
			mapgen.set_tiles_highlight(player_one_color, p_id)
	
	
	else:
		used_deadzone = player_two_dz
		player.change_color(player_two_color)
		
		if first:
			mapgen.set_tiles_highlight(player_two_color, p_id)
	
	if player_cameras:
		if global_vars.players_left == 1 && split_screen:
			if !in_split_screen:
				setup_split_screen()
			
			if p_id == global_vars.P1:
				camera_one.smoothing_enabled = false
				camera_one.add_target(player)
			
			else:
				camera_two.smoothing_enabled = false
				camera_two.add_target(player)
		
		else:
			camera_one.smoothing_enabled = false
			camera_one.add_target(player)
	
	
	if p_id in ammo:
		ammo_amount = ammo[p_id]
	
	else:
		ammo_amount = initial_ammo
		ammo[p_id]  = initial_ammo
	
	
	player.start(spawn_pos, p_speed, p_vanilla_m, p_fire_rate, p_laser_speed, friendly_fire, player_colls, electrified_walls, used_deadzone, limited_ammo, ammo_amount)
	
	if first:
		$HUD.set_hud_color(player.get_color(), p_id)
	
	if limited_ammo:
		$HUD.update_ammo(p_id, ammo_amount)
	
	global_vars.players_left += 1


# reposition the players and setup the map
func respawn_players(first : bool = false) -> void:
	var spawn_pos     : Vector2
	var mod           : Color
	var otto_modifier : int   = 0
	var cameras       : Array = [camera_one, camera_two]
	var r_ammo        : int
	
	if global_vars.room == 0 and random_init_door:
		global_vars.cur_door = get_random_door()
	
	for p in marked_cells:
		for cell in marked_cells[p]:
			mapgen.highlight.set_cell(cell[0].x, cell[0].y, -1)
		
		marked_cells[p] = []
	
	# clean the map and update everything before spawning
	clean_map()
	global_vars.update_difficulty()
	
	global_vars.lasers      = 0
	global_vars.path_groups = 0
	
	# reset exited player so we properly trigger respawn if needed
	exited_player = -1
	
	# clean the interface
	$HUD.hide_bonus()
	
	# reduce otto additional spawn time
	if otto_c_down > 0:
		otto_c_down -= 1
	
	mod = global_vars.levels[global_vars.difficulty][7]
	
	mapgen.set_locked_doors_color(mod)
	
	mapgen.set_walls_color(walls_color)
	mapgen.generate_map(map_size, area_size, wall_chance, random_doors)
	
	# setup camera limits
	for camera in cameras:
		camera.set_camera_limits(Vector2(), mapgen.world_map_size)
	
	players                  = {}
	ammo_positions           = []
	global_vars.players_left = 0
	otto_spawn               = Vector2()
	
	initialize_robots()
	
	if first:
		for p in range(global_vars.num_players):
			lives[p]             = initial_lives
			last_extra_life[p]   = 0
			global_vars.score[p] = 0
	
	for p_id in lives:
		
		if lives[p_id] >= 0:
			
			if global_vars.num_players == 1:
				spawn_pos  = mapgen.get_player_spawn_pos()
				otto_spawn = spawn_pos
			
			elif len(players) == 0:
				spawn_pos = mapgen.get_player_spawn_pos(global_vars.P1)
				
				# randomly set otto spawn position at player1 spawn
				if randf() > 0.5:
					otto_spawn = spawn_pos
			
			else:
				spawn_pos = mapgen.get_player_spawn_pos(global_vars.P2)
			
			spawn_player(p_id, spawn_pos, true)
	
	$HUD.update_score(global_vars.score, true)
	
	# set otto spawn position if needed
	if otto_spawn == Vector2():
		otto_spawn = spawn_pos
	
	players_ready = true
	
	#initialize_robots()
	spawn_robots(true)
	
	otto_modifier = (otto_c_down + spawned_robots * time_per_robot + global_vars.levels[global_vars.difficulty][0]) * timer_multi
	otto_modifier /= ORIG_FPS
	
	# initialize robot voices
	if !talking:
		reset_voice_timer(global_vars.levels[global_vars.difficulty][2])
	
	if global_vars.get_game_config("OTTO", "OTTO_SPAWN")[0] && $OttoTimer.is_stopped():
		$OttoTimer.wait_time = otto_modifier
		$OttoTimer.start()
	
	# prepare to re-enable smoothing
	if $CameraTimer.is_stopped():
		$CameraTimer.start()
	
	if limited_ammo && $AmmoTimer.is_stopped():
		spawned_ammo_packs   = 0
		ammo_left            = int(stepify(robots_left * ammo_per_robot, 1.0))
		
		if extra_ammo > 0:
			if global_vars.room == 1:
				#print("FIRST")
				
				if extra_ammo_first == 1.0 || extra_ammo_first >= randf():
					next_extra_room = global_vars.room
				
				else:
					next_extra_room += randi() % (extra_ammo_max_recur - extra_ammo_min_recur) + extra_ammo_min_recur
			
			
			if global_vars.room == next_extra_room:
				next_extra_room += randi() % (extra_ammo_max_recur - extra_ammo_min_recur) + extra_ammo_min_recur
				ammo_left       += extra_ammo
				
				#print("EXTRA: ", extra_ammo, " - current room: ", global_vars.room, " - next: ", next_extra_room)
		
		if random_ammo > 0:
			r_ammo = randi() % random_ammo
			
			#print("random: ", r_ammo)
			
			if randf() > 0.5:
				ammo_left += r_ammo
			
			else:
				ammo_left -= r_ammo
		
		# add some ammo to the players on spawn if the option is enabled
		if global_vars.room > 0 && no_empty_on_change:
			r_ammo = randi() % (max_refill_on_change + 1 - min_refill_on_change) + min_refill_on_change
			
			if r_ammo * len(players) > ammo_left:
				r_ammo = floor(ammo_left / 2)
			
			for p_id in players:
				if players[p_id][0].ammo < min_refill_on_change:
					players[p_id][0].add_ammo(r_ammo)
					ammo_left -= r_ammo
			
			if ammo_left < 0:
				return
		
		robot_ammo_left      = floor(ammo_left * ammo_in_robots)
		
		ammo_left -= robot_ammo_left
		
		#print(ammo_left, " - ", robot_ammo_left)
		
		if ammo_left > 0:
			spawn_random_ammo()
			
			$AmmoTimer.wait_time = ammo_spawn_time
			$AmmoTimer.start()


# we've exited a door
func door_exited(p_id : int = -1, pos : Vector2 = Vector2(-10, -10)) -> void:
	var e_door : int
	
	# there are still players around the map, don't exit.
	if global_vars.players_left > 1 && p_id > -1:
		global_vars.players_left -= 1
		players[p_id][0].ready = false
		players[p_id][0].freeze(freeze_color)
		
		exited_player = p_id
		
		if players[p_id][0].death_timer.is_stopped():
			players[p_id][0].death_timer.start()
		
		e_door = mapgen.get_exit_door(pos)
		mapgen.lock_exit_doors(e_door, true)
		
		for enemy in get_tree().get_nodes_in_group(global_vars.ENEMY_GROUP):
			enemy.controller.find_new_target()
	
	else:
		stop_timers()
		
		chicken    = false
		in_berzerk = false
		
		if berzerk_start_min != berzerk_start_max:
			berzerk_start = randi() % berzerk_start_max + berzerk_start_min
		
		# we haven't died
		if pos.x > -10:
			# we cleared the room
			if robots_left <= 0:
				robot_start_sentence(CLEAR)
			
			# we're fleeing
			else:
				chicken      = true
				taunt_player = true
				
				if talking:
					stop_talking()
				
				reset_voice_timer(chicken_timer)
			
			e_door = mapgen.get_exit_door(pos)
			
			if e_door == global_vars.LX:
				global_vars.cur_door = global_vars.RX
			
			elif e_door == global_vars.RX:
				global_vars.cur_door = global_vars.LX
			
			elif e_door == global_vars.UP:
				global_vars.cur_door = global_vars.DN
			
			else:
				global_vars.cur_door = global_vars.UP
			
			
			for c in actors.get_children():
				c.freeze(freeze_color)
			
			for player in players:
				if is_instance_valid(players[player][0]) && !players[player][0].is_queued_for_deletion():
					players[player][0].stop_anim()
			
			if exit_effect && !in_split_screen:
				
				camera_one.smoothing_enabled = false
				camera_one.stop_tracking()
				
				camera_two.smoothing_enabled = false
				camera_two.stop_tracking()
				
				if camera_one.active:
					exit_camera = camera_one
				
				else:
					exit_camera = camera_two
				
				exit_camera.reset_camera_limits(mapgen.world_map_size)
				exit_position   = exit_camera.position
				
				move_camera = e_door
			
			$MapTransitionTimer.start()
			return
		
		clean_map()
		change_room()


# handle endgame and prompt high score board
func end_game() -> void:
	var low : int
	var gm  : int = global_vars.game_mode
	
	global_vars.read_hi_scores()
	
	low = global_vars.get_lower_hi_score()
	
	global_vars.game_over = true
	
	for player in global_vars.score:
		if global_vars.score[player] > 0 && (global_vars.score[player] >= low || global_vars.current_hs[gm] < global_vars.max_hs):
			get_tree().change_scene(set_hs_scene)
			return
	
	# show current high scores
	get_tree().change_scene(view_hs_scene)


# restart game, resetting score, lives and ideally everything
func restart_game() -> void:
	stop_timers()
	ammo                    = {}
	chicken                 = false
	taunt_player            = false
	global_vars.reset_vars(true)
	
	for player in lives:
		lives[player] = initial_lives
	
	stop_talking()
	mapgen.forward_spawned = false
	$HUD.update_lives(lives)
	$HUD.update_score(global_vars.score, true)
	door_exited()


# stop main timers
func stop_timers() -> void:
	if !$VoiceTimer.is_stopped():
		$VoiceTimer.stop()
	
	if !$OttoTimer.is_stopped():
		$OttoTimer.stop()
	
	if !$RespawnTimer.is_stopped():
		$RespawnTimer.stop()
	
	if !$RespawnCDTimer.is_stopped():
		$RespawnCDTimer.stop()

	if !$RobotSpawnTimer.is_stopped():
		$RobotSpawnTimer.stop()
	
	if !$AmmoTimer.is_stopped():
		$AmmoTimer.stop()
	
	if !$MapTransitionTimer.is_stopped():
		$MapTransitionTimer.stop()

# start a new sentence
func robot_start_sentence(talk : int = CHAT) -> void:
	var w_rand  : String
	var r_rand  : float
	var idx     : int    = 0
	var r_range : int = 1
	
	sentence = []
	talking  = true
	
	if talk == CHAT:
		r_rand = randf()
		
		if r_rand < alert_chance:
			sentence = alert_sentence
		
		else:
			for n in chat_words:
				
				if idx == 2 && !chicken:
					r_range = 1
				
				else:
					r_range = 0
				
				w_rand = n[randi() % (len(n) - r_range)]
				
				sentence.append(w_rand)
				
				if idx == 1 && w_rand == "it":
					break
				
				idx += 1
	
	elif talk == CLEAR:
		for w in escape_words:
			if w != "random":
				sentence.append(w)
			
			else:
				w_rand = escape_names[randi() % len(escape_names)]
				sentence.append(w_rand)
	
	elif talk == CHICKEN:
		sentence = chicken_sentence
	
	sentence_idx = 0
	change_voice_pitch()
	robot_talk()


# talk like a robot
func robot_talk() -> void:
	var voice : Node
	
	if sentence_idx < len(sentence):
		voice = $Voices.get_node(sentence[sentence_idx])
		voice.play()
	
	else:
		print("WARNING: voice index '", sentence_idx, "' not found in sentence: ", sentence)
		talking = false


# shut the fuck up
func stop_talking() -> void:
	talking = false
	
	for c in $Voices.get_children():
		c.stop()
	
	if !$VoiceTimer.is_stopped():
		$VoiceTimer.stop()


# reset voice timer and change the pitch
func reset_voice_timer(w_time : float = -1) -> void:
	var t_rand : int
	
	if talking:
		return
	
	if w_time > 0:
		if !$VoiceTimer.is_stopped():
			$VoiceTimer.stop()
		
		$VoiceTimer.wait_time = w_time
		$VoiceTimer.start()
	
	elif $VoiceTimer.is_stopped():
		t_rand = randi() % (max_voice_delay - min_voice_delay) + min_voice_delay
		
		$VoiceTimer.wait_time = t_rand
		$VoiceTimer.start()


# change the pitch of every voice sound
func change_voice_pitch() -> void:
	var t_rand : float
	
	t_rand = rand_range(voice_min_pitch, voice_max_pitch)
	
	for c in $Voices.get_children():
			c.set_pitch_scale(t_rand)


# pause the game
func pause() -> void:
	$HUD/PauseMenu.show_pause_menu()
	can_pause         = false
	get_tree().paused = true


# remove the pause state and start lifting the pause block
func unpause() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = false
	$PauseTimer.start()


# put every working robot in berzerk
func go_berzerk() -> void:
	in_berzerk = true
	
	for c in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		c.berzerk()

# start room transition, waiting for everything to be clear
func change_room() -> void:
	room_transition  = true
	room_trans_ticks = 0

# return a random door
func get_random_door() -> int:
	return randi() % global_vars.DN + 1

### signals ###

func _on_Player_dead(p_id : int) -> void:
	if p_id in marked_cells:
		for cell in marked_cells[p_id]:
			mapgen.highlight.set_cell(cell[0].x, cell[0].y, -1)
	
	if in_split_screen:
		in_split_screen = false
		
		if p_id in marked_cells:
			for cell in marked_cells[p_id]:
				mapgen.highlight.set_cell(cell[0].x, cell[0].y, -1)
			
			marked_cells[p_id] = []
		
		if p_id == global_vars.P1:
			viewcont_one.hide()
			viewcont_two.set_size(config_size)
			viewport_two.size = get_viewport().size
			viewport_two.set_size_override(true, global_vars.ORIG_SIZE)
			viewport_two.set_size_override_stretch(true)
			camera_one.active = false
			
		else:
			viewcont_two.hide()
			viewcont_one.set_size(config_size)
			viewport_one.size = get_viewport().size
			viewport_one.set_size_override(true, global_vars.ORIG_SIZE)
			viewport_one.set_size_override_stretch(true)
			camera_two.active = false
	
	elif player_cameras && !split_screen:
		camera_one.smoothing_enabled = false
		
		if $CameraTimer.is_stopped():
			$CameraTimer.start()
	
	if respawn_over_time && p_id != exited_player && lives[p_id] >= 0:
		dead_player = p_id
		$HUD.update_count_down(dead_player, respawn_count_down)
		$RespawnCDTimer.start()

# connect the timer to every death
func _on_Player_hit(player : Node2D) -> void:
	global_vars.players_left -= 1
	
	move_camera = global_vars.STOP
	
	if player.player_id in lives:
		lives[player.player_id] -= 1
		$HUD.update_lives(lives)
	
	if global_vars.players_left <= 0:
		global_vars.cur_door = get_random_door()
		
		if $RespawnTimer.is_stopped():
			$RespawnTimer.start()
	
	else:
		for enemy in get_tree().get_nodes_in_group(global_vars.ENEMY_GROUP):
			if enemy.controller.target == player:
				enemy.controller.find_new_target()
		
		player.death_timer.start()

# update ammo counters and the interface
func _on_Player_ammo_update(player : Node2D, wasted_ammo : int = 0):
	ammo_left += wasted_ammo
	
	ammo[player.player_id] = player.ammo
	
	ammo_positions.erase(player.last_pickup)
	
	$HUD.update_ammo(player.player_id, player.ammo)

# we died, we should do something about it, don't we?
func _on_RespawnTimer_timeout() -> void:
	for player in lives:
		if lives[player] >= 0:
			door_exited()
			return
	
	end_game()

# one player died, countdown to respawn
func _on_RespawnCDTimer_timeout() -> void:
	if respawn_count_down > 0:
		respawn_count_down -= 1
		$HUD.update_count_down(dead_player, respawn_count_down)
	
	elif in_game_respawn():
		respawn_count_down = respawn_time
		$HUD.update_score(global_vars.score, true)
		$RespawnCDTimer.stop()
	
	else:
		print("Respawn failed, Retrying...")

# an enemy was hit, award points or make the last ones go berzerk
func _on_enemy_hit(robot : Standard_Robot) -> void:
	var t_time      : float
	var bonus       : int
	var ammo_chance : float
	var min_chance  : float
	var ammo_amount : int
	var point       : Vector2
	
	# robot ammo spawn
	if limited_ammo && robot_ammo_left > 0:
		ammo_chance = robot_ammo_chance
		
		min_chance  = float(robot_ammo_left) / max_ammo_pickup
		
		if min_chance > 0:
			min_chance = floor(min_chance / float(robots_left))
			
			if min_chance > ammo_chance:
				ammo_chance = min_chance
				ammo_amount = max_ammo_pickup
			
			if ammo_chance >= 1.0 || randf() < ammo_chance:
				if robot.last_tile == Vector2():
					point = mapgen.map.world_to_map(robot.global_position)
				
				else:
					point = robot.last_tile
				
				call_deferred("spawn_ammo_pack", point, robot.get_color(), ammo_amount, true)
	
	
	robots_left    -= 1
	spawned_robots -= 1
	
	# award score to players
	add_score(kill_score, robot.killer_id)
	
	# add some time to otto spawn timer
	t_time = $OttoTimer.time_left
	
	for enemy in get_tree().get_nodes_in_group(global_vars.ROBOT_GROUP):
		enemy.controller.find_new_target()
	
	if t_time > 0 && !$OttoTimer.is_stopped():
		$OttoTimer.stop()
		$OttoTimer.wait_time = t_time + kill_t_bonus
		$OttoTimer.start()
	
	# not many robots are left
	if robots_left <= berzerk_start && !in_berzerk:
		go_berzerk()
	
	# award clear score
	if robots_left == 0:
		#print(total_spawned)
		#print("OVER")
		bonus = clear_score * total_robots
		add_score(bonus)
		#global_vars.score += bonus
		$HUD.show_bonus(bonus)
		
		# make our friendly otto go berzerk
		if is_instance_valid(evil_otto) && !evil_otto.is_queued_for_deletion():
			evil_otto.berzerk()
		
		stop_talking()
	
	# update the HUD
	$HUD.update_score(global_vars.score)

func check_extra_life(player_id : int) -> bool:
	var ups   : int = 0
	var other : int
	
	if player_id < 0:
		for id in last_extra_life:
			if check_extra_life(id):
				ups += 1
		
		if ups > 0:
			return true
	
	elif (last_extra_life[player_id] == 0 || award_life_recur) && global_vars.score[player_id] >= last_extra_life[player_id] + award_life_score:
		last_extra_life[player_id] += award_life_score
		
		if global_vars.num_players == 1 || extra_lives_both == global_vars.EXTRA_LIVES_DEFAULT:
			lives[player_id]           += award_life_amount
		
		else:
			if player_id == global_vars.P1:
				other = global_vars.P2
			
			else:
				other = global_vars.P1
			
			# give both players extra lives
			if extra_lives_both == global_vars.EXTRA_LIVES_ALIVE:
				lives[player_id] += award_life_amount
				lives[other]     += award_life_amount
			
			# if both are alive, give it to the one who scored, else the other
			elif extra_lives_both == global_vars.EXTRA_LIVES_DEAD_ONLY:
				if lives[other] < 0:
					lives[other] += award_life_amount
				
				else:
					lives[player_id] += award_life_amount
			
			# if one of the players is out of lives, reward them too
			elif extra_lives_both == global_vars.EXTRA_LIVES_DEAD:
				lives[player_id] += award_life_amount
				
				if lives[other] < 0:
					lives[other] += award_life_amount
			
		return true
	
	return false

# properly add score to players
func add_score(score : int, player_id : int = -1) -> void:
	if score == 0:
		return
	
	# split points to every alive player
	if player_id < 0 || !player_id in players:
		# split them between every player
		if add_score_while_dead:
			score /= global_vars.num_players
		
		elif global_vars.players_left > 0:
			score /= global_vars.players_left
		
		for player in players:
			if add_score_while_dead || (is_instance_valid(players[player][0]) && !players[player][0].is_queued_for_deletion()):
				# update
				if player in global_vars.score:
					global_vars.score[player] += score
				
				# add
				else:
					global_vars.score[player] = score
	
	# give them to someone specific
	elif player_id in global_vars.score:
		global_vars.score[player_id] += score
	
	# add a new player to the score dictionary
	else:
		global_vars.score[player_id] = score
	
	# award extra lives to players
	if should_award_life && check_extra_life(player_id):
		$HUD.update_lives(lives)
		
		if play_life_sound:
			$Sounds/LifeAward.play()


# spawn otto
func _on_OttoTimer_timeout() -> void:
	var control : Otto_Controller
	
	# instance otto and its controller
	evil_otto      = Otto.instance()
	control        = OttoController.instance() 
	
	# attach its body and mind
	control.body          = evil_otto
	evil_otto.controller  = control
	#evil_otto.add_to_group(global_vars.ENEMY_GROUP)
	
	actors.add_child(evil_otto)
	evil_otto.add_child(control)
	
	control.find_new_target()
	
	evil_otto.start(otto_spawn, robots_left, c_modulate)



# used to give some delay between sentences
func _on_VoiceTimer_timeout() -> void:
	if !talking:
		
		if (taunt_player):
			taunt_player = false
			robot_start_sentence(CHICKEN)
		
		else:
			robot_start_sentence()
	
	else:
		print("WARNING: Voice timer ended while talking.")



# called on voice sound end, used to play next voice and combine
func _on_voice_finished() -> void:
	if talking:
		if sentence_idx < len(sentence) - 1:
			sentence_idx += 1
			robot_talk()
		
		else:
			talking = false
			reset_voice_timer()


# triggered by the pause menu hiding
func _on_PauseMenu_popup_hide() -> void:
	unpause()


# slight timer to enable pause again
func _on_PauseTimer_timeout() -> void:
	can_pause = true


# the player clicked restart button, make sure everything goes well
func _on_PauseMenu_pause_restart() -> void:
	move_camera        = global_vars.STOP
	#$Camera2D.position = Vector2()
	
	restart_game()


# the player wants to go to the main menu
func _on_PauseMenu_main_menu() -> void:
	get_tree().change_scene(main_menu_scene)


# spawn new robots
func _on_RobotSpawnTimer_timeout() -> void:
	if update_spawn:
		global_vars.update_difficulty()
	
	if spawned_robots < max_robots_at_once:
		spawn_robots()
	
	else:
		$RobotSpawnTimer.start()

# used to re-enable player camera smoothing after some time once we changed room
func _on_CameraTimer_timeout() -> void:
	camera_one.smoothing_enabled = true
	camera_two.smoothing_enabled = true

# used to check periodically if robots are too far from players and teleport them closer
func _on_TeleportTimer_timeout() -> void:
	if robots_left > 0:
		check_teleports()
		#$TeleportTimer.start()

# a controller wants a path, put it in queue and start the timer
func _on_controller_wants_path(control : Base_Controller) -> void:
	path_queue.append(control)
	
	if $PathTimer.is_stopped():
		$PathTimer.start()

# used to make a controller process a single path and restart the timer if needed
func _on_PathTimer_timeout() -> void:
	var selected : Base_Controller
	
	while len(path_queue) > 0 && !is_instance_valid(selected):
		selected = path_queue.pop_front()
	
	if is_instance_valid(selected) && !selected.is_queued_for_deletion():
		selected.following_path = selected.find_path()
	
	if len(path_queue) <= 0 && !$PathTimer.is_stopped():
		$PathTimer.stop()

# used instead of exit_effect
func _on_MapTransitionTimer_timeout() -> void:
	clean_map()
	change_room()

# used to resize the viewport
func _on_viewport_size_changed() -> void:
	if in_split_screen:
		setup_split_screen()
	
	elif player_cameras:
		setup_single_camera()
	
	else:
		setup_fixed_camera()

# used to spawn more random ammo
func _on_AmmoTimer_timeout() -> void:
	spawn_random_ammo()
