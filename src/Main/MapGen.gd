extends Node

### constants ###
enum {WALL,                                                 # identifiers for the map tiles
	  L_DOOR_V,
	  L_DOOR_H,
	  M_WALL,
	  ML_DOOR_V,
	  ML_DOOR_H,
	  DOOR,
	  DEBUG}

const MULTI_S_MARGIN           : Vector2    = Vector2(2, 2)                       # multiplayer player spawn margin
const TILESET_S                : int        = 3                                   # size of the tileset, used to swap tiles
const R_MARGIN                 : int        = 3                                   # robot spawn margin
const SPAWN_MARGIN             : Vector2    = Vector2(3, 3)                       # margin from other robots to create a spawn point
const LINE_LIFETIME            : int        = 5000                                # debug line minimum lifetime

### exported vars ###

export (Color) var debug_color : Color      = Color(1, 0, 0, .2)                  # color for debug elements

### global vars ###

onready var global_vars        : Node       = get_node("/root/Global")

### vars used in code ###
var area_size                  : Vector2    = Vector2()                          # area size in cells
var map_size                   : Vector2    = Vector2()                          # map size in cells
var world_map_size             : Vector2    = Vector2()                          # map size in world coordinates 
var exit_doors                 : Dictionary = {}                                 # exit doors location
var map_pillars                : Dictionary = {}                                 # stored positions of the pillars expressed in cells
var x_len                      : int        = 0                                  # x length of the map pillars dictionary
var y_len                      : int        = 0                                  # y length of the map pillars dictionary
var player_sa                  : Vector2    = Vector2()                          # player spawn area, so we don't spawn robots that close to the player
var debug                      : Array      = []                                 # list to store debug elements
var forward_spawned            : bool       = false                              # have we spawned the forward dude?

onready var map                : TileMap    = $TileMap                           # tilemap class, here to be accessed from the outside
onready var highlight          : TileMap    = $Hilight                           # highlight tilemap class, here to be accessed from the outside

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

# return a correct position taking into account the tile size
func tile_center(pos : Vector2) -> Vector2:
	pos = map.map_to_world(pos)
	pos.y += map.cell_size.y / 2
	pos.x += map.cell_size.x / 2
	return pos

# generate a new map from a defined amount of pillars (outer walls included, for a vanilla berzerk map use 6x4)
# maps of random shapes aren't implemented yet
func generate_map(n_pillars : Vector2, a_size : Vector2, w_chance : float, random_doors : bool = false, random : bool = false) -> void:
	var x_idx     : Array   = []
	var y_idx     : Array   = []
	var d_ch      : float   = 1.0 / 4
	
	var start     : Vector2
	var end       : Vector2
	var start2    : Vector2
	var end2      : Vector2
	var door_s    : Vector2
	var door_e    : Vector2
	var rand      : float
	var d_idx     : int
	var old_debug : Array
	
	# define a new area size for the MapGen, so other classes will know how big an area is
	area_size = a_size
	
	# clear the tilemap
	map.clear()
	
	# clear debug elements
	while len(debug) > 0:
		old_debug = debug.pop_back()
		
		if is_instance_valid(old_debug[0]) && !old_debug[0].is_queued_for_deletion():
			old_debug[0].queue_free()
	
	# reset and define the pillars dictionary
	map_pillars = {}
	
	# define map pillars, map vectors used to draw the map
	# add two of them each row and column for the external walls
	for nX in range(n_pillars.x):
		map_pillars[nX] = []
		
		for nY in range(n_pillars.y):
			map_pillars[nX].append(Vector2(a_size.x * nX, a_size.y * nY))
	
	#print(map_pillars)
	
	x_len = len(map_pillars)
	y_len = len(map_pillars[0])
	
	# defining map size, so other classes will know
	map_size       = map_pillars[x_len - 1][y_len - 1]
	
	world_map_size = map.map_to_world(map_size)
	
	world_map_size.x += map.cell_size.x
	world_map_size.y += map.cell_size.y
	
	
	
	# try to draw doors at the center of the map
	if !random_doors:
		for _n in range(2):
			y_idx.append(y_len / 2 - 1)
			x_idx.append(x_len / 2 - 1)
	
	# draw doors at random locations
	else:
		for _n in range(2):
			x_idx.append(randi() % (x_len - 1))
			y_idx.append(randi() % (y_len - 1))
		
		# corner door prevention, basically we don't want doors to draw a corner
		if x_idx[0] == 0 && y_idx[0] == 0:
			x_idx[0] = 1
		
		elif x_idx[0] >= x_len - 2 && y_idx[1] == 0:
			x_idx[0] -= 1
		
		if x_idx[1] == 0 && y_idx[0] >= y_len - 2:
			x_idx[1] = 1
		
		elif x_idx[1] >= x_len - 2 && y_idx[1] >= y_len - 2:
			x_idx[1] -= 1
	
	# outer walls and doors for the non random shape version
	if !random:
		# reinitialize exit doors dictionary
		exit_doors = {}
		
		# add empty doors
		exit_doors[global_vars.UP] = []
		exit_doors[global_vars.DN] = []
		exit_doors[global_vars.LX] = []
		exit_doors[global_vars.RX] = []
		
		# draw the outer walls and start saving exit doors
		for n in range(2):
			start    = map_pillars[x_idx[0] + n][0]
			start2   = map_pillars[x_idx[1] + n][y_len - 1]
			
			if n == 0:
				end  = map_pillars[0][y_idx[n]]
				end2 = map_pillars[0][y_idx[n] + 1]
			
			else:
				end  = map_pillars[x_len - 1][y_idx[n]]
				end2 = map_pillars[x_len - 1][y_idx[n] + 1]
			
			exit_doors[global_vars.UP].append(start)
			exit_doors[global_vars.DN].append(start2)
			
			if n == 0:
				d_idx = global_vars.LX
			
			else:
				d_idx = global_vars.RX
			
			exit_doors[d_idx] = [end, end2]
			
			draw_tiles(start, end)
			draw_tiles(start2, end2)
		
		# creating exit doors
		for door in exit_doors:
			if global_vars.room == 0 || door != global_vars.cur_door:
				door_s = exit_doors[door][0]
				door_e = exit_doors[door][1]
				
				# drawing exit doors outside the map
				if door_s.y == door_e.y:
					if door_s.y == 0:
						door_s.y -= 1
						door_e.y -= 1
				
					else:
						door_s.y += 1
						door_e.y += 1
				
				else:
					if door_s.x == 0:
						door_s.x -= 1
						door_e.x -= 1
					
					else:
						door_s.x += 1
						door_e.x += 1
				
				draw_tiles(door_s, door_e, false, DOOR)
			
			else:
				lock_exit_doors(door)
		
		
		# generating random walls
		for key in map_pillars:
			
			# skip the first and last row, since they're external walls
			if key > 0 && key < x_len - 1:
				
				# list every internal pillar, skipping the first and last column (again they're external)
				for n in range(1, y_len - 1):
					
					# check if this wall should spawn
					if w_chance >= randf():
						
						# take another random chance, this time to decide the direction of the wall
						end = map_pillars[key][n]
						rand = randf()
						
						if rand >= d_ch * 3:
							end.y -= a_size.y
						
						elif rand >= d_ch * 2:
							end.x += a_size.x
						
						elif rand >= d_ch:
							end.x -= a_size.x
						
						else:
							end.y += a_size.y
						
						draw_tiles(map_pillars[key][n], end)
						
	
	global_vars.room += 1

# lock one or three exit doors
func lock_exit_doors(door : int, three : bool = false) -> void:
	var door_s : Vector2 
	var door_e : Vector2 
	var l_door : int
	
	if three:
		for d in global_vars.cardinal_directions():
			if d != door:
				lock_exit_doors(d)
	
	else:
		door_s = exit_doors[door][0]
		door_e = exit_doors[door][1]
		
		if door == global_vars.UP || door == global_vars.DN:
			l_door = L_DOOR_H
			door_s.x += 1
			door_e.x -= 1
			
		else:
			l_door = L_DOOR_V
			door_s.y += 1
			door_e.y -= 1
			
		draw_tiles(door_s, door_e, false, l_door)

# draw a line or corner with walls or other tiles. Edit it if you plan to draw in negative x but I don't see a single reason to
func draw_tiles(start : Vector2, end : Vector2 = Vector2(-10, -10), y_first : bool = false, tile : int = WALL, area : bool = false) -> void:
	# draw a single wall
	if end.x == -10 || start == end:
		map.set_cell(start.x, start.y, tile)
	
	# draw an area
	elif area:
		for y in range(min(start.y, end.y), max(start.y, end.y) + 1):
			draw_tiles_x(Vector2(start.x, y), max(start.x, end.x) - min(start.x, end.x), tile)
	
	# draw a simple x line
	elif start.y == end.y && start.x != end.x:
		if start.x < end.x:
			draw_tiles_x(start, end.x - start.x, tile)
		
		else:
			draw_tiles_x(end, start.x - end.x, tile)
	
	# draw a simple y line
	elif start.x == end.x && start.y != end.y:
		if start.y < end.y:
			draw_tiles_y(start, end.y - start.y, tile)
		
		else:
			draw_tiles_y(end, start.y - end.y, tile)
	
	# draw a corner
	elif start != end:
		if y_first:
			draw_tiles(start, Vector2(start.x, end.y), y_first, tile)
			draw_tiles(Vector2(start.x, end.y), end, y_first, tile)
		
		else:
			draw_tiles(start, Vector2(end.x, start.y), y_first, tile)
			draw_tiles(Vector2(end.x, start.y), end, y_first, tile)

# draw an horizontal tile line, only draws left from right
func draw_tiles_x(pos : Vector2, length : int, tile : int) -> void:
	for n in range(length + 1):
		map.set_cell(pos.x + n, pos.y, tile)

# draw a vertical tile line, only draws top from bottom
func draw_tiles_y(pos : Vector2, length : int, tile : int) -> void:
	for n in range(length + 1):
		map.set_cell(pos.x, pos.y + n, tile)

# return a spawn position for the player, need some work to be configurable
func get_player_spawn_pos(player_id : int = -1) -> Vector2:
	var spawn : Vector2  = exit_doors[global_vars.cur_door][0]
	var rand  : float     = randf()
	
	if global_vars.cur_door == global_vars.RX:
		if player_id > -1:
			if forward_spawned || (player_id == global_vars.P1 && rand <= 0.5):
				spawn.y += area_size.y * .3
				spawn.x -= area_size.x * .3
				forward_spawned = false
			
			else:
				spawn.y += area_size.y * .8
				spawn.x -= area_size.x * .8
				forward_spawned = true
		
		else:
			spawn.y += area_size.y * .5
			spawn.x -= area_size.x * .3
	
	elif global_vars.cur_door == global_vars.LX:
		if player_id > -1:
			if forward_spawned || (player_id == global_vars.P1 && rand <= 0.5):
				spawn.y += area_size.y * .3
				spawn.x += area_size.x * .3
				forward_spawned = false
			
			else:
				spawn.y += area_size.y * .8
				spawn.x += area_size.x * .8
				forward_spawned = true
		
		else:
			spawn.y += area_size.y * .5
			spawn.x += area_size.x * .3
	
	elif global_vars.cur_door == global_vars.UP:
		if player_id > -1:
			if forward_spawned || (player_id == global_vars.P1 && rand <= 0.5):
				spawn.y += area_size.y * .3
				spawn.x += area_size.x * .3
				forward_spawned = false
			
			else:
				spawn.y += area_size.y * .7
				spawn.x += area_size.x * .7
				forward_spawned = true
		
		else:
			spawn.y += max(3, area_size.y * .3)
			spawn.x += max(3, area_size.x * .5)
		
	
	elif global_vars.cur_door == global_vars.DN:
		if player_id > -1:
			if forward_spawned || (player_id == global_vars.P1 && rand <= 0.5):
				spawn.y -= area_size.y * .3
				spawn.x += area_size.x * .3
				forward_spawned = false
			
			else:
				spawn.y -= area_size.y * .6
				spawn.x += area_size.x * .6
				forward_spawned = true
		
		else:
			spawn.y -= area_size.y * .3
			spawn.x += area_size.x * .5
	
	
	player_sa = get_area(spawn)
	
	return tile_center(spawn)

# set walls color to a custom one
func set_walls_color(mod : Color) -> void:
	map.tile_set.tile_set_modulate(0, mod)

# custom color for any kind of tiles
func set_tiles_color(indexes : Array, mod : Color) -> void:
	for index in indexes:
		map.tile_set.tile_set_modulate(index, mod)

# set the color for the locked doors
func set_locked_doors_color(mod : Color) -> void:
	for tile in range(L_DOOR_V, L_DOOR_H + 1):
		map.tile_set.tile_set_modulate(tile, mod)

# set tile highlight
func set_tiles_highlight(mod : Color, p_id) -> void:
	var list : Array
	
	if p_id == 0:
		list = range(WALL, L_DOOR_H + 1)
	
	else:
		list = range(M_WALL, ML_DOOR_H + 1)
	
	for tile in list:
		highlight.tile_set.tile_set_modulate(tile, mod)

# return what exit door has been used
func get_exit_door(pos : Vector2) -> int:
	var dim
	
	# normalizing position
	if pos.x < 0:
		pos.x = 0
	
	elif pos.x > map_size.x:
		pos.x = map_size.x
	
	if pos.y < 0:
		pos.y = 0
	
	elif pos.y > map_size.y:
		pos.y = map_size.y
	
	# detecting exit door
	for door in exit_doors:
		dim = exit_doors[door]
		
		if pos.y == dim[0].y && pos.x >= dim[0].x && pos.x <= dim[1].x:
			return door
		
		if pos.x == dim[0].x && pos.y >= dim[0].y && pos.y <= dim[1].y:
			return door
	
	# this honestly shouldn't happen never ever
	print("WARNING: exit door not detected.")
	return global_vars.RX


# return an array of spawn points for robots
func get_spawn_points(amount : int, players_pos : PoolVector2Array, enemy_pos : PoolVector2Array, spawn_zone : bool, zone : Vector2) -> PoolVector2Array:
	var n            = 0
	var e_areas      = []
	var a_pillars    = []
	var s_points     = []
	var e_ar
	var r_idx
	var x_idx
	var y_idx
	var r_x
	var r_y
	var rand
	var t_pos        = Vector2()
	var safety       = 0
	var spawn_zones  = []
	var point
	
	# first we get player areas so we don't kill a player spawning a robot into them
	if len(players_pos) == 0:
		players_pos.append(player_sa)
	
	else:
		while n < len(players_pos):
			players_pos[n] = get_area(players_pos[n])
			n += 1
	
	# then we'll mark used areas so we'll not spawn enemies into others
	for pos in enemy_pos:
		e_ar = get_area(pos)
		
		if !e_ar in e_areas:
			e_areas.append(e_ar)
	
	n = 0
	
	# create spawn zones around the player
	if spawn_zone:
		for pos in players_pos:
			for x in range(-zone.x + 1, zone.x):
				for y in range(-zone.y +1, zone.y):
					if !(x == 0 && y == 0):
						t_pos = Vector2(pos.x + x * area_size.x, pos.y + y * area_size.y)
						
						if t_pos.x >= 0 && t_pos.y >= 0 && t_pos.x < map_size.x && t_pos.y < map_size.y:
							if !t_pos in spawn_zones && !t_pos in players_pos && !t_pos in enemy_pos:
								spawn_zones.append(t_pos)
	
	# start checking spawn points areas and define spawn points, surrender if the map starts looking too populated
	while n < amount && safety < 10:
		safety += 1
		
		if spawn_zone:
			if len(spawn_zones) == 0:
				break
			
			if len(spawn_zones) == 1:
				r_idx = 0
			
			else:
				r_idx = randi() % (len(spawn_zones) - 1)
				
			x_idx = spawn_zones[r_idx].x
			y_idx = spawn_zones[r_idx].y
			
			point = Vector2(x_idx, y_idx)
			
			spawn_zones.remove(r_idx)
		
		else:
			x_idx = randi() % (x_len - 1)
			y_idx = randi() % (y_len - 1)
			
			# if we selected an area occupied by a player, try a nearby one
			while map_pillars[x_idx][y_idx] in players_pos:
				rand = randf()
				
				#print("PLAYER: ", map_pillars[x_idx][y_idx])
				
				if rand >= .75:
					if x_idx > 0:
						x_idx -= 1
					
					else:
						x_idx += 1
				
				
				elif rand >= .5:
					if y_idx > 0:
						y_idx -= 1
					
					else:
						y_idx += 1
				
				
				elif rand >= .25:
					if x_idx < x_len - 2:
						x_idx += 1
					
					else:
						x_idx -= 1
				
				else:
					if y_idx < y_len - 2:
						y_idx += 1
					
					else:
						y_idx -= 1
			
			point = map_pillars[x_idx][y_idx]
		
		# spawn just one enemy per area
		if point in e_areas || point in a_pillars:
			continue
		
		safety = 0
		
		a_pillars.append(point)
		
		n += 1
	
	#if safety > 0:
	#	print("WARNING: this map cannot host the specified amount of robots, spawning the maximum amount")
	
	n = 0
	
	# ok we've got a list of pillars, make them actual spawn points
	while n < len(a_pillars):
		point = a_pillars[n]
		safety = 0
		
		r_x = randi() % int(area_size.x - R_MARGIN * 2)
		r_y = randi() % int(area_size.y - R_MARGIN * 2)
		point.x += r_x + R_MARGIN
		point.y += r_y + R_MARGIN
		s_points.append(point)
		
		
		n += 1
	
	return s_points

# return the position of the pillar of the area the provided position belongs to
func get_area(pos : Vector2, world : bool = false) -> Vector2:
	var area = Vector2()
	
	if world:
		pos = map.world_to_map(pos)
	
	if pos.x > area_size.x:
		area.x = max(0, int(pos.x) - int(pos.x) % int(area_size.x))
		area.x = min(area.x, map_size.x - area_size.x)
	
	if pos.y > area_size.y:
		area.y = max(0, int(pos.y) - int(pos.y) % int(area_size.y))
		area.y = min(area.y, map_size.y - area_size.y)
	
	#print("POS: ", pos, " AREA: ", area)
	
	return area


# return a vector expressing distance in areas between two tiles, not counting pos_a current area
func get_area_distance(pos_a : Vector2, pos_b : Vector2, world : bool = false) -> Vector2:
	var dist : Vector2
	
	pos_a = get_area(pos_a, world)
	pos_b = get_area(pos_b, world)
	
	if pos_a.x != pos_b.x:
		dist.x = (max(pos_a.x, pos_b.x) - min(pos_a.x, pos_b.x)) / area_size.x
	
	if pos_a.y != pos_b.y:
		dist.y = (max(pos_a.y, pos_b.y) - min(pos_a.y, pos_b.y)) / area_size.y
	
	if dist.x < 0:
		dist.x = -dist.x
	
	if dist.y < 0:
		dist.y = -dist.y
	
	return dist

# return an int expressing the area distance
func get_area_distance_int(pos_a : Vector2, pos_b : Vector2, world : bool = false) -> int:
	var dist = get_area_distance(pos_a, pos_b, world)
	return int(max(1, dist.x) * max(1, dist.y))

# draw a line to debug a path, storing it into the debug array
func draw_debug_path(path : PoolVector2Array, clear : bool = false, world : bool = false) -> void:
	var new_line = Line2D.new()
	var cur_time = OS.get_ticks_msec()
	var n        = 0
	
	if clear:
		for d in debug:
			if d[1] < cur_time && is_instance_valid(d[0]):
				d[0].queue_free()
		
		while n < len(debug):
			if !is_instance_valid(debug[n][0]) || debug[n][0].is_queued_for_deletion():
				debug.remove(n)
			
			else:
				n += 1
	
	n = 0
	
	while !world && n < len(path):
		path[n] = map.map_to_world(path[n])
		n += 1
	
	new_line.default_color = debug_color
	new_line.points        = path
	new_line.width         = 1
	add_child(new_line)
	
	debug.append([new_line, cur_time + LINE_LIFETIME])
