extends Node2D

class_name Ammo_Pack

onready var global_vars         : Node =  get_node("/root/Global")

var ammo_amount                 : int      = 1  # amount given by this pack
var tile_pos                    : Vector2       # position in the tilemap
var pickup                      : bool          # has the pickup even happened

onready var max_player_ammo     : int     = global_vars.get_game_config("PLAYER", "MAX_PLAYER_AMMO")[0]         # maximum amount of ammo the player can carry

### standard methods ###
func _ready() -> void:
	hide()

### common methods ###

# setup position and spawn
func start(pos : Vector2, amount : int, color : Color, point : Vector2) -> void:
	ammo_amount      = amount
	position         = pos
	tile_pos         = point
	
	$Sprite.modulate = color
	
	show()


# compatibility with other actors, change sprite color
func freeze(color : Color) -> void:
	$Sprite.modulate   = color

# pickup function called on touch or from the outside
func do_pickup(player : Node2D) -> void:
	pickup = true
	player.add_ammo(ammo_amount, tile_pos)
	
	if global_vars.vibration:
		Input.stop_joy_vibration(player.player_id)
		Input.start_joy_vibration(player.player_id, 1.0, 0, 0.15)
	
	queue_free()

### signals ###

# handle pickup or store us in the player standing on us
func _on_Area2D_body_entered(body : Node2D) -> void:
	if !pickup && is_instance_valid(body) && body.is_in_group(global_vars.PLAYER_GROUP):
		if body.ammo < max_player_ammo:
			do_pickup(body)
		
		else:
			body.current_ammo_pack = self

# remove us from the player on exit
func _on_Area2D_body_exited(body: Node) -> void:
	if !pickup && is_instance_valid(body) && body.is_in_group(global_vars.PLAYER_GROUP):
		body.current_ammo_pack = null
