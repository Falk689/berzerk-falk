extends KinematicBody2D

class_name Single_Laser

### global vars ###
onready var global_vars : Node =  get_node("/root/Global")

### global vars used by code ###
var instigator    : Node2D
var hit           : bool   = false
var player_id     : int
var fire_rotation : float
var frozen        : bool
var velocity      : Vector2
var ignore        : Node2D


### standard methods ###

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if is_instance_valid(ignore):
		add_collision_exception_with(ignore)
		$CollisionShape2D.set_deferred("disabled", false)

### common methods ###

func _physics_process(delta: float) -> void:
	var collision : KinematicCollision2D
	
	collision = move_and_collide(velocity * delta)
	
	if collision:
		if is_instance_valid(instigator) && !instigator.is_queued_for_deletion() && instigator.is_in_group(global_vars.PLAYER_GROUP) && collision.collider.is_in_group(global_vars.PLAYER_HIT_GROUP):
			if global_vars.friendly_fire:
				collision.collider.hit(instigator, player_id)
				laser_destroy()
			
			else:
				add_collision_exception_with(collision.collider)
		
		elif collision.collider.is_in_group(global_vars.HITBOX_GROUP):
			collision.collider.hit(instigator, player_id)
			laser_destroy()
		
		elif collision.collider.is_in_group(global_vars.LASER_GROUP):
			collision.collider.laser_destroy()
			laser_destroy()
		
		else:
			laser_destroy()

# fire this laser
func fire(sp : int, mod : Color, ins : Node2D, pos : Vector2, rot : float, p_id : int = -1) -> void:
	instigator = ins
	player_id  = p_id
	
	# scale the laser a bit for the robots
	if !ins.is_in_group(global_vars.PLAYER_GROUP):
		$Sprite.scale.x *= .7
		$CollisionShape2D.scale.x *= .7
	
	$Sprite.modulate = mod
	fire_rotation = rot
	position = pos
	rotation = rot
	velocity = Vector2(sp, 0).rotated(rot)
	show()
	
	if !instigator.is_in_group(global_vars.PLAYER_GROUP):
		global_vars.lasers += 1

# change this laser color
func change_color(color : Color) -> void:
	if not frozen:
		$Sprite.modulate = color

# return current color
func get_color() -> Color:
	return $Sprite.modulate

# stop, change color and go to sleep waiting to be peacefully terminated
func freeze(color : Color) -> void:
	velocity = Vector2.ZERO
	$Sprite.modulate = color
	$CollisionShape2D.set_deferred("disabled", true)
	frozen   = true

### custom methods ###


# basically die, but for lasers
func laser_destroy() -> void:
	if !hit && (!is_instance_valid(instigator) || !instigator.is_in_group(global_vars.PLAYER_GROUP)):
		hit = true
		global_vars.lasers -= 1
	
	
	if !is_queued_for_deletion():
		queue_free()
